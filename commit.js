"format cjs";

var wrap = require('word-wrap');
var map = require('lodash.map');
var longest = require('longest');
var rightPad = require('right-pad');

var filter = function(array) {
  return array.filter(function(x) {
    return x;
  });
};

var types= {
  "feature": {
      "description": "A new feature",
      "title": "Features"
    },
  "bug": {
      "description": "A code changes that led to a bug fix",
      "title": "Bug Fixes"
    },
  "improvement": {
      "description": "A code changes that led to the improvement of the program",
      "title": "Improvement"
    }
  };

 function buildCommit(options) {
  var types = options.types;

  var length = longest(Object.keys(types)).length + 1;
  var choices = map(types, function (type, key) {
    return {
      name: rightPad(key + ':', length) + ' ' + type.description,
      value: key
    };
  });

  return {
    prompter: function(cz, commit) {
      console.log('\nLine 1 will be cropped at 100 characters. All other lines will be wrapped after 100 characters.\n');

      // Let's ask some questions of the user
      // so that we can populate our commit
      // template.

      cz.prompt([
        {
          type: 'list',
          name: 'type',
          message: 'Select the type of change that you\'re committing:',
          choices: choices
        }, {
          type: 'input',
          name: 'subject',
          message: 'Enter the issue number affected by this change:',
          validate: function (value) {
            var valid = !isNaN(parseFloat(value));
            return valid || 'Please enter a number';
          },
          filter: Number
        }, {
          type: 'input',
          name: 'body',
          message: 'Write a short description of the change:\n',
          validate: function (text) {
            if (text.length < 2) {
              return 'Write a clear description.';
            }
            return true;
          }
        }, {
          type: 'confirm',
          name: 'isBreaking',
          message: 'Are there any breaking changes?(this will change the major version)',
          default: false
        }, {
          type: 'input',
          name: 'breaking',
          message: 'Describe the breaking changes:\n',
          validate: function (text) {
            if (text.length < 2) {
              return 'Write a clear description.';
            }
            return true;
          },
          when: function(answers) {
            return answers.isBreaking;
          }
        }
      ]).then(function(answers) {

        var maxLineWidth = 100;

        var wrapOptions = {
          newline: '\n',
          indent:'',
          width: maxLineWidth
        };


        // Hard limit this line
        var head = (answers.type  + ': ' + 'DER-'+answers.subject).slice(0, maxLineWidth);

        // Wrap these lines at 100 characters
        var body = wrap(answers.body, wrapOptions);

        // Apply breaking change prefix, removing it if already present
        var breaking = answers.breaking ? answers.breaking : '';
        breaking = breaking ? 'BREAKING CHANGE: ' + breaking.replace(/^BREAKING CHANGE: /, '') : '';
        breaking = wrap(breaking, wrapOptions);

        var footer = filter([ breaking]).join('\n\n');

        commit(`${head} \n\n ${answers.type.toUpperCase()}: ${body} \n\n ${footer}`);
      });
    }
  };
};
module.exports = buildCommit({
  types: types
});
