'use strict';

module.exports = {
  db: {
    uri: process.env.MEISTERFIT_POSTGRES_URI,
    options: {
      dialect: 'pg',
      debug: false,
      ssl: true
    }
  },
  apiUrl: 'https://api-dev.meisterfit.com',
  adminUrl: 'https://admin-dev.meisterfit.com',
  chargebee: {
    apiKey: 'test_2fByYjwo7qSXIT0CzON3rVO2l7cucd2An0',
    site: 'kolia-test'
  },
  log: {
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'dev',
    // Stream defaults to process.stdout
    // Uncomment to enable logging to a log on the file system
    options: {
      skip: function (req, res) { return res.statusCode < 400 }
    }
  }
};
