'use strict';

module.exports = {
  app: {
    title: 'Meisterfit - Taras API'
  },

  port: process.env.PORT || 3010,

  db: {
    migration: {
      table: 'migrations',
      folder: 'migrations'
    }
  },

  googleCloud: {
    projectId: 'meisterfit',
    keyFilename: process.env.GOOGLE_APPLICATION_CREDENTIALS || './googleCloudStorage.json',
    bucketName: 'meisterfit-dev',
  },
  jwt: {
    secret: 'taras-api',
    ttl: 180 * 24 * 60 * 60
  },
  files: {
    server: {
      models: 'app/models/**/*.js',
      policies: 'app/policies/**/*.js',
      routes: 'app/routes/**/*.js',
      controllers: 'app/controllers/**/*.js'
    },
    config: 'config/**/*.js'
  }
};
