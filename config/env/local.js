'use strict';

module.exports = {
  db: {
    uri: process.env.MEISTERFIT_POSTGRES_URI,
    options: {
      dialect: 'pg',
      ssl: true
    }
  },
  apiUrl: 'http://localhost:3010',
  adminUrl: 'http://localhost:3020',
  chargebee: {
    apiKey: 'test_2fByYjwo7qSXIT0CzON3rVO2l7cucd2An0',
    site: 'kolia-test'
  },
  log: {
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'dev',
    options: {
      //stream: 'access.log'
    }
  }
};
