'use strict';

module.exports = {
  secure: {
    ssl: false,
    privateKey: './config/sslcerts/key.pem',
    certificate: './config/sslcerts/cert.pem'
  },
  db: {
    uri: process.env.MEISTERFIT_POSTGRES_URI,
    options: {
      dialect: 'pg',
      ssl: true
    }
  },
  apiUrl: 'https://api.meisterfit.com',
  adminUrl: 'https://admin.meisterfit.com',
  chargebee: {
    apiKey: 'live_g96Z1L8rpIc1hhXGuhcdSRhPqIy1KzSof',
    site: 'meisterfit'
  },
  log: {
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'combined',
    // Stream defaults to process.stdout
    // Uncomment to enable logging to a log on the file system
    options: {
      skip: function (req, res) { return res.statusCode < 400 }
    }
  }
};
