'use strict';

/**
 * Module dependencies.
 */
const config = require('../config');
const _ = require('lodash');
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const compress = require('compression');
const methodOverride = require('method-override');
const helmet = require('helmet');
const path = require('path');
const jwt = require('jsonwebtoken');
const knex = require('./knex').db;
const cors = require('cors');
const core = require(path.resolve('app/controllers/core.controller'));
const trim = require(path.resolve('app/helpers/trim.helper.js'));

/**
 * Initialize local variables
 */
module.exports.initLocalVariables = function (app) {
  if (config.secure && config.secure.ssl === true) {
    app.locals.secure = config.secure.ssl;
  }

  // Passing the request url to environment locals
  app.use(function (req, res, next) {
    res.locals.host = req.protocol + '://' + req.hostname;
    res.locals.url = req.protocol + '://' + req.headers.host + req.originalUrl;
    next();
  });
};

/**
 * Initialize application middleware
 */
module.exports.initMiddleware = function (app) {
  // Showing stack errors
  app.set('showStackError', true);

  // Enable jsonp
  app.enable('jsonp callback');

  // Should be placed before express.static
  app.use(compress({
    filter: function (req, res) {
      return (/json|text|javascript|css|font|svg/).test(res.getHeader('Content-Type'));
    },
    level: 9
  }));

  app.use(cors());

  app.use(morgan(config.log.format, config.log.options));

  // Environment dependent middleware
  if (process.env.NODE_ENV === 'production') {
    app.locals.cache = 'memory';
  } else {
    // Disable views cache
    app.set('view cache', false);
  }

  // Request body parsing middleware should be above methodOverride
  app.use(bodyParser.urlencoded({
    limit: '5mb',
    extended: true
  }));
  app.use(bodyParser.json({limit: '5mb'}));
  app.use(methodOverride());
  // Trim body data
  app.use(function(req, res, next) {
    if (req.body) {
      req.body = trim(req.body);
    }
    next();
  });
};

/**
 * Configure Helmet headers configuration
 */
module.exports.initHelmetHeaders = function (app) {
  // Use helmet to secure Express headers
  app.use(helmet());
  app.use(helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: ["'self'"]
    }
  }))
};

/**
 * Configure the modules server routes
 */
module.exports.initModulesServerRoutes = function (app) {
  // Globbing routing files
  config.files.server.routes.forEach(function (routePath) {
    require(path.resolve(routePath))(app);
  });
};

/**
 * Configure error handling
 */
module.exports.initErrorRoutes = function (app) {
  app.use(function (err, req, res, next) {
    // If the error object doesn't exists
    if (!err) return next();

    if (err instanceof Error) {
      // Log it
      console.error(err.stack);

      // Send error
      return res.status(500).json({
        message: err.message
      });
    }

    console.log(err);
    return res.status(400).json({...err});
  });

  // Assume 404 since no middleware responded
  app.use((req, res) => {
    res.status(404).json({
      path: req.originalUrl,
      message: 'Not Found'
    });
  });
};


/**
 * Initialize JWTMiddleware
 */
module.exports.initJWTMiddleware = function (app) {
  app.use('/:url(admins|admins/*|programs|programs/*|exercises|exercises/*|athletes|athletes/*|teams|teams/*|companies|companies/*|common|users|users/*)',
    async function (req, res, next) {
    try {
      req.user = jwt.verify(core.getToken(req.headers.authorization), config.jwt.secret);

      let users = await knex('users')
        .select('users.id as id', 'name', 'email', knex.raw('(lower(users_roles.title)) as user_role'), 'users_settings.profile_image_url')
        .where({'users.id': req.user.id})
        .leftJoin('users_roles', 'users_roles.id', 'users.user_role')
        .leftJoin('users_settings', 'users_settings.user_id', 'users.id')
        .limit(1);

      req.user = users[0];

      if (!req.user) {
        return res.status(401).json({message: 'Account not found'});
      }

      knex('users').update({
        last_seen: knex.raw('now()'),
      }).where({id: req.user.id}).then();

      if (req.user.user_role === 'ADMIN') {
        return next();
      }

      next();
    } catch (err) {
      console.error(err);
      res.status(401).json({message: err.message});
    }
  });
};

/**
 * Configure the modules server policies
 */
module.exports.initModulesServerPolicies = function (app) {
  // Globbing policies files
  config.files.server.policies.forEach(function (routePath) {
    require(path.resolve(routePath)).invokeRolesPolicies();
  });
};



/**
 * Initialize the Express application
 */
module.exports.init = function () {

  // Initialize express app
  let app = express();

  // Initialize local variables
  this.initLocalVariables(app);

  // Initialize Express middleware
  this.initMiddleware(app);

  // Initialize Express middleware for JWT
  this.initJWTMiddleware(app);

  // Initialize Helmet security headers
  this.initHelmetHeaders(app);

  // Initialize modules server routes
  this.initModulesServerRoutes(app);

  // Initialize modules server routes
  this.initModulesServerPolicies(app);


  // Initialize error routes
  this.initErrorRoutes(app);

  return app;
};
