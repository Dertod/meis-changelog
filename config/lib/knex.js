'use strict';

/**
 * Module dependencies.
 */
const config = require('../config');
const path = require('path');
const types = require('pg').types;
const moment = require('moment');
const knex = require('knex');


const TIMESTAMPTZ_OID = 1184;
const TIMESTAMP_OID = 1114;

function parseFn(val) {
  return val === null ? null : moment.utc(val).toDate();
}
types.setTypeParser(TIMESTAMPTZ_OID, parseFn);
types.setTypeParser(TIMESTAMP_OID, parseFn);

// Initialize Knex
const db = knex({
  client: config.db.options.dialect,
  debug: config.db.options.debug,
  connection: config.db.uri +'?ssl=true',
  migrations: {
    directory: path.resolve() + '/' + config.db.migration.folder,
    tableName: config.db.migration.table
  },
  pool: {
    min: 1,
    max: 20
  }
});

// Connection
module.exports.db = db;


// Check connections
module.exports.checkConnection = () => {
  return db.raw('select 1 + 1');
};
