'use strict';

const Joi = require('joi');

const alphanumRegex = /^[0-9A-Z-_]+$/i;
const emailRegex = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/i;

const options = {
  language: {
    key: '{{!key}} ',
    any: {
      invalid: 'Invalid {{key}}',
      empty: 'is required'
    },
    alternatives: {
      base: 'not matching any of the allowed alternatives'
    },
    string: {
      base: 'must be a string',
      min: 'length must be at least {{limit}} characters long',
      max: 'length must be less than or equal to {{limit}} characters long',
      length: 'length must be {{limit}} characters long',
      alphanum: 'must only contain alpha-numeric characters',
      token: 'must only contain alpha-numeric and underscore characters',
      regex: {
        base: 'Invalid {{key}}',
        name: 'Invalid {{key}}',
        invert: {
          base: 'Invalid {{key}}',
          name: 'Invalid {{key}}'
        }
      },
      email: 'must be a valid email',
      uri: 'must be a valid uri',
      uriRelativeOnly: 'must be a valid relative uri',
      uriCustomScheme: 'must be a valid uri with a scheme matching the {{scheme}} pattern',
      isoDate: 'must be a valid ISO 8601 date',
      guid: 'Invalid {{key}}',
      hex: 'must only contain hexadecimal characters',
      base64: 'must be a valid base64 string',
      hostname: 'must be a valid hostname',
      lowercase: 'must only contain lowercase characters',
      uppercase: 'must only contain uppercase characters',
      trim: 'must not have leading or trailing whitespace',
      creditCard: 'must be a credit card',
      ref: 'references "{{ref}}" which is not a number',
      ip: 'must be a valid ip address with a {{cidr}} CIDR',
      ipVersion: 'must be a valid ip address of one of the following versions {{version}} with a {{cidr}} CIDR'
    }
  }
};

const extension = [
  {
    name: 'email',
    base: Joi.string().min(1).max(255).regex(emailRegex).options(options)
  },
  {
    name: 'password',
    base: Joi.string().min(2).max(255).options(options)
  },
  {
    name: 'name',
    base: Joi.string().min(2).max(255).options(options)
  },
  {
    name: 'imageUrl',
    base: Joi.string().allow(null).max(255).options(options)
  },
  {
    name: 'weight',
    base: Joi.number().allow(null).min(10).max(250).options(options)
  },
  {
    name: 'rating',
    base: Joi.number().allow(null).min(1).max(5).options(options)
  },
  {
    name: 'height',
    base: Joi.number().allow(null).min(45).max(270).options(options)
  },
  {
    name: 'age',
    base: Joi.number().allow(null).min(5).max(150).options(options)
  },
  {
    name: 'token',
    base: Joi.number().integer().options(options)
  },
  {
    name: 'userId',
    base: Joi.number().integer().options(options)
  },
  {
    name: 'companyId',
    base: Joi.number().integer().options(options)
  },
  {
    name: 'adminId',
    base: Joi.number().integer().options(options)
  },
  {
    name: 'teamId',
    base: Joi.number().integer().options(options)
  },
  {
    name: 'programId',
    base: Joi.number().integer().options(options)
  },
  {
    name: 'athleteId',
    base: Joi.number().integer().options(options)
  },
  {
    name: 'commentId',
    base: Joi.number().integer().options(options)
  },
  {
    name: 'ticketId',
    base: Joi.number().integer().options(options)
  }
];

const MeisterfitJoi = Joi.extend(extension);

module.exports = MeisterfitJoi;
