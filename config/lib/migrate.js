'use strict';

var config = require('../config'),
  chalk = require('chalk'),
  path = require('path'),
  fs = require('fs'),
  knex = require('./knex').db;

var actions = {
  /**
   * Create a new migration file. Example: 'node migrate create test-migration'
   * @param {string} name
   * @returns {Promise}
   */
  create: function (name) {
    return knex.migrate
      .make(name)
      .tap(function (file) {
        console.info('migration "' + name + '" created: ' + file);
        console.info('don\'t forget to add it to the git repository');
      });
  },

  /**
   * Run latest migrations. Example: 'node migrate latest'
   * @returns {Promise}
   */
  latest: function () {
    return knex.migrate
      .latest()
      .tap(function (result) {
        if (result[1].length === 0) {
          console.info('no new migrations were processed');
        }
        else {
          console.info(result[1].length + ' new migrations were processed');
        }
      });
  },

  /**
   * Rollback previous migration batch. Example: 'node migrate rollback'
   * @returns {Promise}
   */
  rollback: function () {
    return knex.migrate
      .rollback()
      .tap(function (result) {
        if (result[1].length === 0) {
          console.info('no migrations were rolled back');
        }
        else {
          console.info(result[1].length + ' migrations were rolled back');
        }
      });
  }
};

try {
  actions[process.argv[2]].apply(null, process.argv.slice(3))
    .then(function () {
      process.exit();
    })
    .catch(function (err) {
      console.error(err);
      process.exit();
    });
}
catch (err) {
  console.error(err);
  process.exit();
}
