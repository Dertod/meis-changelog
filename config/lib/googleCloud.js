'use strict';
const Storage = require('@google-cloud/storage');
const config = require('../config');

const CLOUD_BUCKET = config.googleCloud.bucketName;
const storage = Storage({
  projectId: config.googleCloud.projectId,
  keyFilename: config.googleCloud.keyFilename
});
const bucket = storage.bucket(CLOUD_BUCKET);

module.exports = bucket;

