'use strict';

/**
 * Module dependencies.
 */
const config = require('../config');
const knex = require('./knex');
const express = require('./express');
const chalk = require('chalk');


// Initialize Database
exports.init = function init(callback) {

  knex.checkConnection()
    .then(() => {
      // Initialize express
      let app = express.init(knex.db);

      if (callback) callback(app, knex.db, config);

      return app;
    })
    .catch((err) => {
      console.error(chalk.red(err.message));
      process.exit(-1);
    });
};


// Start app
exports.start = function start() {
  process.on('unhandledRejection', function (reason, p) {
    console.log('Possibly Unhandled Rejection at: Promise ', p, ' reason: ', reason);
  });

  this.init(function (app, db, config) {

    // Start the app by listening on <port>
    app.listen(config.port, () => {

      // Logging initialization
      console.log('--');
      console.log(chalk.green(config.app.title));
      console.log(chalk.green('Environment:\t\t\t' + process.env.NODE_ENV));
      console.log(chalk.green('Port:\t\t\t\t' + config.port));
      console.log(chalk.green('Database:\t\t\t' + config.db.uri));

      if (config.secure && config.secure.ssl === 'secure') {
        console.log(chalk.green('HTTPs:\t\t\t\ton'));
      }

      console.log('--');
    });
  });

};
