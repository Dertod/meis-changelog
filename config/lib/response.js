'use strict';

const chalk = require('chalk');
const _  = require('lodash');

/**
 * Response methods
 */
module.exports = class Response {
  getValidationError(error, custom = false) {
    if (error === null) {
      return 'next';
    }

    let errors = [];
    if (_.isArray(error.details) || _.isArray(error)) {
      let errorArray = error.details || error;
      errors = errorArray.map(detail => {
        let err = {
          param: detail.path || detail.param,
          msg: detail.message || detail.msg
        };

        if (detail.value) {
          err.value = detail.value;
        }

        return err;
      });
    }

    if (!custom && !errors.length) {
      return 'next';
    }

    let response = {
      code: 400,
      data: {
        message: 'Validation failed',
        errors
      }
    };

    if (custom) {
      response.data = {
        message: error
      };
    }

    return response;
  }

  /**
   * Method return result for request
   * todo: add new.target.name for chalk
   */
  method(name) {

    if (!this[name]) {
      console.error(chalk.red(`+ Error: Method ${name} is missing`));
    }

    return async (req, res, next) => {
      try {
        const result = await this[name](req);

        if (result === 'next') {
          return next();
        } else if (result && result.text) {
          return res.send(result.text || result);
        } else if (!result) {
          throw new Error('Method returned nothing')
        }

        res.status(result.code || 200).json(result.data || result);
      } catch (err) {
        let message, errors;

        switch (parseInt(err.code, 10)) {
          case 23505:
            if (/users_user_role_email_unique/.test(err.constraint) || /companies_athletes/.test(err.constraint)) {

              message = 'Validation failed';

              errors = [{
                param: 'email',
                msg: 'Email already exists'
              }];
            }
            break;
          default:
            try {
              message = JSON.parse(err.message).custom || 'Bad request';
            } catch (e) {
              // Log it
              console.error(err.stack || err.message);

              message = process.env.NODE_ENV === 'production' ? 'Bad request' : err.message;
            }
            break;
        }

        next({message, errors});
      }
    };
  }
};
