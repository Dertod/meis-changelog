'use strict';

if (process.env.NODE_ENV !== 'local') {
  require('@google-cloud/trace-agent').start({
    logLevel: 3,
    enhancedDatabaseReporting: true,
    ignoreUrls: ['/health'],
    serviceContext: {
      service: process.env.NODE_ENV + '-meisterfit-api',
      version: process.env.BUILD_NUMBER || null,
      minorVersion: null
    }
  });
}

require('./server.babel');

/**
 * Module dependencies.
 */
var app = require('./config/lib/app');
var server = app.start();

if (typeof gc === 'function') {
  setInterval(gc, 30 * 1000);
}
