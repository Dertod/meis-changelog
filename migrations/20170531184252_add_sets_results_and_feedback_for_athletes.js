exports.up = function (knex, Promise) {
  return knex.schema.createTable('athletes_statuses', table => {
    table.increments('id').notNullable().primary();
    table.string('title');
  }).then(() => {
    return knex('athletes_statuses').insert([{title: 'PAIN'}, {title: 'INJURY'}]);
  }).then(() => {
    return knex.schema.createTable('sets_results', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.bigint('set_id').notNullable().references('sets.id').onDelete('cascade');
      table.bigint('athlete_id').notNullable().references('users.id').onDelete('cascade');
      table.integer('time_elapsed_sec').unsigned();
      table.integer('rest_time').unsigned();
      table.integer('athlete_status_id').references('athletes_statuses.id').unsigned().onDelete('cascade');
    });
  }).then(() => {
    return knex.schema.createTable('days_feedback', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.bigint('day_id').notNullable().references('days.id').onDelete('cascade');
      table.bigint('athlete_id').notNullable().references('users.id').onDelete('cascade');
      table.float('rating');
      table.text('message');
      table.integer('athlete_status_id').references('athletes_statuses.id').unsigned().onDelete('cascade');
    });
  })
};

exports.down = function (knex, Promise) {

};
