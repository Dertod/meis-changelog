
exports.up = function(knex, Promise) {
  return knex.schema.table('tickets', (table) => {
    table.dropColumn('description');
  }).then( () => {
    return knex('tickets_types').insert([{name: 'Question'}]);
  }).then ( () => {
    return knex.schema.table('tickets', (table) => {
      table.string('description', 1000);
    })
  });
};

exports.down = function(knex, Promise) {

};
