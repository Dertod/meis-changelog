exports.up = function (knex, Promise) {
  return knex.schema.renameTable('sets_results', 'exercises_results').then(() => {
    return knex.schema.table('exercises_results', table => {
      table.dropColumn('set_id');
      table.bigint('exercise_settings_id').references('exercises_settings.id').onDelete('set null');
    })
  }).then(() => {
    return knex.schema.dropTable('sets').then(() => {
      return knex.schema.table('exercises_settings', table => {
        table.integer('duration');
        table.integer('duration_measurement_type_id').references('measurements_units_types');
        table.integer('rest_time');
        table.integer('weight');
        table.string('sets');
      })
    })
  })
};

exports.down = function (knex, Promise) {

};
