exports.up = function (knex, Promise) {

  return knex.raw(`CREATE OR REPLACE FUNCTION pseudo_encrypt(VALUE int) returns int AS $$
  DECLARE
  l1 int;
  l2 int;
  r1 int;
  r2 int;
  i int:=0;
    BEGIN
      l1:= (VALUE >> 16) & 65535;
      r1:= VALUE & 65535;
        WHILE i < 3 LOOP
          l2 := r1;
          r2 := l1 # ((((1366 * r1 + 150889) % 714025) / 714025.0) * 32767)::int;
          l1 := l2;
          r1 := r2;
          i := i + 1;
        END LOOP;
      RETURN ((r1 << 16) + l1);
    END;
  $$ LANGUAGE plpgsql strict immutable;`).then(() => {

    return knex.raw('create sequence idsSeq maxvalue 2147483647').then(() => {
      return knex.schema.createTable('users_roles', table => {
        table.increments('id').notNullable().primary();
        table.string('title').notNullable().unique();
      }).then(() => {
        return knex.schema.createTable('users', table => {
          table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
          table.string('name').notNullable();
          table.string('email').notNullable();
          table.string('hash_password');
          table.integer('user_role').unsigned().notNullable().references('users_roles.id').onDelete('cascade');
          table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
          table.timestamp('updated_at', true).defaultTo(knex.raw('now()')).notNullable();
          table.unique(['user_role', 'email']);
        })
      }).then(() => {
        return knex('users_roles')
          .insert([{title: 'ADMIN'}, {title: 'COACH'}, {title: 'ATHLETE'}])
      }).then(() => {
        return knex.schema.createTable('emails_tokens_types', table => {
          table.increments('id').notNullable().primary();
          table.string('title').notNullable().unique();
        }).then(() => {
          return knex('emails_tokens_types').insert([{title: 'INVITE NEW ADMIN'}, {title: 'CONFIRM EMAIL'}, {title: 'FORGOT PASSWORD'}, {title: 'DECLINE EMAIL CHANGE'}])
        })
      }).then(() => {
        return knex.schema.createTable('emails_tokens', table => {
          table.increments('id').notNullable().primary();
          table.bigint('token').notNullable();
          table.bigint('other_id').notNullable();
          table.integer('email_token_type_id').unsigned().notNullable().references('emails_tokens_types.id');
          table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
        })
      }).then(() => {
        return knex.schema.createTable('two_factor_auth_tokens', table => {
          table.increments('id').notNullable().primary();
          table.string('token').notNullable();
          table.bigint('other_id').notNullable();
          table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
        })
      })
    })
  })
};

exports.down = function (knex, Promise) {

};
