exports.up = function (knex, Promise) {
  return knex.schema.createTable('draft_programs', table => {
    table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
    table.string('name');
    table.boolean('is_active').defaultTo(true);
    table.bigint('creator_id').references('users.id').notNullable();
    table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
    table.timestamp('updated_at', true).defaultTo(knex.raw('now()')).notNullable();
    table.integer('category_id').references('categories.id').unsigned();
    table.float('price');
    table.bigint('company_owner_id').references('companies.id').notNullable().onDelete('cascade');
    table.text('description');
    table.integer('program_status_id').unsigned().references('programs_statuses.id');
    table.bigint('parent_program_id').references('programs.id').onDelete('set null');


  }).then(() => {
    return knex.schema.createTable('draft_exercises_settings', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.bigint('exercise_id').references('exercises.id');
      table.integer('order').unsigned();
      table.integer('characters_in_sets').unsigned();
      table.string('tempo');
      table.text('description');
      table.integer('duration_type_id').references('measurements_units_types');
      table.integer('rest_time');
      table.integer('resistance_type_id').unsigned().defaultTo(1).references('exercise_resistance_types.id').onDelete('cascade');
      table.integer('resistance').unsigned().defaultTo(0);
      table.integer('day_number').unsigned();
      table.bigint('program_id').references('draft_programs.id').notNullable().onDelete('cascade');
      table.boolean('is_warmup').defaultTo(false);
    })
  }).then(() => {
    return knex.schema.createTable('draft_sets', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.integer('sets').unsigned();
      table.string('duration').unsigned();
      table.integer('weight').unsigned();
      table.integer('week_number').unsigned();
      table.bigint('exercise_setting_id');
    })
  });
};

exports.down = function (knex, Promise) {

};
