
exports.up = function(knex, Promise) {
  return knex.schema.createTable('metadata', table => {
    table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
    table.string('key').notNullable();
    table.string('value').notNullable();
    table.bigint('company_id').references('companies.id').notNullable().onDelete('cascade');
    table.unique(['key', 'value', 'company_id']);
  }).then(() => {
    return knex.schema.createTable('athletes_metadata', table => {
      table.bigint('metadata_id').references('metadata.id').notNullable().onDelete('cascade');
      table.bigint('athlete_id').references('users.id').notNullable().onDelete('cascade');
      table.primary(['metadata_id', 'athlete_id']);
    })
  })
};

exports.down = function(knex, Promise) {

};
