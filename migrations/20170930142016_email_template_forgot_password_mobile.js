const forgotPasswordMobileTemplate = {
  html: `<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title></title>
  <style type="text/css">

    #outlook a {
      padding: 0;
    }

    .ReadMsgBody {
      width: 100%;
    }

    .ExternalClass {
      width: 100%;
    }

    .ExternalClass * {
      line-height: 100%;
    }

    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table, td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    p {
      display: block;
      margin: 13px 0;
    }

  </style>
  <!--[if !mso]><!-->
  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300);
  </style>
  <style type="text/css">
    @media only screen and (max-width: 480px) {
      @-ms-viewport {
        width: 320px;
      }
      @viewport {
        width: 320px;
      }
    }
  </style>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300" rel="stylesheet" type="text/css">
  <!--<![endif]-->
  <style type="text/css">
    @media only screen and (min-width: 480px) {
      .mj-column-per-100, * [aria-labelledby="mj-column-per-100"] {
        width: 100% !important;
      }
    }</style>
</head>
<body id="YIELD_MJML" style="">
<div class="mj-body"><!--[if mso]>
  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="width:100%;">
    <tr>
      <td>
  <![endif]-->
  <div style="margin:0 auto;max-width:100%;background:#EDEDF1;">
    <table class="" cellpadding="0" cellspacing="0" style="width:100%;font-size:0px;background:#ffffff;" align="center">
      <tbody>
      <tr>
        <td style="text-align:center;vertical-align:top;font-size:0;padding:20px 0;"><!--[if mso]>
          <table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td style="width:100%;">
          <![endif]-->
          <div style="vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;"
               class="mj-column-per-100" aria-labelledby="mj-column-per-100">
            <table width="100%">
              <tbody>
              <tr>
                <td colspan="3" style="font-size:0;padding:10px 25px;" align="center">
                  <table cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;"
                         align="center">
                    <tbody>
                    <tr>
                      <td></td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td colspan="3"
                    style="font-size:0;padding:10px 25px;-moz-border-radius:5px;-webkit-border-radius:5px; border-radius:5px;">
                  <!--[if mso]>
                  <table style="font-size: 1px; margin: 0px; border-top: 4px solid #FFFFFF; width: 100%;"></table>
                  <![endif]--></td>
              </tr>

              <tr >
                <td style="width: 25%"></td>
                <td 
                  style="width: 50%;font-size:0;padding:10px 25px;border: 2px solid #f2f2f2;background:#FBFBFB;-moz-border-radius:5px;-webkit-border-radius:5px; border-radius:5px;"
                  align="left">
                  <div class="mj-content"` +
                       'style="padding: 0px 0px 80px 0px;cursor:auto;color:#000000;font-family:helvetica;font-size:14px;line-height:22px; min-width: 470px"><p>Dear ${name},</p><br><p>You have requested the recovery password in MeisterFit service.</p><p>Copy this code to your MeisterFit Application (The code will expire in 10 minutes and will become invalid if you write it incorrectly 3 times)</p> <p style="font-size:24px">${code}</p>' +
                       `</div>
                </td>
                <td style="width: 25%"></td>
              </tr>
             </tbody>
            </table>
          </div>
          <!--[if mso]>
          </td></tr></table>
          <![endif]--></td>
      </tr>
      </tbody>
    </table>
  </div>
  <!--[if mso]>
  </td></tr></table>
  <![endif]--></div>
</body>
</html>
`,
  subject: 'MeisterFit Recovery. Mobile App.',
  email_template_type_id: 7,
  language_id: 1
};

exports.up = function (knex, Promise) {
  return knex('email_templates_types').insert({title: 'FORGOT PASSWORD MOBILE'}).then(() => {
    return knex('email_templates').insert(forgotPasswordMobileTemplate);
  });
};

exports.down = function (knex, Promise) {

};
