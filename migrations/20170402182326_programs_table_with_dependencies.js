
exports.up = function(knex, Promise) {
  return knex.schema.createTable('programs', table => {
    table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
    table.string('name').notNullable();
    table.boolean('is_active').defaultTo(true);
    //table.integer('format_id').references('program_formats.id').notNullable();
    table.bigint('creator_id').references('users.id').notNullable();
    table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
    table.timestamp('updated_at', true).defaultTo(knex.raw('now()')).notNullable();
  }).then(() => {
    return knex.schema.createTable('weeks', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.bigint('program_id').notNullable().references('programs.id').onDelete('cascade');
      table.integer('week_number');
    })
  }).then(() => {
    return knex.schema.createTable('days', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.bigint('week_id').notNullable().references('weeks.id').onDelete('cascade');
      table.integer('day_number');
      table.boolean('is_rest_day').defaultTo(false);
    })
  }).then(() => {
    return knex.schema.createTable('measurements_units_types', table => {
      table.increments('id').notNullable().primary();
      table.string('title');
    }).then(() => {
      return knex('measurements_units_types').insert([{title: 'REPS'}, {title: 'TIME'}])
    })
  }).then(() => {
    return knex.schema.createTable('exercises', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.string('name').notNullable();
      table.text('description');
      table.string('video_link');
      table.string('logo_image_url');
    })
  }).then(() => {
    return knex.schema.createTable('exercises_settings', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.bigint('exercise_id').references('exercises.id').notNullable();
      table.bigint('day_id').references('days.id').notNullable().onDelete('cascade');
      table.integer('order').unsigned();
      table.integer('set_number').notNullable().unsigned();
      table.integer('sets');
      table.integer('duration');
      table.integer('duration_measurement_type_id').references('measurements_units_types');
      table.integer('rest_time');
      table.integer('weight');
      table.string('tempo');
    })
  })
};

exports.down = function(knex, Promise) {

};
