
exports.up = function(knex, Promise) {
  return knex.schema.createTable('exercise_movement_types', table => {
    table.increments('id').notNullable().primary();
    table.string('movement_type');
  }).then(()=>{
    return knex('exercise_movement_types').insert([
      {movement_type: 'Horizontal push'},
      {movement_type: 'Horizontal pull'},
      {movement_type: 'Vertical push'},
      {movement_type: 'Vertical pull'},
      {movement_type: 'Lower body hip dominant'},
      {movement_type: 'Lower body knee dominant '},
      {movement_type: 'Core'},
      {movement_type: 'Power'},
    ]);
  }).then(()=>{
    return knex.schema.table('exercises', table => {
      table.integer('movement_type_id').references('exercise_movement_types.id');
    })
  });
};

exports.down = function(knex, Promise) {

};
