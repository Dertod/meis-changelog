
exports.up = function (knex, Promise) {
  return knex.schema.createTable('draft_programs_exercises_days', table => {
    table.bigint('program_id').references('draft_programs.id').onDelete('cascade');
    table.bigint('exercise_setting_id').references('draft_exercises_settings.id').onDelete('cascade');
    table.primary(['program_id', 'exercise_setting_id']);
  }).then(() => {
    return knex('draft_exercises_settings').select('id as exercise_setting_id', 'program_id').then((programsExercises) => {
      return knex('draft_programs_exercises_days').insert(programsExercises);
    })
  }).then(() => {
    return knex.schema.table('draft_exercises_settings', table => {
      table.dropColumn('program_id');
    }).then(() => {
      return knex.schema.createTable('programs_exercises_days', table => {
        table.bigint('program_id').references('programs.id').onDelete('cascade');
        table.bigint('exercise_setting_id').references('exercises_settings.id').onDelete('cascade');
        table.primary(['program_id', 'exercise_setting_id']);
      }).then(() => {
        return knex('exercises_settings').select('id as exercise_setting_id', 'program_id').then((programsExercises) => {
          return knex('programs_exercises_days').insert(programsExercises);
        })
      }).then(() => {
        return knex.schema.table('exercises_settings', table => {
          table.dropColumn('program_id');
        })
      })
    })
  }).then(() => {
    return knex.schema.table('draft_exercises_settings', table => {
      table.bigint('original_exercise_setting_id').references('exercises_settings.id').onDelete('cascade');
    })
  }).then(() => {
    return knex.schema.table('programs', table => {
      table.boolean('is_variation').defaultTo(false).notNullable();
    })
  })

};

exports.down = function (knex, Promise) {

};
