
exports.up = function(knex, Promise) {
  return knex.schema.createTable('exercise_resistance_types', table => {
    table.increments('id').notNullable().primary();
    table.string('resistance_type');
  }).then(() => {
    return knex('exercise_resistance_types').insert([{resistance_type: 'EVERY_WORKOUT'}, {resistance_type: 'LAST_WORKOUT'}])
  }).then(() => {
    return knex.schema.table('exercises_settings', table => {
      table.integer('resistance_type_id').unsigned().notNullable().defaultTo(1).references('exercise_resistance_types.id').onDelete('cascade');
      table.integer('resistance').unsigned().defaultTo(0).notNullable();
    })
  })
};

exports.down = function(knex, Promise) {

};
