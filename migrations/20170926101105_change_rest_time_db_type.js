var draftRestTimes, restTimes;

exports.up = function (knex, Promise) {
  return knex('draft_exercises_settings').select('id', 'rest_time').then((restTime) => {
    draftRestTimes = restTime;
    return knex.schema.table('draft_exercises_settings', table => {
      table.dropColumn('rest_time');
    }).then(() => {
      return knex.schema.table('draft_exercises_settings', table => {
        table.string('rest_time');
      })/*.then(() => {
        return knex.raw(`update draft_exercises_settings as des set
    rest_time = temp.rest_time from (values ${draftRestTimes.map(exercise => `(${exercise.id}, '${exercise.rest_time}')`)})
     as temp(id, rest_time) where temp.id = des.id;`)
      })*/
    }).then(() => {
      return knex('exercises_settings').select('id', 'rest_time').then((restTime) => {
        restTimes = restTime;
        return knex.schema.table('exercises_settings', table => {
          table.dropColumn('rest_time');
        })
      })
    })
  }).then(() => {
    return knex.schema.table('exercises_settings', table => {
      table.string('rest_time');
    })/*.then(() => {
      return knex.raw(`update exercises_settings as es set
    rest_time = temp.rest_time from (values ${restTimes.map(exercise => `(${exercise.id}, '${exercise.rest_time}')`)})
     as temp(id, rest_time) where temp.id = es.id;`)
    })*/
  })
};

exports.down = function (knex, Promise) {

};
