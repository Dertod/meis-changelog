
exports.up = function(knex, Promise) {
  return knex.schema.table('comments', (table) => {
    table.dropColumn('created_at');
  }).then( () => {
    return knex.schema.table('comments', (table) => {
      table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
      table.timestamp('updated_at', true).defaultTo(knex.raw('now()')).notNullable();
    })
  }).then( () => {
    return knex.schema.table('tickets', (table) => {
      table.dropColumn('created_at');
      table.dropColumn('updated_at');
    })
  }).then( () => {
    return knex.schema.table('tickets', (table) => {
      table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
      table.timestamp('updated_at', true).defaultTo(knex.raw('now()')).notNullable();
    })
  });
};

exports.down = function(knex, Promise) {

};
