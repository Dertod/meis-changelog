
exports.up = function(knex, Promise) {

  return knex.schema.table('exercises_settings', table => {
    table.text('description');
  }).then(() => {

    return knex.schema.table('exercises', table => {
      table.bigint('creator_id').references('users.id').onDelete('set null');
    }).then(() => {
      return knex.insert({}).into('users_settings').returning('id').then(id => {
        return knex('users').insert({
          name: 'System',
          email: 'denis@compomatic.com',
          hash_password: knex.raw(`crypt('${encodeURIComponent('123')}', gen_salt(\'md5\'))`),
          user_role: 1,
          user_settings_id: id[0]
        }).returning('id').then((userId) => {

          return knex('exercises').update({creator_id: userId[0]})
            .where({name: 'Barbell bench press'})
            .orWhere({name: 'Heavy Dips'})
            .orWhere({name: 'Cable Crossover'});
        });
      });
    })
  })
};

exports.down = function(knex, Promise) {

};
