exports.up = function (knex, Promise) {
  return knex.schema.createTable('teams', table => {
    table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
    table.string('name').notNullable();
    table.string('description');
    table.string('brand_image_url');
    table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
    table.timestamp('updated_at', true).defaultTo(knex.raw('now()')).notNullable();
  }).then(() => {
    return knex.schema.createTable('users_settings', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.string('profile_image_url');
      table.integer('weight').unsigned();
      table.integer('height').unsigned();
      table.integer('age').unsigned();
      table.bigint('facebook_id');
    }).then(() => {
      return knex.schema.table('users', table => {
        table.bigint('user_settings_id').references('users_settings.id').notNullable().onDelete('cascade');
        table.unique('user_settings_id');
      })
    })
  }).then(() => {
    return knex.schema.createTable('categories', table => {
      table.increments('id').notNullable().primary();
      table.string('title').notNullable();
    }).then(() => {
      return knex.schema.table('programs', table => {
        table.integer('category_id').references('categories.id').unsigned();
      })
    }).then(() => {
      return knex('categories').insert([{title: 'FAT BURN'}, {title: 'MUSCLE GROWTH'}, {title: 'STRENGTH GAINS'}]);
    })
  })
};

exports.down = function (knex, Promise) {

};
