
exports.up = function(knex, Promise) {
  return knex('message_types').insert([{message_type: 'system_assign_athlete_to_program'}, {message_type: 'system_delete_athlete_from_program'},
    {message_type: 'system_added_athlete'}, {message_type: 'system_delete_athlete'}])
};

exports.down = function(knex, Promise) {

};
