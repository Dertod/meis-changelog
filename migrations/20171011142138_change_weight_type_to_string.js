
exports.up = function(knex, Promise) {
 return knex.schema.raw("ALTER TABLE sets ALTER COLUMN weight SET DATA TYPE varchar(255)").then(()=>{
   return knex.schema.raw("ALTER TABLE draft_sets ALTER COLUMN weight SET DATA TYPE varchar(255)")
 });
};

exports.down = function(knex, Promise) {

};
