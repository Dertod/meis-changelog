
exports.up = function(knex, Promise) {
  return knex.schema.createTable('message_types', table => {
    table.increments('id').notNullable().primary();
    table.string('message_type');
  }).then(() => {
    return knex('message_types').insert({message_type: 'direct'});
  }).then(() => {
    return knex.schema.createTable('messages', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.integer('message_type_id').notNullable().references('message_types.id');
      table.bigint('from_user_id').notNullable().references('users.id').onDelete('cascade');
      table.bigint('to_id').notNullable().references('users.id').onDelete('cascade');
      table.text('message');
      table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
      table.timestamp('updated_at', true).defaultTo(knex.raw('now()')).notNullable();
    });
  });
};

exports.down = function(knex, Promise) {

};
