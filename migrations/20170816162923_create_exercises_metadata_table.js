
exports.up = function(knex, Promise) {
  return knex.schema.createTable('exercises_metadata', table => {
    table.bigint('metadata_id').references('metadata.id').notNullable().onDelete('cascade');
    table.bigint('exercise_id').references('exercises.id').notNullable().onDelete('cascade');
    table.primary(['metadata_id', 'exercise_id']);
  }).then(() => {
    return knex.schema.table('exercises', table => {
      table.boolean('is_moderated').defaultTo(false).notNullable();
    })
  })
};

exports.down = function(knex, Promise) {

};
