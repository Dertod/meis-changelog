exports.up = function (knex, Promise) {
  return knex.schema.createTable('coaches_athletes_relationship_types', table => {
    table.increments('id').notNullable().primary();
    table.string('title');
  }).then(() => {
    return knex('coaches_athletes_relationship_types').insert([{title: 'PROGRAM'}, {title: 'SUBSCRIPTION'}, {title: 'INDIVIDUAL'}])
  }).then(() => {
    return knex.schema.createTable('coaches_athletes', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.bigint('coach_id').references('users.id').notNullable().onDelete('cascade');
      table.integer('coach_athlete_relationship_type_id').unsigned().references('coaches_athletes_relationship_types.id').notNullable().onDelete('cascade');
      table.bigint('athlete_id').references('users.id').notNullable().onDelete('cascade');
      table.bigint('creator_id').references('users.id').notNullable().onDelete('cascade');
    })
  }).then(() => {
    return knex.schema.createTable('programs_athletes', table => {
      table.bigint('program_id').references('programs.id');
      table.bigint('coach_athlete_id').references('coaches_athletes.id');
      table.primary(['program_id', 'coach_athlete_id']);
    });
  }).then(() => {
    return knex.schema.createTable('teams_athletes', table => {
      table.bigint('team_id').references('teams.id').onDelete('cascade');
      table.bigint('coach_athlete_id').references('coaches_athletes.id').onDelete('cascade');
      table.primary(['team_id', 'coach_athlete_id']);
    })
  });
};

exports.down = function (knex, Promise) {

};
