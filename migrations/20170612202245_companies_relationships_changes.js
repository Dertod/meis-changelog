exports.up = function (knex, Promise) {
  return knex.schema.table('programs', table => {
    table.bigint('company_owner_id').references('companies.id').notNullable().onDelete('cascade');
  }).then(() => {
    return knex.schema.renameTable('coaches_athletes_relationship_types', 'companies_athletes_relationship_types').then(() => {
      return knex.schema.renameTable('coaches_athletes', 'companies_athletes').then(() => {
        return knex.schema.table('companies_athletes', table => {
          table.dropColumn('coach_id');
          table.bigint('company_id').references('companies.id').notNullable().onDelete('cascade');
          table.renameColumn('coach_athlete_relationship_type_id','company_athlete_relationship_type_id');
          table.unique(['company_id', 'athlete_id']);
        })
      })
    }).then(() => {
      return knex.schema.table('programs_athletes', table => {
        table.renameColumn('coach_athlete_id', 'company_athlete_id');
      });
    }).then(() => {
      return knex.schema.table('teams_athletes', table => {
        table.renameColumn('coach_athlete_id', 'company_athlete_id');
      })
    }).then(() => {
      return knex.schema.table('messages', table => {
        table.bigint('company_id').references('companies.id').onDelete('set null');
      })
    }).then(() => {
      return knex.schema.table('teams', table => {
        table.dropColumn('team_coach_id');
        table.bigint('team_company_id').references('companies.id').onDelete('cascade');
        table.bigint('creator_id').references('users.id').onDelete('set null');
      })
    });
  }).then(() => {
    return knex.schema.table('users_settings', table => {
      table.unique('user_id');
    })
  }).then(() => {
    return knex('companies').insert({name: 'System'}).returning('id').then((companyId) => {
      return knex('users').select('id').where({email: 'denis@compomatic.com'}).then((userId) => {
        return knex
          .insert({
            company_id: companyId[0],
            admin_id: userId[0].id,
            company_member_type: 1
          }).into('companies_admins')
      })
    })
  })
};

exports.down = function (knex, Promise) {

};
