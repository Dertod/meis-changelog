
exports.up = function(knex, Promise) {
  return knex.schema.table('exercises_results', table => {
    table.bigint('exercise_id').references('exercises.id').onDelete('cascade');
    table.integer('order').unsigned();
    table.integer('week_number').unsigned();
    table.bigint('day_feedback_id').references('days_feedback.id').onDelete('set null');
    table.dropColumn('weight');
  }).then(() => {
    return knex.schema.createTable('exercises_results_sets', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.bigint('exercise_result_id').notNullable().references('exercises_results.id').onDelete('cascade');
      table.integer('duration');
      table.integer('duration_measurement_type_id').references('measurements_units_types');
      table.integer('rest_time');
      table.integer('time_elapsed_sec').unsigned();
      table.integer('weight');
    })
  }).then(() => {
    return knex.schema.table('exercises_settings', table => {
      table.dropColumns(['weight', 'session']);
    })
  }).then(() => {
    return knex.schema.table('days_feedback', table => {
      table.integer('week_number').unsigned();
    })
  }).then(() => {
    return knex.schema.table('exercises_settings', table => {
      table.renameColumn('set_number', 'characters_in_sets');
    })
  }).then(() => {
    return knex('athletes_statuses').insert({title: 'HAPPINESS'});
  });
};

exports.down = function(knex, Promise) {

};
