exports.up = function (knex, Promise) {
  let ids;
  return knex('exercises').insert([{
    name: "Rickshaw Carry",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Single-Leg Press", category_type_id: "2", is_moderated: "true"}, {
    name: "Atlas Stones",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Incline Hammer Curls",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Wrist Rotations with Straight Bar",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Single-Arm Linear Jammer",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Side Laterals to Front Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "T-Bar Row with Handle", category_type_id: "5", is_moderated: "true"}, {
    name: "One-Arm Medicine Ball Slam",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Palms-Down Wrist Curl Over A Bench",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Clean from Blocks", category_type_id: "2", is_moderated: "true"}, {
    name: "Weighted Pull Ups",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Landmine 180's", category_type_id: "3", is_moderated: "true"}, {
    name: "Dips - Triceps Version",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Barbell Full Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Standing Palm-In One-Arm Dumbbell Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Palms-Up Barbell Wrist Curl Over A Bench",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Farmer's Walk", category_type_id: "1", is_moderated: "true"}, {
    name: "Romanian Deadlift With Dumbbells",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Barbell Glute Bridge", category_type_id: "2", is_moderated: "true"}, {
    name: "Clean Deadlift",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Deficit Deadlift", category_type_id: "5", is_moderated: "true"}, {
    name: "Tire Flip",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Clean and Press", category_type_id: "6", is_moderated: "true"}, {
    name: "Barbell Deadlift",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Standing Palms-Up Barbell Behind The Back Wrist Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Lying Face Down Plate 6 Resistance",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Decline EZ Bar Triceps Extension", category_type_id: "1", is_moderated: "true"}, {
    name: "Push Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Snatch", category_type_id: "2", is_moderated: "true"}, {
    name: "Clean and Jerk",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Hang Clean", category_type_id: "2", is_moderated: "true"}, {
    name: "Finger Curls",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Suspended Fallout", category_type_id: "3", is_moderated: "true"}, {
    name: "Wide-Grip Standing Barbell Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Standing Palms-In Dumbbell Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Standing Military Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "One-Arm Kettlebell Push Press", category_type_id: "6", is_moderated: "true"}, {
    name: "Box Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Dumbbell Floor Press", category_type_id: "1", is_moderated: "true"}, {
    name: "Sumo Deadlift",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Reverse Band Box Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Plank",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Standing Cable Lift", category_type_id: "3", is_moderated: "true"}, {
    name: "Bottoms Up",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Spell Caster", category_type_id: "3", is_moderated: "true"}, {
    name: "Dumbbell V-Sit Cross Jab",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Dumbbell Bench Press", category_type_id: "4", is_moderated: "true"}, {
    name: "Pullups",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Pushups", category_type_id: "4", is_moderated: "true"}, {
    name: "Reverse Grip Bent-Over Rows",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Smith Machine Shrug", category_type_id: "5", is_moderated: "true"}, {
    name: "Spider Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Decline Reverse Crunch",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Front Squats With Two Kettlebells", category_type_id: "2", is_moderated: "true"}, {
    name: "Power Snatch",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Romanian Deadlift from Deficit",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Single Leg Push-off", category_type_id: "2", is_moderated: "true"}, {
    name: "Hip Circles (prone)",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Smith Machine Calf Raise",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Seated Two-Arm Palms-Up Low-Pulley Wrist Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Seated Barbell Military Press", category_type_id: "6", is_moderated: "true"}, {
    name: "Rope Jumping",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Power Clean from Blocks", category_type_id: "2", is_moderated: "true"}, {
    name: "Spider Crawl",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Barbell Walking Lunge", category_type_id: "2", is_moderated: "true"}, {
    name: "Dumbbell Flyes",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Close-Grip Barbell Bench Press", category_type_id: "1", is_moderated: "true"}, {
    name: "Cross-Body Crunch",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Chin-Up", category_type_id: "5", is_moderated: "true"}, {
    name: "Wrist Roller",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Triceps Pushdown - V-Bar Attachment",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "One-Arm Side Laterals", category_type_id: "6", is_moderated: "true"}, {
    name: "EZ-Bar Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Power Partials", category_type_id: "6", is_moderated: "true"}, {
    name: "Barbell Hip Thrust",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Olympic Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Axle Deadlift",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Leverage Shrug",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "One-Arm High-Pulley Cable Side Bends",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Floor Glute-Ham Raise", category_type_id: "2", is_moderated: "true"}, {
    name: "Elbow to Knee",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Hyperextensions (Back Extensions)", category_type_id: "5", is_moderated: "true"}, {
    name: "Hammer Curls",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Zottman Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Weighted Bench Dip",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Seated Dumbbell Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Standing Dumbbell Straight-Arm Front Delt Raise Above Head",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Reverse Flyes", category_type_id: "6", is_moderated: "true"}, {
    name: "Incline Dumbbell Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Kettlebell Pistol Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Step Mill",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Natural Glute Ham Raise", category_type_id: "2", is_moderated: "true"}, {
    name: "Standing Dumbbell Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Low Cable Crossover", category_type_id: "4", is_moderated: "true"}, {
    name: "Cocoons",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "1 Curl To Shoulder Press", category_type_id: "1", is_moderated: "true"}, {
    name: "Decline Dumbbell Flyes",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Plate Twist", category_type_id: "3", is_moderated: "true"}, {
    name: "Concentration Curls",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Dips - Chest Version",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Kneeling Cable Triceps Extension", category_type_id: "1", is_moderated: "true"}, {
    name: "Thigh Adductor",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Barbell Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Reverse Grip Triceps Pushdown",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Gorilla Chin/Crunch", category_type_id: "3", is_moderated: "true"}, {
    name: "Decline Crunch",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Overhead Cable Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Hanging Pike",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Narrow Stance Squats", category_type_id: "2", is_moderated: "true"}, {
    name: "Rocky Pull-Ups/Pulldowns",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Flexor Incline Dumbbell Curls",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Dumbbell Lying One-Arm Rear Lateral Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Standing Dumbbell Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Barbell Bench Press - Medium Grip",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Front Dumbbell Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Seated One-Arm Dumbbell Palms-Up Wrist Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Seated Palms-Down Barbell Wrist Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Bear Crawl Sled Drags", category_type_id: "2", is_moderated: "true"}, {
    name: "Alternating Deltoid Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Machine Bicep Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Bodyweight Flyes",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "One-Arm Long Bar Row", category_type_id: "5", is_moderated: "true"}, {
    name: "Hanging Oblique Knee Raise",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Bodyweight Lunge", category_type_id: "2", is_moderated: "true"}, {
    name: "One-Arm Dumbbell Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Standing Calf Raises", category_type_id: "2", is_moderated: "true"}, {
    name: "One-Legged Cable Kickback",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Incline Cable Flye", category_type_id: "4", is_moderated: "true"}, {
    name: "Dumbbell Shoulder Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Push-Ups - Close Triceps Position",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Box Squat with Bands", category_type_id: "2", is_moderated: "true"}, {
    name: "Squat with Chains",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Weighted Jump Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Single-Leg High Box Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "EZ-Bar Skullcrusher", category_type_id: "1", is_moderated: "true"}, {
    name: "Single Dumbbell Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Car Drivers", category_type_id: "6", is_moderated: "true"}, {
    name: "Standing Alternating Dumbbell Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Weighted Suitcase Crunch", category_type_id: "3", is_moderated: "true"}, {
    name: "Oblique Cable Crunch",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Snatch-Grip Behind-The-6 Overhead Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Bent Over One-Arm Long Bar Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Wide-Grip Decline Barbell Bench Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Front Barbell Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Lying Leg Curls",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Triceps Pushdown - Rope Attachment",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Smith Machine Overhead Shoulder Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Cable One Arm Tricep Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Arnold Dumbbell Press", category_type_id: "6", is_moderated: "true"}, {
    name: "Exercise Ball Pull-In",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Cable Crunch", category_type_id: "3", is_moderated: "true"}, {
    name: "V-Bar Pulldown",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Barbell Ab Rollout", category_type_id: "3", is_moderated: "true"}, {
    name: "Hanging Leg Raise",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Dumbbell Bicep Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Decline Close-Grip Bench To Skull Crusher",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Dumbbell Lying Supination", category_type_id: "1", is_moderated: "true"}, {
    name: "Bicycling",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Rowing, Stationary", category_type_id: "2", is_moderated: "true"}, {
    name: "Leverage Shoulder Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Kneeling Cable Crunch With Alternating Oblique Twists",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Muscle Up", category_type_id: "5", is_moderated: "true"}, {
    name: "Shotgun Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Incline Dumbbell Press Reverse-Grip", category_type_id: "4", is_moderated: "true"}, {
    name: "T-Bar Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Barbell Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Bent Over Two-Arm Long Bar Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Wide-Grip Barbell Bench Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Decline Barbell Bench Press", category_type_id: "4", is_moderated: "true"}, {
    name: "Dumbbell Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Ab Roller", category_type_id: "3", is_moderated: "true"}, {
    name: "Barbell Ab Rollout - On Knees",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Front Plate Raise", category_type_id: "6", is_moderated: "true"}, {
    name: "Cross Body Hammer Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Close-Grip EZ Bar Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Seated Triceps Press",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Incline Barbell Triceps Extension", category_type_id: "1", is_moderated: "true"}, {
    name: "Snatch Deadlift",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Side to Side Box Shuffle", category_type_id: "2", is_moderated: "true"}, {
    name: "Weighted Crunches",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Dumbbell Goblet Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Wide-Grip Pull-Up",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Hyperextensions With No Hyperextension Bench",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Dumbbell Lunges", category_type_id: "2", is_moderated: "true"}, {
    name: "Leg Press",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Butt Lift (Bridge)", category_type_id: "2", is_moderated: "true"}, {
    name: "Reverse Crunch",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Air Bike", category_type_id: "3", is_moderated: "true"}, {
    name: "Narrow Stance Hack Squats",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Narrow Stance Leg Press",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Barbell Curls Lying Against An Incline",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Dumbbell Lying Pronation", category_type_id: "1", is_moderated: "true"}, {
    name: "Groiners",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Elliptical Trainer", category_type_id: "2", is_moderated: "true"}, {
    name: "Stairmaster",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Double Leg Butt Kick", category_type_id: "2", is_moderated: "true"}, {
    name: "Standing Long Jump",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Ball Leg Curl", category_type_id: "2", is_moderated: "true"}, {
    name: "Incline Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Single Leg Glute Bridge", category_type_id: "2", is_moderated: "true"}, {
    name: "Dumbbell Incline Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Close-Grip EZ-Bar Press", category_type_id: "1", is_moderated: "true"}, {
    name: "Bodyweight Mid Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Burpee", category_type_id: "2", is_moderated: "true"}, {
    name: "Reverse Barbell Preacher Curls",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Close-Grip Front Lat Pulldown",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Bent Over Two-Dumbbell Row With Palms In",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Stiff-Legged Dumbbell Deadlift",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Incline Dumbbell Flyes", category_type_id: "4", is_moderated: "true"}, {
    name: "Barbell Lunge",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Seated Cable Rows", category_type_id: "5", is_moderated: "true"}, {
    name: "Cable Crossover",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "V-Bar Pullup", category_type_id: "5", is_moderated: "true"}, {
    name: "Dip Machine",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Preacher Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Standing One-Arm Cable Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Standing Dumbbell Calf Raise", category_type_id: "2", is_moderated: "true"}, {
    name: "6 Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Seated Close-Grip Concentration Barbell Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Dumbbell One-Arm Shoulder Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Tricep Dumbbell Kickback",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Standing Bent-Over Two-Arm Dumbbell Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Front Two-Dumbbell Raise", category_type_id: "6", is_moderated: "true"}, {
    name: "Mountain Climbers",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Drop Push", category_type_id: "4", is_moderated: "true"}, {
    name: "Bodyweight Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Standing Hip Circles", category_type_id: "1", is_moderated: "true"}, {
    name: "Trap Bar Deadlift",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Ring Dips", category_type_id: "1", is_moderated: "true"}, {
    name: "Parallel Bar Dip",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Chair Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "3/4 Sit-Up",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Otis-Up", category_type_id: "3", is_moderated: "true"}, {
    name: "Clam",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Machine Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Barbell Incline Bench Press Medium-Grip",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Front Cable Raise", category_type_id: "6", is_moderated: "true"}, {
    name: "Crunches",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Dumbbell Rear Lunge",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Alternate Incline Dumbbell Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Power Clean",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Wide-Grip Rear Pull-Up", category_type_id: "5", is_moderated: "true"}, {
    name: "Handstand Push-Ups",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Dumbbell Alternate Bicep Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "One Arm Dumbbell Preacher Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Bench Dips",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Standing Dumbbell Upright Row", category_type_id: "5", is_moderated: "true"}, {
    name: "Goblet Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Kettlebell Sumo High Pull", category_type_id: "5", is_moderated: "true"}, {
    name: "Trail Running/Walking",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Deadlift with Bands", category_type_id: "5", is_moderated: "true"}, {
    name: "Hang Snatch",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "External Rotation with Cable",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Incline Push-Up Medium", category_type_id: "4", is_moderated: "true"}, {
    name: "Incline Cable Chest Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Lunge Pass Through", category_type_id: "2", is_moderated: "true"}, {
    name: "Rope Straight-Arm Pulldown",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Step-up with Knee Raise", category_type_id: "2", is_moderated: "true"}, {
    name: "Bent Over Barbell Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Barbell Step Ups", category_type_id: "2", is_moderated: "true"}, {
    name: "Smith Machine Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Dumbbell Shrug", category_type_id: "5", is_moderated: "true"}, {
    name: "Reverse Cable Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Seated Calf Raise", category_type_id: "2", is_moderated: "true"}, {
    name: "Calf-Machine Shoulder Shrug",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Rocking Standing Calf Raise",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Calf Press On The Leg Press Machine",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Front Incline Dumbbell Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Barbell Shoulder Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Lying Close-Grip Barbell Triceps Extension Behind The Head",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Seated Palm-Up Barbell Wrist Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Two-Arm Kettlebell Military Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Bench Press - Powerlifting", category_type_id: "1", is_moderated: "true"}, {
    name: "Cable Chest Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Dumbbell Bench Press with Neutral Grip", category_type_id: "4", is_moderated: "true"}, {
    name: "Rope Climb",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Standing Concentration Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Standing Bradford Press", category_type_id: "6", is_moderated: "true"}, {
    name: "Dumbbell Rear Delt Row",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Palms-Up Dumbbell Wrist Curl Over A Bench",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Dumbbell Side Bend", category_type_id: "3", is_moderated: "true"}, {
    name: "Incline Dumbbell Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Cable Lying Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Close-Grip Standing Barbell Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Standing Overhead Barbell Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "One-Arm Kettlebell Clean",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Kettlebell Turkish Get-Up (Squat style)",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Deadlift with Chains", category_type_id: "5", is_moderated: "true"}, {
    name: "Rope Crunch",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Leverage Incline Chest Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Triceps Overhead Extension with Rope",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Single-Arm Push-Up", category_type_id: "4", is_moderated: "true"}, {
    name: "Standing Rope Crunch",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Single-Arm Cable Crossover", category_type_id: "4", is_moderated: "true"}, {
    name: "Dead Bug",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Standing Olympic Plate Hand Squeeze",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Wide-Grip Lat Pulldown", category_type_id: "5", is_moderated: "true"}, {
    name: "Crunch - Hands Overhead",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Preacher Hammer Dumbbell Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Alternate Hammer Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Seated Dumbbell Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "JM Press", category_type_id: "1", is_moderated: "true"}, {
    name: "Cable Internal Rotation",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Standing One-Arm Dumbbell Curl Over Incline Bench",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Knee/Hip Raise On Parallel Bars", category_type_id: "3", is_moderated: "true"}, {
    name: "Triceps Pushdown",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Side Lateral Raise", category_type_id: "6", is_moderated: "true"}, {
    name: "Alternating Kettlebell Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "One-Arm Kettlebell Snatch",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Kettlebell One-Legged Deadlift",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Kettlebell Thruster", category_type_id: "6", is_moderated: "true"}, {
    name: "Kneeling Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Front Box Jump", category_type_id: "2", is_moderated: "true"}, {
    name: "Split Squat with Dumbbells",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Band Hip Adductions", category_type_id: "1", is_moderated: "true"}, {
    name: "Sledgehammer Swings",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Decline Push-Up", category_type_id: "4", is_moderated: "true"}, {
    name: "Battling Ropes",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Hammer Grip Incline DB Bench Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Bent Over Low-Pulley Side Lateral",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Pushups (Close and Wide Hand Positions)",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Jackknife Sit-Up", category_type_id: "3", is_moderated: "true"}, {
    name: "Push-Ups With Feet Elevated",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "One Leg Barbell Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Push-Ups With Feet On An Exercise Ball",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Standing 1 Cable Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Low Pulley Row To 6",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Decline Oblique Crunch", category_type_id: "3", is_moderated: "true"}, {
    name: "External Rotation",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Stomach Vacuum", category_type_id: "3", is_moderated: "true"}, {
    name: "Barbell Rear Delt Row",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Alternating Renegade Row", category_type_id: "5", is_moderated: "true"}, {
    name: "Clean",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Good Morning", category_type_id: "2", is_moderated: "true"}, {
    name: "Wide Stance Stiff Legs",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Scissors Jump", category_type_id: "2", is_moderated: "true"}, {
    name: "Calf Press",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Seated Cable Shoulder Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Decline Dumbbell Bench Press", category_type_id: "4", is_moderated: "true"}, {
    name: "Leg Extensions",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Barbell Hack Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Underhand Cable Pulldowns",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Decline Dumbbell Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "One-Arm Incline Lateral Raise", category_type_id: "6", is_moderated: "true"}, {
    name: "Drag Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Press Sit-Up", category_type_id: "3", is_moderated: "true"}, {
    name: "Butterfly",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "One Arm Dumbbell Bench Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Gironda Sternum Chins",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Seated One-arm Cable Pulley Rows",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Dumbbell One-Arm Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Hang Clean - Below the Knees", category_type_id: "2", is_moderated: "true"}, {
    name: "Rack Pull with Bands",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Leverage Chest Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Bosu Ball Cable Crunch With Side Bends",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Incline Push-Up Wide", category_type_id: "4", is_moderated: "true"}, {
    name: "Jumping Jacks",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Lying Face Up Plate 6 Resistance",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Straight-Arm Dumbbell Pullover", category_type_id: "4", is_moderated: "true"}, {
    name: "Hack Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Barbell Shrug", category_type_id: "5", is_moderated: "true"}, {
    name: "Freehand Jump Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Tuck Crunch", category_type_id: "3", is_moderated: "true"}, {
    name: "Lying Dumbbell Tricep Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Cable Rope Rear-Delt Rows", category_type_id: "6", is_moderated: "true"}, {
    name: "Dumbbell Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Weighted Sissy Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Seated Dumbbell Palms-Down Wrist Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Front Leg Raises", category_type_id: "2", is_moderated: "true"}, {
    name: "Skating",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Face Pull", category_type_id: "6", is_moderated: "true"}, {
    name: "Glute Ham Raise",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Band Skull Crusher", category_type_id: "1", is_moderated: "true"}, {
    name: "Isometric Wipers",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Pallof Press", category_type_id: "3", is_moderated: "true"}, {
    name: "Jog In Place",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Man Maker",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Palms-Down Dumbbell Wrist Curl Over A Bench",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Incline Dumbbell Bench With Palms Facing In",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Flat Bench Lying Leg Raise",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Seated Flat Bench Leg Pull-In",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Exercise Ball Crunch", category_type_id: "3", is_moderated: "true"}, {
    name: "Russian Twist",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Side To Side Chins",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "One Arm Pronated Dumbbell Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "One Arm Supinated Dumbbell Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Seated Bent-Over Rear Delt Raise", category_type_id: "6", is_moderated: "true"}, {
    name: "Side Wrist Pull",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "One-Arm Kettlebell Clean and Jerk",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Running, Treadmill", category_type_id: "2", is_moderated: "true"}, {
    name: "Leverage Iso Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Clock Push-Up", category_type_id: "4", is_moderated: "true"}, {
    name: "Decline Smith Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Cable Iron Cross", category_type_id: "4", is_moderated: "true"}, {
    name: "Plie Dumbbell Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Incline Dumbbell Flyes - With A Twist",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Standing Low-Pulley Deltoid Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Oblique Crunches", category_type_id: "3", is_moderated: "true"}, {
    name: "Machine Preacher Curls",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Plate Pinch", category_type_id: "1", is_moderated: "true"}, {
    name: "Reverse Triceps Bench Press",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Upright Barbell Row", category_type_id: "6", is_moderated: "true"}, {
    name: "Seated Side Lateral Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Seated One-Arm Dumbbell Palms-Down Wrist Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Advanced Kettlebell Windmill",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Bottoms-Up Clean From The Hang Position",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Rack Pulls", category_type_id: "5", is_moderated: "true"}, {
    name: "Split Jerk",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Chain Handle Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Band Good Morning (Pull Through)",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Leverage Decline Chest Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Linear Acceleration Wall Drill",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Dumbbell Seated Box Jump", category_type_id: "2", is_moderated: "true"}, {
    name: "Push-Up Wide",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Barbell Rollout from Bench", category_type_id: "3", is_moderated: "true"}, {
    name: "Barbell Thruster",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Straight-Arm Pulldown", category_type_id: "5", is_moderated: "true"}, {
    name: "Bent Over Two-Dumbbell Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Stiff-Legged Barbell Deadlift", category_type_id: "2", is_moderated: "true"}, {
    name: "Glute Kickback",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Butt-Ups", category_type_id: "3", is_moderated: "true"}, {
    name: "Smith Machine Bench Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Middle Back Shrug", category_type_id: "5", is_moderated: "true"}, {
    name: "Iron Cross",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Lying Supine Dumbbell Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Barbell Shrug Behind The Back", category_type_id: "5", is_moderated: "true"}, {
    name: "Superman",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Cross Over - With Bands",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Two-Arm Dumbbell Preacher Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Flutter Kicks",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Front Raise And Pullover", category_type_id: "4", is_moderated: "true"}, {
    name: "Elevated Cable Rows",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Dumbbell Lying Rear Lateral Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Weighted Ball Hyperextension",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Standing One-Arm Dumbbell Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Bicycling, Stationary", category_type_id: "2", is_moderated: "true"}, {
    name: "Jogging-Treadmill",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Floor Press", category_type_id: "1", is_moderated: "true"}, {
    name: "Bench Press with Chains",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Chest Push (multiple response)",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Incline Push-Up Depth Jump",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Internal Rotation with Band",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Straight Raises on Incline Bench",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Kneeling Single-Arm High Pulley Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Cobra Triceps Extension", category_type_id: "1", is_moderated: "true"}, {
    name: "Bodyweight Reverse Lunge",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Yates Row Reverse Grip", category_type_id: "5", is_moderated: "true"}, {
    name: "Bent-Arm Dumbbell Pullover",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Bent Over Dumbbell Rear Delt Raise With Head On Bench",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Lying Cable Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Cable Hammer Curls - Rope Attachment",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Mixed Grip Chin", category_type_id: "5", is_moderated: "true"}, {
    name: "Cable Hip Adduction",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Lying Rear Delt Raise", category_type_id: "6", is_moderated: "true"}, {
    name: "One-Arm Kettlebell Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Recumbent Bike", category_type_id: "2", is_moderated: "true"}, {
    name: "Walking, Treadmill",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Inverted Row", category_type_id: "5", is_moderated: "true"}, {
    name: "Standing Cable Chest Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Cable Reverse Crunch", category_type_id: "3", is_moderated: "true"}, {
    name: "Body Tricep Press",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Band Pull Apart", category_type_id: "6", is_moderated: "true"}, {
    name: "Dumbbell Clean",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Butt Kicks", category_type_id: "2", is_moderated: "true"}, {
    name: "Neutral-Grip Pull Ups",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Machine-Assisted Pull-Up",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Wide-Stance Barbell Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Alien Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Reverse Barbell Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Dumbbell Step Ups",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Thigh Abductor", category_type_id: "1", is_moderated: "true"}, {
    name: "Low Cable Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Standing Barbell Calf Raise", category_type_id: "2", is_moderated: "true"}, {
    name: "Machine Bench Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Bradford/Rocky Presses",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Seated Bent-Over Two-Arm Dumbbell Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Kettlebell Turkish Get-Up (Lunge style)",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Iliotibial Tract-SMR", category_type_id: "1", is_moderated: "true"}, {
    name: "Power Jerk",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Supine Chest Throw", category_type_id: "1", is_moderated: "true"}, {
    name: "Cable Rear Delt Fly",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Reverse Machine Flyes", category_type_id: "6", is_moderated: "true"}, {
    name: "Standing Cable Wood Chop",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Anti-Gravity Press", category_type_id: "6", is_moderated: "true"}, {
    name: "Single Leg Deadlift",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Seated Back Extension", category_type_id: "5", is_moderated: "true"}, {
    name: "Pendlay Rown",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Stiff Leg Barbell Good Morning",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Full Range-Of-Motion Lat Pulldown",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Cable Rope Overhead Triceps Extension", category_type_id: "1", is_moderated: "true"}, {
    name: "Tate Press",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Back Flyes - With Bands", category_type_id: "6", is_moderated: "true"}, {
    name: "Seated Good Mornings",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Box Jump (Multiple Response)", category_type_id: "2", is_moderated: "true"}, {
    name: "Knee Tuck Jump",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Close-Grip Dumbbell Press", category_type_id: "1", is_moderated: "true"}, {
    name: "Upright Cable Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Bent-Knee Hip Raise", category_type_id: "3", is_moderated: "true"}, {
    name: "Smith Machine Bent Over Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Standing Dumbbell Reverse Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Zercher Squats",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Lying Triceps Press",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Lying Close-Grip Barbell Triceps Press To Chin",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Cable Shoulder Press", category_type_id: "6", is_moderated: "true"}, {
    name: "Cable Russian Twists",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Romanian Deadlift", category_type_id: "2", is_moderated: "true"}, {
    name: "Bent-Arm Barbell Pullover",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Lying Machine Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Machine Shoulder (Military) Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Standing Low-Pulley One-Arm Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Smith Machine Incline Bench Press", category_type_id: "4", is_moderated: "true"}, {
    name: "Cuban Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Ab Crunch Machine", category_type_id: "3", is_moderated: "true"}, {
    name: "Weighted Sit-Ups - With Bands",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Jefferson Squats", category_type_id: "2", is_moderated: "true"}, {
    name: "Windmills",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Jerk Dip Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Yoke Walk",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Rocket Jump", category_type_id: "2", is_moderated: "true"}, {
    name: "Smith Machine Leg Press",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Wind Sprints", category_type_id: "3", is_moderated: "true"}, {
    name: "Bodyweight Walking Lunge",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Smith Machine Hip Raise",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Romanian Deadlift with Kettlebell", category_type_id: "2", is_moderated: "true"}, {
    name: "Fire Hydrant",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Body-Up", category_type_id: "1", is_moderated: "true"}, {
    name: "Wall Ball Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Alternate Heel Touchers",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Smith Machine Close-Grip Bench Press",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Reverse Flyes With External Rotation",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Incline Inner 1 Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Plyo Kettlebell Pushups",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Depth Jump Leap", category_type_id: "2", is_moderated: "true"}, {
    name: "Reverse Hyperextension",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Alternating Cable Shoulder Press", category_type_id: "6", is_moderated: "true"}, {
    name: "Seated Leg Curl",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Cable Shrugs", category_type_id: "5", is_moderated: "true"}, {
    name: "Overhead Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Alternating Kettlebell Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Single-Leg Leg Extension", category_type_id: "2", is_moderated: "true"}, {
    name: "Pistol Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Lying T-Bar Row", category_type_id: "5", is_moderated: "true"}, {
    name: "Lying One-Arm Lateral Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Lateral Raise - With Bands", category_type_id: "6", is_moderated: "true"}, {
    name: "High Cable Curls",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Standing Bent-Over One-Arm Dumbbell Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Standing Barbell Press Behind 6", category_type_id: "6", is_moderated: "true"}, {
    name: "Band Good Morning",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Leverage High Row", category_type_id: "5", is_moderated: "true"}, {
    name: "Overhead Slam",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Pallof Press With Rotation",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Close-Grip EZ-Bar Curl with Band",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Kneeling High Pulley Row", category_type_id: "5", is_moderated: "true"}, {
    name: "Upright Row - With Bands",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Barbell Side Split Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Leg-Up Hamstring Stretch",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Sumo Deadlift with Chains", category_type_id: "2", is_moderated: "true"}, {
    name: "Physioball Hip Bridge",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Suspended Reverse Crunch", category_type_id: "3", is_moderated: "true"}, {
    name: "Kettlebell Pirate Ships",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Side Lunge", category_type_id: "2", is_moderated: "true"}, {
    name: "Walking High Knees",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Close-Hands Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "See-Saw Press (Alternating Side Press)",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Cable Wrist Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Dumbbell One-Arm Upright Row",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Dumbbell Scaption",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Smith Machine Behind the Back Shrug",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Flat Bench Leg Pull-In", category_type_id: "3", is_moderated: "true"}, {
    name: "Speed Squats",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Cable Seated Crunch", category_type_id: "3", is_moderated: "true"}, {
    name: "Smith Single-Leg Split Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Seated Leg Press", category_type_id: "2", is_moderated: "true"}, {
    name: "Kettlebell Curtsy Lunge",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Frog Sit-Ups", category_type_id: "3", is_moderated: "true"}, {
    name: "Kettlebell Windmill",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Kettlebell Figure 8", category_type_id: "3", is_moderated: "true"}, {
    name: "Side Leg Raises",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Board Press", category_type_id: "1", is_moderated: "true"}, {
    name: "Circus Bell",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Frankenstein Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Single Leg Butt Kick",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Cable Deadlifts", category_type_id: "2", is_moderated: "true"}, {
    name: "One-Arm Plank Dumbbell 1 Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Around The Worlds", category_type_id: "4", is_moderated: "true"}, {
    name: "Incline Bench Pull",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Dumbbell Prone Incline Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Band Assisted Pull-Up",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Suspended Row", category_type_id: "5", is_moderated: "true"}, {
    name: "Jump Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Tiger-Bend Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Seated Head Harness 6 Resistance",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Barbell Seated Calf Raise",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Shoulder Press - With Bands", category_type_id: "6", is_moderated: "true"}, {
    name: "Kneeling Jump Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Barbell Bench Press-Wide Grip",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "One-Arm Side Deadlift", category_type_id: "2", is_moderated: "true"}, {
    name: "Zottman Preacher Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Sit-Up",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Seated Bent-Over One-Arm Dumbbell Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Two-Arm Kettlebell Clean", category_type_id: "6", is_moderated: "true"}, {
    name: "Vertical Swing",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Inchworm", category_type_id: "2", is_moderated: "true"}, {
    name: "Cable Preacher Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Side Bridge", category_type_id: "3", is_moderated: "true"}, {
    name: "Balance Board",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Double Kettlebell Push Press", category_type_id: "6", is_moderated: "true"}, {
    name: "Split Jump",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Plyo Push-up", category_type_id: "4", is_moderated: "true"}, {
    name: "Bench Jump",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Isometric 6 Exercise - Sides", category_type_id: "6", is_moderated: "true"}, {
    name: "Standing Leg Curl",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Leg Pull-In", category_type_id: "3", is_moderated: "true"}, {
    name: "Standing Inner-1 Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Dumbbell Incline Shoulder Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "One-Arm Kettlebell Swings",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Machine Triceps Extension", category_type_id: "1", is_moderated: "true"}, {
    name: "Prowler Sprint",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Barbell Front Raise", category_type_id: "6", is_moderated: "true"}, {
    name: "Barbell Squat To A Bench",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Oblique Crunches - On The Floor", category_type_id: "3", is_moderated: "true"}, {
    name: "Janda Sit-Up",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Seated Dumbbell Inner 1 Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Donkey Calf Raises",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Pull Through", category_type_id: "2", is_moderated: "true"}, {
    name: "Incline Push-Up Close-Grip",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Smith Machine Upright Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Crunch - Legs On Exercise Ball", category_type_id: "3", is_moderated: "true"}, {
    name: "Side Jackknife",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Kettlebell Pass Between The Legs",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Moving Claw Series", category_type_id: "2", is_moderated: "true"}, {
    name: "Suspended Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Dumbbell Walking Lunge", category_type_id: "2", is_moderated: "true"}, {
    name: "Flat Bench Cable Flyes",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "One Arm Floor Press",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Wide-Grip Decline Barbell Pullover",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Smith Machine Reverse Calf Raises",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Two-Arm Kettlebell Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Front Cone Hops (or hurdle hops)", category_type_id: "2", is_moderated: "true"}, {
    name: "Lateral Bound",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "One Arm Lat Pulldown",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Close-Grip Push-Up off of a Dumbbell",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Smith Machine Decline Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Dumbbell Seated One-Leg Calf Raise", category_type_id: "2", is_moderated: "true"}, {
    name: "Weighted Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Clean Pull", category_type_id: "2", is_moderated: "true"}, {
    name: "Smith Machine One-Arm Upright Row",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Machine Lateral Raise", category_type_id: "6", is_moderated: "true"}, {
    name: "Cable Incline Pushdown",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Reverse Plate Curls",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Standing Front Barbell Raise Over Head",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Ankle Circles", category_type_id: "2", is_moderated: "true"}, {
    name: "Hip Extension with Bands",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Kipping Muscle Up", category_type_id: "5", is_moderated: "true"}, {
    name: "Isometric Chest Squeezes",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Cable Incline Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Lying Close-Grip Bar Curl On High Pulley",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Groin and Back Stretch", category_type_id: "1", is_moderated: "true"}, {
    name: "Push Up to Side Plank",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Wide-Grip Pulldown Behind The 6", category_type_id: "5", is_moderated: "true"}, {
    name: "Seated Leg Tucks",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Standing 1 Stretch", category_type_id: "1", is_moderated: "true"}, {
    name: "Dumbbell Squat To A Bench",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Smith Machine Stiff-Legged Deadlift", category_type_id: "2", is_moderated: "true"}, {
    name: "Scissor Kick",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Pin Presses", category_type_id: "1", is_moderated: "true"}, {
    name: "London Bridges",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "90-Degree Jump Squat Twist", category_type_id: "2", is_moderated: "true"}, {
    name: "Barbell Side Bend",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "One-Arm Overhead Kettlebell Squats",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Kettlebell Seated Press", category_type_id: "6", is_moderated: "true"}, {
    name: "Double Kettlebell Jerk",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Smith Machine Pistol Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Bench Sprint",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Wide-Stance Leg Press", category_type_id: "2", is_moderated: "true"}, {
    name: "Toe Touchers",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Backward Drag", category_type_id: "2", is_moderated: "true"}, {
    name: "Alternate Leg Diagonal Bound",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Sled Push", category_type_id: "2", is_moderated: "true"}, {
    name: "Backward Medicine Ball Throw",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Kettlebell Arnold Press", category_type_id: "6", is_moderated: "true"}, {
    name: "Clean Shrug",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Landmine Linear Jammer", category_type_id: "6", is_moderated: "true"}, {
    name: "Standing Toe Touches",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Barbell Guillotine Bench Press", category_type_id: "4", is_moderated: "true"}, {
    name: "Upward Stretch",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Shoulder Circles", category_type_id: "6", is_moderated: "true"}, {
    name: "Intermediate Groin Stretch",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Standing Hamstring and Calf Stretch", category_type_id: "2", is_moderated: "true"}, {
    name: "Monster Walk",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Squat with Plate Movers", category_type_id: "2", is_moderated: "true"}, {
    name: "Side To Side Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Piriformis-SMR", category_type_id: "2", is_moderated: "true"}, {
    name: "Scapular Pull-Up",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Cable Seated Lateral Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Standing Towel Triceps Extension", category_type_id: "1", is_moderated: "true"}, {
    name: "Lower Back Curl",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Straight Bar Bench Mid Rows", category_type_id: "5", is_moderated: "true"}, {
    name: "Leg Lift",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "One-Arm Flat Bench Dumbbell Flye",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Barbell Incline Shoulder Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Push Press - Behind the 6", category_type_id: "6", is_moderated: "true"}, {
    name: "Suspended Split Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Isometric 6 Exercise - Front And Back",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Weighted Ball Side Bend", category_type_id: "3", is_moderated: "true"}, {
    name: "Inverted Row with Straps",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Peroneals-SMR", category_type_id: "2", is_moderated: "true"}, {
    name: "Atlas Stone Trainer",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Knee Circles",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Intermediate Hip Flexor and Quad Stretch",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Lying Hamstring",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Dumbbell Tricep Extension -Pronated Grip",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Bent Press", category_type_id: "3", is_moderated: "true"}, {
    name: "One Arm Chin-Up",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Extended Range One-Arm Kettlebell Floor Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Seated Dumbbell Palms-Up Wrist Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Lying Cambered Barbell Row", category_type_id: "5", is_moderated: "true"}, {
    name: "Snatch Shrug",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Sit Squats", category_type_id: "2", is_moderated: "true"}, {
    name: "Crucifix",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Hip Lift with Band", category_type_id: "2", is_moderated: "true"}, {
    name: "Alternating Floor Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Calves-SMR", category_type_id: "2", is_moderated: "true"}, {
    name: "Forward Drag with Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Shoulder Stretch", category_type_id: "6", is_moderated: "true"}, {
    name: "Hamstring Stretch",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Medicine Ball Chest Pass",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Lying High Bench Barbell Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "One Arm Against Wall", category_type_id: "5", is_moderated: "true"}, {
    name: "Squats - With Bands",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Dynamic Chest Stretch", category_type_id: "4", is_moderated: "true"}, {
    name: "Catch and Overhead Throw",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Smith Machine Hang Power Clean", category_type_id: "2", is_moderated: "true"}, {
    name: "Rhomboids-SMR",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "6 Bridge Prone",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Standing Gastrocnemius Calf Stretch", category_type_id: "2", is_moderated: "true"}, {
    name: "Chain Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Bench Press - With Bands", category_type_id: "4", is_moderated: "true"}, {
    name: "Tricep Side Stretch",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Hamstring-SMR", category_type_id: "2", is_moderated: "true"}, {
    name: "Fast Skipping",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Hug A Ball", category_type_id: "5", is_moderated: "true"}, {
    name: "Dancer's Stretch",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Upper Back-Leg Grab", category_type_id: "2", is_moderated: "true"}, {
    name: "Kneeling Hip Flexor",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Quadriceps-SMR", category_type_id: "2", is_moderated: "true"}, {
    name: "Front Barbell Squat To A Bench",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Calf Stretch Hands Against Wall",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Kettlebell Dead Clean",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Supine Two-Arm Overhead Throw",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Partner Farmer's Walk Competition", category_type_id: "1", is_moderated: "true"}, {
    name: "Child's Pose",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Round The World Shoulder Stretch", category_type_id: "6", is_moderated: "true"}, {
    name: "Spinal Stretch",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Elbow Circles", category_type_id: "6", is_moderated: "true"}, {
    name: "Seated Hamstring",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Anterior Tibialis-SMR", category_type_id: "2", is_moderated: "true"}, {
    name: "Kneeling Forearm Stretch",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Iron Crosses (stretch)", category_type_id: "2", is_moderated: "true"}, {
    name: "Pelvic Tilt Into Bridge",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Seated Floor Hamstring Stretch", category_type_id: "2", is_moderated: "true"}, {
    name: "Arm Circles",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "90/90 Hamstring", category_type_id: "2", is_moderated: "true"}, {
    name: "All Fours Quad Stretch",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Seated 1", category_type_id: "1", is_moderated: "true"}, {
    name: "Torso Rotation",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Cable Judo Flip", category_type_id: "3", is_moderated: "true"}, {
    name: "Cat Stretch",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Calf Stretch Elbows Against Wall",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Behind Head Chest Stretch", category_type_id: "4", is_moderated: "true"}, {
    name: "Quad Stretch",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Alternating Hang Clean", category_type_id: "2", is_moderated: "true"}, {
    name: "Calf Raises - With Bands",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Side 6 Stretch", category_type_id: "6", is_moderated: "true"}, {
    name: "Elbows Back",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Dynamic Back Stretch", category_type_id: "5", is_moderated: "true"}, {
    name: "Calf Raise On A Dumbbell",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Ankle On The Knee", category_type_id: "2", is_moderated: "true"}, {
    name: "Triceps Stretch",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Seated Barbell Twist", category_type_id: "3", is_moderated: "true"}, {
    name: "One Half Locust",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Latissimus Dorsi-SMR", category_type_id: "5", is_moderated: "true"}, {
    name: "Seated Overhead Stretch",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Lower Back-SMR", category_type_id: "5", is_moderated: "true"}, {
    name: "Seated Front Deltoid",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Hug Knees To Chest", category_type_id: "5", is_moderated: "true"}, {
    name: "Standing Hip Flexors",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "World's Greatest Stretch", category_type_id: "2", is_moderated: "true"}, {
    name: "Looking At Ceiling",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "The Straddle", category_type_id: "2", is_moderated: "true"}, {
    name: "Upper Back Stretch",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Split Squats", category_type_id: "2", is_moderated: "true"}, {
    name: "Standing Soleus And Achilles Stretch",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Seated Calf Stretch", category_type_id: "2", is_moderated: "true"}, {
    name: "Shoulder Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Chair Upper Body Stretch", category_type_id: "6", is_moderated: "true"}, {
    name: "Downward Facing Balance",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Adductor", category_type_id: "1", is_moderated: "true"}, {
    name: "Kneeling Arm Drill",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Frog Hops", category_type_id: "2", is_moderated: "true"}, {
    name: "Overhead Stretch",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Crossover Reverse Lunge", category_type_id: "5", is_moderated: "true"}, {
    name: "Middle Back Stretch",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Side Lying Groin Stretch", category_type_id: "1", is_moderated: "true"}, {
    name: "Chin To Chest Stretch",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Chest Stretch on Stability Ball", category_type_id: "4", is_moderated: "true"}, {
    name: "Runner's Stretch",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "One Handed Hang", category_type_id: "5", is_moderated: "true"}, {
    name: "Adductor/Groin",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Knee Across The Body", category_type_id: "2", is_moderated: "true"}, {
    name: "Medicine Ball Scoop Throw",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Rear Leg Raises", category_type_id: "2", is_moderated: "true"}, {
    name: "Peroneals Stretch",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "On Your Side Quad Stretch",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "On-Your-Back Quad Stretch", category_type_id: "2", is_moderated: "true"}, {
    name: "Overhead Triceps",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Brachialis-SMR", category_type_id: "1", is_moderated: "true"}, {
    name: "Standing Lateral Stretch",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Chair Lower Back Stretch", category_type_id: "5", is_moderated: "true"}, {
    name: "Standing Pelvic Tilt",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Seated Glute", category_type_id: "2", is_moderated: "true"}, {
    name: "Chest And Front Of Shoulder Stretch",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Double Kettlebell Windmill", category_type_id: "3", is_moderated: "true"}, {
    name: "Lying Glute",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Lying Bent Leg Groin", category_type_id: "1", is_moderated: "true"}, {
    name: "6-SMR",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Standing Elevated Quad Stretch", category_type_id: "2", is_moderated: "true"}, {
    name: "Foot-SMR",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "One Knee To Chest", category_type_id: "2", is_moderated: "true"}, {
    name: "Wrist Circles",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Chair Leg Extended Stretch",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "IT Band and Glute Stretch", category_type_id: "1", is_moderated: "true"}, {
    name: "Lying Crossover",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Pyramid", category_type_id: "5", is_moderated: "true"}, {
    name: "Overhead Lat",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Posterior Tibialis Stretch",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Side-Lying Floor Stretch",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Smith Incline Shoulder Raise",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "One-Arm Kettlebell Jerk",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "One-Arm Kettlebell Floor Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "One-Arm Kettlebell Military Press To The Side",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "One-Arm Kettlebell Para Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Two-Arm Kettlebell Jerk",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "One-Arm Kettlebell Split Jerk",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Leg-Over Floor Press", category_type_id: "4", is_moderated: "true"}, {
    name: "Kettlebell Hang Clean",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Double Kettlebell Alternating Hang Clean",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Seated Hamstring and Calf Stretch",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Box Squat with Chains", category_type_id: "2", is_moderated: "true"}, {
    name: "Car Deadlift",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Good Morning off Pins", category_type_id: "2", is_moderated: "true"}, {
    name: "Hang Snatch - Below Knees",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Jerk Balance", category_type_id: "6", is_moderated: "true"}, {
    name: "Keg Load",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Log Lift", category_type_id: "6", is_moderated: "true"}, {
    name: "Muscle Snatch",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Squat with Bands", category_type_id: "2", is_moderated: "true"}, {
    name: "Sandbag Load",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Sled Drag - Harness", category_type_id: "2", is_moderated: "true"}, {
    name: "Squat Jerk",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Sumo Deadlift with Bands", category_type_id: "2", is_moderated: "true"}, {
    name: "Floor Press with Chains",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Snatch from Blocks", category_type_id: "2", is_moderated: "true"}, {
    name: "Split Clean",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Reverse Band Deadlift", category_type_id: "5", is_moderated: "true"}, {
    name: "Reverse Band Power Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Reverse Band Sumo Deadlift",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Chest Push (single response)", category_type_id: "4", is_moderated: "true"}, {
    name: "Hurdle Hops",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Return Push from Stance", category_type_id: "6", is_moderated: "true"}, {
    name: "Side Hop-Sprint",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Single-Leg Hop Progression",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Standing Two-Arm Overhead Throw",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Stride Jump Crossover", category_type_id: "2", is_moderated: "true"}, {
    name: "External Rotation with Band",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Hip Flexion with Band", category_type_id: "2", is_moderated: "true"}, {
    name: "Carioca Quick Step",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Side Standing Long Jump", category_type_id: "2", is_moderated: "true"}, {
    name: "Single-Cone Sprint Drill",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Linear 3-Part Start Technique", category_type_id: "2", is_moderated: "true"}, {
    name: "Linear Depth Jump",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Elevated Back Lunge", category_type_id: "2", is_moderated: "true"}, {
    name: "Seated Band Hamstring Curl",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Speed Band Overhead Triceps",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Sled Overhead Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Incline Push-Up Reverse Grip", category_type_id: "4", is_moderated: "true"}, {
    name: "Lunge Sprint",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Single Arm Overhead Kettlebell Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Suitcase Crunch", category_type_id: "3", is_moderated: "true"}, {
    name: "Glute Bridge Hamstring Walkout",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Seated Scissor Kick", category_type_id: "3", is_moderated: "true"}, {
    name: "Punches",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Slow Jog", category_type_id: "2", is_moderated: "true"}, {
    name: "Defensive Slide",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Cross Crunch", category_type_id: "3", is_moderated: "true"}, {
    name: "Square Hop",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Lateral Speed Step", category_type_id: "2", is_moderated: "true"}, {
    name: "Stiff-Legged Deadlift",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Suspended Chest Fly", category_type_id: "4", is_moderated: "true"}, {
    name: "Staggered Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "JM Press", category_type_id: "1", is_moderated: "true"}, {
    name: "JM Press With Bands",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Rockers (Pullover To Press) Straight Bar",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Straight-Legged Hip Raise",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Partner Supermans With Alternating High-Five",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Partner Facing Planks With Alternating High-Five",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Partner Facing Side Plank With Band Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Partner Facing Feet-Elevated Side Plank With Band Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Partner 3-Touch Motion Russian Twist",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Partner Side-To-Side Russian Twist &amp; Pass",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Medicine-Ball Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Partner Flat-Bench Back Extension",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Partner Flat-Bench Back Extension With Hold",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Partner Hanging Knee Raise With Throw Down",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Partner Hanging Knee Raise With Manual Resistance",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Lateral Band Walk",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Dumbbell Squat To Shoulder Press",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Dumbbell Overhead Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Single-Arm Dumbbell Overhead Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Single-Arm Landmine Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Medicine Ball Rotational Throw", category_type_id: "3", is_moderated: "true"}, {
    name: "Suspended Leg Curl",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Suspended Crunch", category_type_id: "3", is_moderated: "true"}, {
    name: "Bosu Ball Crunch",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Triceps Plank Extension", category_type_id: "1", is_moderated: "true"}, {
    name: "Single-Leg Box Jump",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Suspended Push-Up", category_type_id: "4", is_moderated: "true"}, {
    name: "Burpee Over Barbell",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Hand Stand Push-Up", category_type_id: "6", is_moderated: "true"}, {
    name: "Kettlebell Sumo Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Barbell Reverse Lunge", category_type_id: "2", is_moderated: "true"}, {
    name: "Assisted Chin-Up",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Stability Ball Pike With Knee Tuck",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Diamond Push-Up", category_type_id: "4", is_moderated: "true"}, {
    name: "Typewriter Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Clapping Push-Up", category_type_id: "4", is_moderated: "true"}, {
    name: "Triple Clap Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Weighted Push-Up", category_type_id: "4", is_moderated: "true"}, {
    name: "One-Arm Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Close-Stance Dumbbell Front Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Front Dumbbell Raise Using Towel", category_type_id: "6", is_moderated: "true"}, {
    name: "Shoulder Tap",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "V-Sit Dumbbell Triceps Extension",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Barbell Deadlift Bent Row Complex",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Bar Throw And Press", category_type_id: "4", is_moderated: "true"}, {
    name: "Decline Explosive Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Around The World Pull-Up",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Plate Shoulder Circle Big To Small Rotation",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Double Under", category_type_id: "2", is_moderated: "true"}, {
    name: "Jumping Jack",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Sprawl Frog Kick", category_type_id: "2", is_moderated: "true"}, {
    name: "Kettlebell Side Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Kettlebell Thruster Progression",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Kettlebell 3-Point Extension", category_type_id: "3", is_moderated: "true"}, {
    name: "Running Lunge",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Southpaw Sprawl", category_type_id: "2", is_moderated: "true"}, {
    name: "Bear Crawl Fire Feet",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Breakdancer", category_type_id: "3", is_moderated: "true"}, {
    name: "Lunge Heel Kick",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Gorilla Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Burppe Box Jump",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Dumbbell Squat Snatch", category_type_id: "2", is_moderated: "true"}, {
    name: "Wall Shoulder Tap",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Bear Crawl Shoulder Tap", category_type_id: "6", is_moderated: "true"}, {
    name: "Thigh Killa",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Double Kettlebell Snatch",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "One-Arm Kettlebell Split Snatch",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Open Palm Kettlebell Clean",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "One-Arm Open Palm Kettlebell Clean",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Kettlebell Seesaw Press", category_type_id: "6", is_moderated: "true"}, {
    name: "Lying Prone Quadriceps",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Conan's Wheel", category_type_id: "2", is_moderated: "true"}, {
    name: "Hanging Bar Good Morning",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Heaving Snatch Balance", category_type_id: "2", is_moderated: "true"}, {
    name: "Power Stairs",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Rack Delivery", category_type_id: "6", is_moderated: "true"}, {
    name: "Snatch Balance",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Snatch Pull", category_type_id: "2", is_moderated: "true"}, {
    name: "Split Snatch",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Rickshaw Deadlift", category_type_id: "2", is_moderated: "true"}, {
    name: "Power Snatch from Blocks",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Reverse Band Bench Press", category_type_id: "1", is_moderated: "true"}, {
    name: "Box Skip",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Chest Push from 3 point stance",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Chest Push with Run Release", category_type_id: "4", is_moderated: "true"}, {
    name: "Heavy Bag Thrust",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Lateral Box Jump", category_type_id: "1", is_moderated: "true"}, {
    name: "Lateral Cone Hops",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Medicine Ball Full Twist", category_type_id: "3", is_moderated: "true"}, {
    name: "Quick Leap",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Single-Leg Lateral Hop", category_type_id: "2", is_moderated: "true"}, {
    name: "Single-Leg Stride Jump",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Star Jump", category_type_id: "2", is_moderated: "true"}, {
    name: "Supine One-Arm Overhead Throw",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Leverage Deadlift", category_type_id: "2", is_moderated: "true"}, {
    name: "Sled Overhead Backward Walk",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Sled Reverse Flye", category_type_id: "6", is_moderated: "true"}, {
    name: "Sled Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Platform Hamstring Slides", category_type_id: "2", is_moderated: "true"}, {
    name: "Prone Manual Hamstring",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Speed Box Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "6 Bridge Supine",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Feet Jack", category_type_id: "2", is_moderated: "true"}, {
    name: "Fast Kick With Arm Circles",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Football Up-Down", category_type_id: "2", is_moderated: "true"}, {
    name: "Jump Lunge To Feet Jack",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Vertical Mountain Climber", category_type_id: "2", is_moderated: "true"}, {
    name: "Seated Glute Stretch",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Slide Jump Shot", category_type_id: "2", is_moderated: "true"}, {
    name: "High Knee Jog",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Axle Clean And Press", category_type_id: "6", is_moderated: "true"}, {
    name: "Suspended Back Fly",
    category_type_id: "6",
    is_moderated: "true"
  }, {
    name: "Front-To-Back Squat With Belt",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Bench Press With Short Bands", category_type_id: "4", is_moderated: "true"}, {
    name: "Plate Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Bosu Ball Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Partner Target Sit-Up",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Partner Lying Leg Raise With Throw Down",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Partner Facing Plank With Band Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Partner Sit-Up With High-Five",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Partner Lying Leg Raise With Lateral Throw Down",
    category_type_id: "3",
    is_moderated: "true"
  }, {
    name: "Partner Suitcase Carry Competition",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Alternating Leg Swing", category_type_id: "2", is_moderated: "true"}, {
    name: "Hip Circle",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Forward Band Walk", category_type_id: "2", is_moderated: "true"}, {
    name: "Tall Muscle Snatch",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Barbell Squat To A Box", category_type_id: "2", is_moderated: "true"}, {
    name: "Suspended Curl",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Suspended Hip Thrust", category_type_id: "2", is_moderated: "true"}, {
    name: "Suspended Triceps Press",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Bosu Ball Push-Up", category_type_id: "4", is_moderated: "true"}, {
    name: "Single-Leg Balance",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Half-kneeling Dumbbell Shoulder Press", category_type_id: "6", is_moderated: "true"}, {
    name: "Ice Skater",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Suitcase Dumbbell Carry", category_type_id: "1", is_moderated: "true"}, {
    name: "Yates Row",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Split Squat With Kettlebells", category_type_id: "2", is_moderated: "true"}, {
    name: "Burpee Pull-Up",
    category_type_id: "5",
    is_moderated: "true"
  }, {name: "Negative Pull-Up", category_type_id: "5", is_moderated: "true"}, {
    name: "Hand Release Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Wall Walk", category_type_id: "6", is_moderated: "true"}, {
    name: "Dumbbell Clean And Jerk",
    category_type_id: "6",
    is_moderated: "true"
  }, {name: "Walking Lunge With Overhead Weight", category_type_id: "2", is_moderated: "true"}, {
    name: "Waiter's Carry",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "High Kick", category_type_id: "2", is_moderated: "true"}, {
    name: "Knee To Chest",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Hip Stretch With Twist", category_type_id: "2", is_moderated: "true"}, {
    name: "Svend Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Wide-Hands Push-Up", category_type_id: "4", is_moderated: "true"}, {
    name: "Dive Bomber Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Front Squat Push Press", category_type_id: "2", is_moderated: "true"}, {
    name: "Front Squat (Bodybuilder)",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Front Squat (Clean Grip)", category_type_id: "2", is_moderated: "true"}, {
    name: "Single-Leg Squat To Box",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Single-Leg Skater Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Wall Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Pop Squat", category_type_id: "2", is_moderated: "true"}, {
    name: "Trap Bar Jump",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Skin The Cat To Push-Up", category_type_id: "3", is_moderated: "true"}, {
    name: "Weighted Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Dumbbell Lunge and Curl", category_type_id: "2", is_moderated: "true"}, {
    name: "Dumbbell Jump Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Lateral Hop Holding Dumbbells",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Dumbbell Front Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Lateral Hop 4 Times To Sprint", category_type_id: "2", is_moderated: "true"}, {
    name: "L-Sit Chin-Up",
    category_type_id: "5",
    is_moderated: "true"
  }, {
    name: "Body Triceps Press Using Flat Bench",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Russian Bar Dip",
    category_type_id: "1",
    is_moderated: "true"
  }, {
    name: "Wide-Stance Jump Squat To Close-Stance",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Bar Push-Up Smith Machine",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "Burpee To Medicine Ball Press",
    category_type_id: "4",
    is_moderated: "true"
  }, {
    name: "V-Sit Lying Down Ball Throw And Catch",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Superman Push-Up", category_type_id: "4", is_moderated: "true"}, {
    name: "Barbell Front-To-Back Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Seated Straight-Bar Curl Superset",
    category_type_id: "1",
    is_moderated: "true"
  }, {name: "Hammer Plate Curl", category_type_id: "1", is_moderated: "true"}, {
    name: "Dumbbell Pistol Squat",
    category_type_id: "2",
    is_moderated: "true"
  }, {
    name: "Dumbbell Alternating Lunge",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Side Lunge Touching Heel", category_type_id: "2", is_moderated: "true"}, {
    name: "Jumping Knee Up Down",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Burpee Tuck Jump", category_type_id: "2", is_moderated: "true"}, {
    name: "Wall Sprawl",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Wall Mountain Climber", category_type_id: "3", is_moderated: "true"}, {
    name: "Full Moon",
    category_type_id: "3",
    is_moderated: "true"
  }, {name: "Medicine Ball Ninja", category_type_id: "2", is_moderated: "true"}, {
    name: "Kettlebell Fire Feet",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Reverse Burpee", category_type_id: "2", is_moderated: "true"}, {
    name: "Feet-Elevated TRX Push-Up",
    category_type_id: "4",
    is_moderated: "true"
  }, {name: "Crab Toe Touch", category_type_id: "3", is_moderated: "true"}, {
    name: "Kettlebell Squat Clean",
    category_type_id: "2",
    is_moderated: "true"
  }, {name: "Burpee Box Jump", category_type_id: "2", is_moderated: "true"},
  ]).returning('id').then((exIds)=> {
    ids = exIds;
    return knex('exercises_muscles').insert([{muscle_id: "2", exercise_id: ids[0], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[2], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[3],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[4], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[5],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[6], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[7],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[8], muscle_group: "1"}, {
      muscle_id: "2",
      exercise_id: ids[9],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[10], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[11],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[12], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[13],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[14], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[15],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[16], muscle_group: "1"}, {
      muscle_id: "2",
      exercise_id: ids[17],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[18], muscle_group: "1"}, {
      muscle_id: "14",
      exercise_id: ids[19],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[20], muscle_group: "1"}, {
      muscle_id: "5",
      exercise_id: ids[21],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[22], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[23],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[24], muscle_group: "1"}, {
      muscle_id: "2",
      exercise_id: ids[25],
      muscle_group: "1"
    }, {muscle_id: "6", exercise_id: ids[26], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[27],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[28], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[29],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[30], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[31],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[32], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[33],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[34], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[35],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[36], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[37],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[38], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[39],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[40], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[41],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[42], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[43],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[44], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[45],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[46], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[47],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[48], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[49],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[50], muscle_group: "1"}, {
      muscle_id: "11",
      exercise_id: ids[51],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[52], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[53],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[54], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[55],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[56], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[57],
      muscle_group: "1"
    }, {muscle_id: "16", exercise_id: ids[58], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[59],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[60], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[61],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[62], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[63],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[64], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[65],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[66], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[67],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[68], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[69],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[70], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[71],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[72], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[73],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[74], muscle_group: "1"}, {
      muscle_id: "14",
      exercise_id: ids[75],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[76], muscle_group: "1"}, {
      muscle_id: "5",
      exercise_id: ids[77],
      muscle_group: "1"
    }, {muscle_id: "11", exercise_id: ids[78], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[79],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[80], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[81],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[82], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[83],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[84], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[85],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[86], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[87],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[88], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[89],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[90], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[91],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[92], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[93],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[94], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[95],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[96], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[97],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[98], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[99],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[100], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[101],
      muscle_group: "1"
    }, {muscle_id: "15", exercise_id: ids[102], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[103],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[104], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[105],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[106], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[107],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[108], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[109],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[110], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[111],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[112], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[113],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[114], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[115],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[116], muscle_group: "1"}, {
      muscle_id: "2",
      exercise_id: ids[117],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[118], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[119],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[120], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[121],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[122], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[123],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[124], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[125],
      muscle_group: "1"
    }, {muscle_id: "9", exercise_id: ids[126], muscle_group: "1"}, {
      muscle_id: "14",
      exercise_id: ids[127],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[128], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[129],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[130], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[131],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[132], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[133],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[134], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[135],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[136], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[137],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[138], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[139],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[140], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[141],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[142], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[143],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[144], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[145],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[146], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[147],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[148], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[149],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[150], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[151],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[152], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[153],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[154], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[155],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[156], muscle_group: "1"}, {
      muscle_id: "2",
      exercise_id: ids[157],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[158], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[159],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[160], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[161],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[162], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[163],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[164], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[165],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[166], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[167],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[168], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[169],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[170], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[171],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[172], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[173],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[174], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[175],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[176], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[177],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[178], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[179],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[180], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[181],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[182], muscle_group: "1"}, {
      muscle_id: "5",
      exercise_id: ids[183],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[184], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[185],
      muscle_group: "1"
    }, {muscle_id: "14", exercise_id: ids[186], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[187],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[188], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[189],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[190], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[191],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[192], muscle_group: "1"}, {
      muscle_id: "15",
      exercise_id: ids[193],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[194], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[195],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[196], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[197],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[198], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[199],
      muscle_group: "1"
    }, {muscle_id: "14", exercise_id: ids[200], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[201],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[202], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[203],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[204], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[205],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[206], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[207],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[208], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[209],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[210], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[211],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[212], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[213],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[214], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[215],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[216], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[217],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[218], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[219],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[220], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[221],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[222], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[223],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[224], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[225],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[226], muscle_group: "1"}, {
      muscle_id: "16",
      exercise_id: ids[227],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[228], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[229],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[230], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[231],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[232], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[233],
      muscle_group: "1"
    }, {muscle_id: "16", exercise_id: ids[234], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[235],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[236], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[237],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[238], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[239],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[240], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[241],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[242], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[243],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[244], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[245],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[246], muscle_group: "1"}, {
      muscle_id: "11",
      exercise_id: ids[247],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[248], muscle_group: "1"}, {
      muscle_id: "11",
      exercise_id: ids[249],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[250], muscle_group: "1"}, {
      muscle_id: "5",
      exercise_id: ids[251],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[252], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[253],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[254], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[255],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[256], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[257],
      muscle_group: "1"
    }, {muscle_id: "14", exercise_id: ids[258], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[259],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[260], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[261],
      muscle_group: "1"
    }, {muscle_id: "11", exercise_id: ids[262], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[263],
      muscle_group: "1"
    }, {muscle_id: "9", exercise_id: ids[264], muscle_group: "1"}, {
      muscle_id: "11",
      exercise_id: ids[265],
      muscle_group: "1"
    }, {muscle_id: "9", exercise_id: ids[266], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[267],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[268], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[269],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[270], muscle_group: "1"}, {
      muscle_id: "2",
      exercise_id: ids[271],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[272], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[273],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[274], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[275],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[276], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[277],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[278], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[279],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[280], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[281],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[282], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[283],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[284], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[285],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[286], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[287],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[288], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[289],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[290], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[291],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[292], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[293],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[294], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[295],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[296], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[297],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[298], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[299],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[300], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[301],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[302], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[303],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[304], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[305],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[306], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[307],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[308], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[309],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[310], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[311],
      muscle_group: "1"
    }, {muscle_id: "14", exercise_id: ids[312], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[313],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[314], muscle_group: "1"}, {
      muscle_id: "15",
      exercise_id: ids[315],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[316], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[317],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[318], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[319],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[320], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[321],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[322], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[323],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[324], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[325],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[326], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[327],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[328], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[329],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[330], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[331],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[332], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[333],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[334], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[335],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[336], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[337],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[338], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[339],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[340], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[341],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[342], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[343],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[344], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[345],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[346], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[347],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[348], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[349],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[350], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[351],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[352], muscle_group: "1"}, {
      muscle_id: "5",
      exercise_id: ids[353],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[354], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[355],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[356], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[357],
      muscle_group: "1"
    }, {muscle_id: "6", exercise_id: ids[358], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[359],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[360], muscle_group: "1"}, {
      muscle_id: "11",
      exercise_id: ids[361],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[362], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[363],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[364], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[365],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[366], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[367],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[368], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[369],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[370], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[371],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[372], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[373],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[374], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[375],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[376], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[377],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[378], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[379],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[380], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[381],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[382], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[383],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[384], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[385],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[386], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[387],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[388], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[389],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[390], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[391],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[392], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[393],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[394], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[395],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[396], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[397],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[398], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[399],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[400], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[401],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[402], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[403],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[404], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[405],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[406], muscle_group: "1"}, {
      muscle_id: "5",
      exercise_id: ids[407],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[408], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[409],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[410], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[411],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[412], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[413],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[414], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[415],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[416], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[417],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[418], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[419],
      muscle_group: "1"
    }, {muscle_id: "14", exercise_id: ids[420], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[421],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[422], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[423],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[424], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[425],
      muscle_group: "1"
    }, {muscle_id: "11", exercise_id: ids[426], muscle_group: "1"}, {
      muscle_id: "5",
      exercise_id: ids[427],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[428], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[429],
      muscle_group: "1"
    }, {muscle_id: "14", exercise_id: ids[430], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[431],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[432], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[433],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[434], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[435],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[436], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[437],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[438], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[439],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[440], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[441],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[442], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[443],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[444], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[445],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[446], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[447],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[448], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[449],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[450], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[451],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[452], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[453],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[454], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[455],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[456], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[457],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[458], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[459],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[460], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[461],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[462], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[463],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[464], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[465],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[466], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[467],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[468], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[469],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[470], muscle_group: "1"}, {
      muscle_id: "16",
      exercise_id: ids[471],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[472], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[473],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[474], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[475],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[476], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[477],
      muscle_group: "1"
    }, {muscle_id: "16", exercise_id: ids[478], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[479],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[480], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[481],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[482], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[483],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[484], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[485],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[486], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[487],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[488], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[489],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[490], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[491],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[492], muscle_group: "1"}, {
      muscle_id: "5",
      exercise_id: ids[493],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[494], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[495],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[496], muscle_group: "1"}, {
      muscle_id: "11",
      exercise_id: ids[497],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[498], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[499],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[500], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[501],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[502], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[503],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[504], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[505],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[506], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[507],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[508], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[509],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[510], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[511],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[512], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[513],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[514], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[515],
      muscle_group: "1"
    }, {muscle_id: "16", exercise_id: ids[516], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[517],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[518], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[519],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[520], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[521],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[522], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[523],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[524], muscle_group: "1"}, {
      muscle_id: "16",
      exercise_id: ids[525],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[526], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[527],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[528], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[529],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[530], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[531],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[532], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[533],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[534], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[535],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[536], muscle_group: "1"}, {
      muscle_id: "11",
      exercise_id: ids[537],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[538], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[539],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[540], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[541],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[542], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[543],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[544], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[545],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[546], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[547],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[548], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[549],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[550], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[551],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[552], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[553],
      muscle_group: "1"
    }, {muscle_id: "11", exercise_id: ids[554], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[555],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[556], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[557],
      muscle_group: "1"
    }, {muscle_id: "14", exercise_id: ids[558], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[559],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[560], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[561],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[562], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[563],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[564], muscle_group: "1"}, {
      muscle_id: "2",
      exercise_id: ids[565],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[566], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[567],
      muscle_group: "1"
    }, {muscle_id: "11", exercise_id: ids[568], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[569],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[570], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[571],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[572], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[573],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[574], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[575],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[576], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[577],
      muscle_group: "1"
    }, {muscle_id: "15", exercise_id: ids[578], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[579],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[580], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[581],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[582], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[583],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[584], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[585],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[586], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[587],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[588], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[589],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[590], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[591],
      muscle_group: "1"
    }, {muscle_id: "6", exercise_id: ids[592], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[593],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[594], muscle_group: "1"}, {
      muscle_id: "14",
      exercise_id: ids[595],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[596], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[597],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[598], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[599],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[600], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[601],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[602], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[603],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[604], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[605],
      muscle_group: "1"
    }, {muscle_id: "9", exercise_id: ids[606], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[607],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[608], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[609],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[610], muscle_group: "1"}, {
      muscle_id: "6",
      exercise_id: ids[611],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[612], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[613],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[614], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[615],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[616], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[617],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[618], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[619],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[620], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[621],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[622], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[623],
      muscle_group: "1"
    }, {muscle_id: "9", exercise_id: ids[624], muscle_group: "1"}, {
      muscle_id: "14",
      exercise_id: ids[625],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[626], muscle_group: "1"}, {
      muscle_id: "11",
      exercise_id: ids[627],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[628], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[629],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[630], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[631],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[632], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[633],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[634], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[635],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[636], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[637],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[638], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[639],
      muscle_group: "1"
    }, {muscle_id: "15", exercise_id: ids[640], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[641],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[642], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[643],
      muscle_group: "1"
    }, {muscle_id: "9", exercise_id: ids[644], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[645],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[646], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[647],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[648], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[649],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[650], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[651],
      muscle_group: "1"
    }, {muscle_id: "9", exercise_id: ids[652], muscle_group: "1"}, {
      muscle_id: "14",
      exercise_id: ids[653],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[654], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[655],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[656], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[657],
      muscle_group: "1"
    }, {muscle_id: "15", exercise_id: ids[658], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[659],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[660], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[661],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[662], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[663],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[664], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[665],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[666], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[667],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[668], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[669],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[670], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[671],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[672], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[673],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[674], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[675],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[676], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[677],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[678], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[679],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[680], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[681],
      muscle_group: "1"
    }, {muscle_id: "11", exercise_id: ids[682], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[683],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[684], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[685],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[686], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[687],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[688], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[689],
      muscle_group: "1"
    }, {muscle_id: "16", exercise_id: ids[690], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[691],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[692], muscle_group: "1"}, {
      muscle_id: "14",
      exercise_id: ids[693],
      muscle_group: "1"
    }, {muscle_id: "11", exercise_id: ids[694], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[695],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[696], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[697],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[698], muscle_group: "1"}, {
      muscle_id: "14",
      exercise_id: ids[699],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[700], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[701],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[702], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[703],
      muscle_group: "1"
    }, {muscle_id: "6", exercise_id: ids[704], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[705],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[706], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[707],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[708], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[709],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[710], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[711],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[712], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[713],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[714], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[715],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[716], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[717],
      muscle_group: "1"
    }, {muscle_id: "11", exercise_id: ids[718], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[719],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[720], muscle_group: "1"}, {
      muscle_id: "14",
      exercise_id: ids[721],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[722], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[723],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[724], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[725],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[726], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[727],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[728], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[729],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[730], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[731],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[732], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[733],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[734], muscle_group: "1"}, {
      muscle_id: "6",
      exercise_id: ids[735],
      muscle_group: "1"
    }, {muscle_id: "9", exercise_id: ids[736], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[737],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[738], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[739],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[740], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[741],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[742], muscle_group: "1"}, {
      muscle_id: "5",
      exercise_id: ids[743],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[744], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[745],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[746], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[747],
      muscle_group: "1"
    }, {muscle_id: "9", exercise_id: ids[748], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[749],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[750], muscle_group: "1"}, {
      muscle_id: "2",
      exercise_id: ids[751],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[752], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[753],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[754], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[755],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[756], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[757],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[758], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[759],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[760], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[761],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[762], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[763],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[764], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[765],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[766], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[767],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[768], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[769],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[770], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[771],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[772], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[773],
      muscle_group: "1"
    }, {muscle_id: "6", exercise_id: ids[774], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[775],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[776], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[777],
      muscle_group: "1"
    }, {muscle_id: "14", exercise_id: ids[778], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[779],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[780], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[781],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[782], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[783],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[784], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[785],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[786], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[787],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[788], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[789],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[790], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[791],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[792], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[793],
      muscle_group: "1"
    }, {muscle_id: "9", exercise_id: ids[794], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[795],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[796], muscle_group: "1"}, {
      muscle_id: "14",
      exercise_id: ids[797],
      muscle_group: "1"
    }, {muscle_id: "15", exercise_id: ids[798], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[799],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[800], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[801],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[802], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[803],
      muscle_group: "1"
    }, {muscle_id: "15", exercise_id: ids[804], muscle_group: "1"}, {
      muscle_id: "6",
      exercise_id: ids[805],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[806], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[807],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[808], muscle_group: "1"}, {
      muscle_id: "15",
      exercise_id: ids[809],
      muscle_group: "1"
    }, {muscle_id: "14", exercise_id: ids[810], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[811],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[812], muscle_group: "1"}, {
      muscle_id: "9",
      exercise_id: ids[813],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[814], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[815],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[816], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[817],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[818], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[819],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[820], muscle_group: "1"}, {
      muscle_id: "14",
      exercise_id: ids[821],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[822], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[823],
      muscle_group: "1"
    }, {muscle_id: "14", exercise_id: ids[824], muscle_group: "1"}, {
      muscle_id: "15",
      exercise_id: ids[825],
      muscle_group: "1"
    }, {muscle_id: "6", exercise_id: ids[826], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[827],
      muscle_group: "1"
    }, {muscle_id: "9", exercise_id: ids[828], muscle_group: "1"}, {
      muscle_id: "14",
      exercise_id: ids[829],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[830], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[831],
      muscle_group: "1"
    }, {muscle_id: "16", exercise_id: ids[832], muscle_group: "1"}, {
      muscle_id: "16",
      exercise_id: ids[833],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[834], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[835],
      muscle_group: "1"
    }, {muscle_id: "9", exercise_id: ids[836], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[837],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[838], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[839],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[840], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[841],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[842], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[843],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[844], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[845],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[846], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[847],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[848], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[849],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[850], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[851],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[852], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[853],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[854], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[855],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[856], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[857],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[858], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[859],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[860], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[861],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[862], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[863],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[864], muscle_group: "1"}, {
      muscle_id: "5",
      exercise_id: ids[865],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[866], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[867],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[868], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[869],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[870], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[871],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[872], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[873],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[874], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[875],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[876], muscle_group: "1"}, {
      muscle_id: "15",
      exercise_id: ids[877],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[878], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[879],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[880], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[881],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[882], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[883],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[884], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[885],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[886], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[887],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[888], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[889],
      muscle_group: "1"
    }, {muscle_id: "14", exercise_id: ids[890], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[891],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[892], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[893],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[894], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[895],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[896], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[897],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[898], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[899],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[900], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[901],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[902], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[903],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[904], muscle_group: "1"}, {
      muscle_id: "5",
      exercise_id: ids[905],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[906], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[907],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[908], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[909],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[910], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[911],
      muscle_group: "1"
    }, {muscle_id: "5", exercise_id: ids[912], muscle_group: "1"}, {
      muscle_id: "5",
      exercise_id: ids[913],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[914], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[915],
      muscle_group: "1"
    }, {muscle_id: "16", exercise_id: ids[916], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[917],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[918], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[919],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[920], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[921],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[922], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[923],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[924], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[925],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[926], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[927],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[928], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[929],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[930], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[931],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[932], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[933],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[934], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[935],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[936], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[937],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[938], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[939],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[940], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[941],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[942], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[943],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[944], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[945],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[946], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[947],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[948], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[949],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[950], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[951],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[952], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[953],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[954], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[955],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[956], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[957],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[958], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[959],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[960], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[961],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[962], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[963],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[964], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[965],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[966], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[967],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[968], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[969],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[970], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[971],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[972], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[973],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[974], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[975],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[976], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[977],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[978], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[979],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[980], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[981],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[982], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[983],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[984], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[985],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[986], muscle_group: "1"}, {
      muscle_id: "15",
      exercise_id: ids[987],
      muscle_group: "1"
    }, {muscle_id: "15", exercise_id: ids[988], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[989],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[990], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[991],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[992], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[993],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[994], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[995],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[996], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[997],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[998], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[999],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[1000], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1001],
      muscle_group: "1"
    }, {muscle_id: "14", exercise_id: ids[1002], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1003],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1004], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1005],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1006], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1007],
      muscle_group: "1"
    }, {muscle_id: "14", exercise_id: ids[1008], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1009],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1010], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[1011],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[1012], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1013],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[1014], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[1015],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1016], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[1017],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[1018], muscle_group: "1"}, {
      muscle_id: "4",
      exercise_id: ids[1019],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[1020], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[1021],
      muscle_group: "1"
    }, {muscle_id: "2", exercise_id: ids[1022], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[1023],
      muscle_group: "1"
    }, {muscle_id: "16", exercise_id: ids[1024], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1025],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[1026], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1027],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[1028], muscle_group: "1"}, {
      muscle_id: "14",
      exercise_id: ids[1029],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[1030], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[1031],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[1032], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[1033],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1034], muscle_group: "1"}, {
      muscle_id: "2",
      exercise_id: ids[1035],
      muscle_group: "1"
    }, {muscle_id: "4", exercise_id: ids[1036], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1037],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[1038], muscle_group: "1"}, {
      muscle_id: "3",
      exercise_id: ids[1039],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[1040], muscle_group: "1"}, {
      muscle_id: "12",
      exercise_id: ids[1041],
      muscle_group: "1"
    }, {muscle_id: "12", exercise_id: ids[1042], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1043],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[1044], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[1045],
      muscle_group: "1"
    }, {muscle_id: "8", exercise_id: ids[1046], muscle_group: "1"}, {
      muscle_id: "8",
      exercise_id: ids[1047],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[1048], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[1049],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[1050], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1051],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1052], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1053],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1054], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1055],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1056], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1057],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1058], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[1059],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[1060], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1061],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1062], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1063],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1064], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1065],
      muscle_group: "1"
    }, {muscle_id: "3", exercise_id: ids[1066], muscle_group: "1"}, {
      muscle_id: "10",
      exercise_id: ids[1067],
      muscle_group: "1"
    }, {muscle_id: "10", exercise_id: ids[1068], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1069],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[1070], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[1071],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[1072], muscle_group: "1"}, {
      muscle_id: "1",
      exercise_id: ids[1073],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1074], muscle_group: "1"}, {
      muscle_id: "17",
      exercise_id: ids[1075],
      muscle_group: "1"
    }, {muscle_id: "17", exercise_id: ids[1076], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1077],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1078], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1079],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1080], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1081],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1082], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[1083],
      muscle_group: "1"
    }, {muscle_id: "13", exercise_id: ids[1084], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1085],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1086], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1087],
      muscle_group: "1"
    }, {muscle_id: "1", exercise_id: ids[1088], muscle_group: "1"}, {
      muscle_id: "13",
      exercise_id: ids[1089],
      muscle_group: "1"
    }, {muscle_id: "7", exercise_id: ids[1090], muscle_group: "1"}, {
      muscle_id: "7",
      exercise_id: ids[1091],
      muscle_group: "1"
    },]).then(()=> {
      return knex('exercises_equipments').insert([{equipment_id: " 16 ", exercise_id: ids[0]}, {
        equipment_id: " 15 ",
        exercise_id: ids[1]
      }, {equipment_id: " 16 ", exercise_id: ids[2]}, {equipment_id: " 1 ", exercise_id: ids[3]}, {
        equipment_id: " 3 ",
        exercise_id: ids[4]
      }, {equipment_id: " 3 ", exercise_id: ids[5]}, {equipment_id: " 1 ", exercise_id: ids[6]}, {
        equipment_id: " 3 ",
        exercise_id: ids[7]
      }, {equipment_id: " 12 ", exercise_id: ids[8]}, {equipment_id: " 3 ", exercise_id: ids[9]}, {
        equipment_id: " 3 ",
        exercise_id: ids[10]
      }, {equipment_id: " 16 ", exercise_id: ids[11]}, {
        equipment_id: " 3 ",
        exercise_id: ids[12]
      }, {equipment_id: " 10 ", exercise_id: ids[13]}, {
        equipment_id: " 3 ",
        exercise_id: ids[14]
      }, {equipment_id: " 1 ", exercise_id: ids[15]}, {
        equipment_id: " 3 ",
        exercise_id: ids[16]
      }, {equipment_id: " 16 ", exercise_id: ids[17]}, {
        equipment_id: " 1 ",
        exercise_id: ids[18]
      }, {equipment_id: " 3 ", exercise_id: ids[19]}, {equipment_id: " 3 ", exercise_id: ids[20]}, {
        equipment_id: " 3 ",
        exercise_id: ids[21]
      }, {equipment_id: " 16 ", exercise_id: ids[22]}, {
        equipment_id: " 3 ",
        exercise_id: ids[23]
      }, {equipment_id: " 3 ", exercise_id: ids[24]}, {
        equipment_id: " 3 ",
        exercise_id: ids[25]
      }, {equipment_id: " 16 ", exercise_id: ids[26]}, {
        equipment_id: " 3 ",
        exercise_id: ids[27]
      }, {equipment_id: " 3 ", exercise_id: ids[28]}, {equipment_id: " 3 ", exercise_id: ids[29]}, {
        equipment_id: " 3 ",
        exercise_id: ids[30]
      }, {equipment_id: " 3 ", exercise_id: ids[31]}, {
        equipment_id: " 3 ",
        exercise_id: ids[32]
      }, {equipment_id: " 16 ", exercise_id: ids[33]}, {
        equipment_id: " 3 ",
        exercise_id: ids[34]
      }, {equipment_id: " 1 ", exercise_id: ids[35]}, {equipment_id: " 3 ", exercise_id: ids[36]}, {
        equipment_id: " 6 ",
        exercise_id: ids[37]
      }, {equipment_id: " 3 ", exercise_id: ids[38]}, {equipment_id: " 1 ", exercise_id: ids[39]}, {
        equipment_id: " 3 ",
        exercise_id: ids[40]
      }, {equipment_id: " 3 ", exercise_id: ids[41]}, {
        equipment_id: " 10 ",
        exercise_id: ids[42]
      }, {equipment_id: " 11 ", exercise_id: ids[43]}, {
        equipment_id: " 10 ",
        exercise_id: ids[44]
      }, {equipment_id: " 1 ", exercise_id: ids[45]}, {equipment_id: " 1 ", exercise_id: ids[46]}, {
        equipment_id: " 1 ",
        exercise_id: ids[47]
      }, {equipment_id: " 10 ", exercise_id: ids[48]}, {
        equipment_id: " 10 ",
        exercise_id: ids[49]
      }, {equipment_id: " 3 ", exercise_id: ids[50]}, {
        equipment_id: " 15 ",
        exercise_id: ids[51]
      }, {equipment_id: " 13 ", exercise_id: ids[52]}, {
        equipment_id: " 10 ",
        exercise_id: ids[53]
      }, {equipment_id: " 6 ", exercise_id: ids[54]}, {equipment_id: " 3 ", exercise_id: ids[55]}, {
        equipment_id: " 3 ",
        exercise_id: ids[56]
      }, {equipment_id: " 16 ", exercise_id: ids[57]}, {
        equipment_id: " 10 ",
        exercise_id: ids[58]
      }, {equipment_id: " 15 ", exercise_id: ids[59]}, {
        equipment_id: " 11 ",
        exercise_id: ids[60]
      }, {equipment_id: " 3 ", exercise_id: ids[61]}, {
        equipment_id: " 16 ",
        exercise_id: ids[62]
      }, {equipment_id: " 3 ", exercise_id: ids[63]}, {
        equipment_id: " 10 ",
        exercise_id: ids[64]
      }, {equipment_id: " 3 ", exercise_id: ids[65]}, {equipment_id: " 1 ", exercise_id: ids[66]}, {
        equipment_id: " 3 ",
        exercise_id: ids[67]
      }, {equipment_id: " 10 ", exercise_id: ids[68]}, {
        equipment_id: " 10 ",
        exercise_id: ids[69]
      }, {equipment_id: " 16 ", exercise_id: ids[70]}, {
        equipment_id: " 11 ",
        exercise_id: ids[71]
      }, {equipment_id: " 1 ", exercise_id: ids[72]}, {
        equipment_id: " 13 ",
        exercise_id: ids[73]
      }, {equipment_id: " 1 ", exercise_id: ids[74]}, {equipment_id: " 3 ", exercise_id: ids[75]}, {
        equipment_id: " 3 ",
        exercise_id: ids[76]
      }, {equipment_id: " 16 ", exercise_id: ids[77]}, {
        equipment_id: " 15 ",
        exercise_id: ids[78]
      }, {equipment_id: " 11 ", exercise_id: ids[79]}, {
        equipment_id: " 16 ",
        exercise_id: ids[80]
      }, {equipment_id: " 10 ", exercise_id: ids[81]}, {
        equipment_id: " 16 ",
        exercise_id: ids[82]
      }, {equipment_id: " 1 ", exercise_id: ids[83]}, {
        equipment_id: " 1 ",
        exercise_id: ids[84]
      }, {equipment_id: " 16 ", exercise_id: ids[85]}, {
        equipment_id: " 1 ",
        exercise_id: ids[86]
      }, {equipment_id: " 1 ", exercise_id: ids[87]}, {equipment_id: " 1 ", exercise_id: ids[88]}, {
        equipment_id: " 1 ",
        exercise_id: ids[89]
      }, {equipment_id: " 6 ", exercise_id: ids[90]}, {
        equipment_id: " 15 ",
        exercise_id: ids[91]
      }, {equipment_id: " 10 ", exercise_id: ids[92]}, {
        equipment_id: " 1 ",
        exercise_id: ids[93]
      }, {equipment_id: " 11 ", exercise_id: ids[94]}, {
        equipment_id: " 10 ",
        exercise_id: ids[95]
      }, {equipment_id: " 1 ", exercise_id: ids[96]}, {
        equipment_id: " 1 ",
        exercise_id: ids[97]
      }, {equipment_id: " 16 ", exercise_id: ids[98]}, {
        equipment_id: " 1 ",
        exercise_id: ids[99]
      }, {equipment_id: " 16 ", exercise_id: ids[100]}, {
        equipment_id: " 11 ",
        exercise_id: ids[101]
      }, {equipment_id: " 15 ", exercise_id: ids[102]}, {
        equipment_id: " 3 ",
        exercise_id: ids[103]
      }, {equipment_id: " 11 ", exercise_id: ids[104]}, {
        equipment_id: " 10 ",
        exercise_id: ids[105]
      }, {equipment_id: " 10 ", exercise_id: ids[106]}, {
        equipment_id: " 11 ",
        exercise_id: ids[107]
      }, {equipment_id: " 10 ", exercise_id: ids[108]}, {
        equipment_id: " 3 ",
        exercise_id: ids[109]
      }, {equipment_id: " 16 ", exercise_id: ids[110]}, {
        equipment_id: " 1 ",
        exercise_id: ids[111]
      }, {equipment_id: " 1 ", exercise_id: ids[112]}, {
        equipment_id: " 1 ",
        exercise_id: ids[113]
      }, {equipment_id: " 3 ", exercise_id: ids[114]}, {
        equipment_id: " 1 ",
        exercise_id: ids[115]
      }, {equipment_id: " 1 ", exercise_id: ids[116]}, {
        equipment_id: " 3 ",
        exercise_id: ids[117]
      }, {equipment_id: " 16 ", exercise_id: ids[118]}, {
        equipment_id: " 1 ",
        exercise_id: ids[119]
      }, {equipment_id: " 15 ", exercise_id: ids[120]}, {
        equipment_id: " 13 ",
        exercise_id: ids[121]
      }, {equipment_id: " 3 ", exercise_id: ids[122]}, {
        equipment_id: " 10 ",
        exercise_id: ids[123]
      }, {equipment_id: " 10 ", exercise_id: ids[124]}, {
        equipment_id: " 1 ",
        exercise_id: ids[125]
      }, {equipment_id: " 15 ", exercise_id: ids[126]}, {
        equipment_id: " 11 ",
        exercise_id: ids[127]
      }, {equipment_id: " 11 ", exercise_id: ids[128]}, {
        equipment_id: " 1 ",
        exercise_id: ids[129]
      }, {equipment_id: " 10 ", exercise_id: ids[130]}, {
        equipment_id: " 3 ",
        exercise_id: ids[131]
      }, {equipment_id: " 3 ", exercise_id: ids[132]}, {
        equipment_id: " 3 ",
        exercise_id: ids[133]
      }, {equipment_id: " 16 ", exercise_id: ids[134]}, {
        equipment_id: " 13 ",
        exercise_id: ids[135]
      }, {equipment_id: " 1 ", exercise_id: ids[136]}, {
        equipment_id: " 3 ",
        exercise_id: ids[137]
      }, {equipment_id: " 1 ", exercise_id: ids[138]}, {
        equipment_id: " 12 ",
        exercise_id: ids[139]
      }, {equipment_id: " 11 ", exercise_id: ids[140]}, {
        equipment_id: " 3 ",
        exercise_id: ids[141]
      }, {equipment_id: " 3 ", exercise_id: ids[142]}, {
        equipment_id: " 3 ",
        exercise_id: ids[143]
      }, {equipment_id: " 3 ", exercise_id: ids[144]}, {
        equipment_id: " 15 ",
        exercise_id: ids[145]
      }, {equipment_id: " 11 ", exercise_id: ids[146]}, {
        equipment_id: " 15 ",
        exercise_id: ids[147]
      }, {equipment_id: " 11 ", exercise_id: ids[148]}, {
        equipment_id: " 1 ",
        exercise_id: ids[149]
      }, {equipment_id: " 9 ", exercise_id: ids[150]}, {
        equipment_id: " 11 ",
        exercise_id: ids[151]
      }, {equipment_id: " 11 ", exercise_id: ids[152]}, {
        equipment_id: " 3 ",
        exercise_id: ids[153]
      }, {equipment_id: " 10 ", exercise_id: ids[154]}, {
        equipment_id: " 1 ",
        exercise_id: ids[155]
      }, {equipment_id: " 3 ", exercise_id: ids[156]}, {
        equipment_id: " 1 ",
        exercise_id: ids[157]
      }, {equipment_id: " 16 ", exercise_id: ids[158]}, {
        equipment_id: " 15 ",
        exercise_id: ids[159]
      }, {equipment_id: " 15 ", exercise_id: ids[160]}, {
        equipment_id: " 11 ",
        exercise_id: ids[161]
      }, {equipment_id: " 16 ", exercise_id: ids[162]}, {
        equipment_id: " 11 ",
        exercise_id: ids[163]
      }, {equipment_id: " 1 ", exercise_id: ids[164]}, {
        equipment_id: " 3 ",
        exercise_id: ids[165]
      }, {equipment_id: " 3 ", exercise_id: ids[166]}, {
        equipment_id: " 3 ",
        exercise_id: ids[167]
      }, {equipment_id: " 3 ", exercise_id: ids[168]}, {
        equipment_id: " 3 ",
        exercise_id: ids[169]
      }, {equipment_id: " 1 ", exercise_id: ids[170]}, {
        equipment_id: " 16 ",
        exercise_id: ids[171]
      }, {equipment_id: " 3 ", exercise_id: ids[172]}, {
        equipment_id: " 16 ",
        exercise_id: ids[173]
      }, {equipment_id: " 1 ", exercise_id: ids[174]}, {
        equipment_id: " 3 ",
        exercise_id: ids[175]
      }, {equipment_id: " 1 ", exercise_id: ids[176]}, {
        equipment_id: " 3 ",
        exercise_id: ids[177]
      }, {equipment_id: " 3 ", exercise_id: ids[178]}, {
        equipment_id: " 16 ",
        exercise_id: ids[179]
      }, {equipment_id: " 12 ", exercise_id: ids[180]}, {
        equipment_id: " 1 ",
        exercise_id: ids[181]
      }, {equipment_id: " 10 ", exercise_id: ids[182]}, {
        equipment_id: " 10 ",
        exercise_id: ids[183]
      }, {equipment_id: " 1 ", exercise_id: ids[184]}, {
        equipment_id: " 15 ",
        exercise_id: ids[185]
      }, {equipment_id: " 10 ", exercise_id: ids[186]}, {
        equipment_id: " 10 ",
        exercise_id: ids[187]
      }, {equipment_id: " 10 ", exercise_id: ids[188]}, {
        equipment_id: " 15 ",
        exercise_id: ids[189]
      }, {equipment_id: " 15 ", exercise_id: ids[190]}, {
        equipment_id: " 3 ",
        exercise_id: ids[191]
      }, {equipment_id: " 1 ", exercise_id: ids[192]}, {
        equipment_id: " 10 ",
        exercise_id: ids[193]
      }, {equipment_id: " 15 ", exercise_id: ids[194]}, {
        equipment_id: " 15 ",
        exercise_id: ids[195]
      }, {equipment_id: " 10 ", exercise_id: ids[196]}, {
        equipment_id: " 10 ",
        exercise_id: ids[197]
      }, {equipment_id: " 9 ", exercise_id: ids[198]}, {
        equipment_id: " 10 ",
        exercise_id: ids[199]
      }, {equipment_id: " 10 ", exercise_id: ids[200]}, {
        equipment_id: " 1 ",
        exercise_id: ids[201]
      }, {equipment_id: " 13 ", exercise_id: ids[202]}, {
        equipment_id: " 16 ",
        exercise_id: ids[203]
      }, {equipment_id: " 10 ", exercise_id: ids[204]}, {
        equipment_id: " 13 ",
        exercise_id: ids[205]
      }, {equipment_id: " 11 ", exercise_id: ids[206]}, {
        equipment_id: " 1 ",
        exercise_id: ids[207]
      }, {equipment_id: " 1 ", exercise_id: ids[208]}, {
        equipment_id: " 1 ",
        exercise_id: ids[209]
      }, {equipment_id: " 3 ", exercise_id: ids[210]}, {
        equipment_id: " 11 ",
        exercise_id: ids[211]
      }, {equipment_id: " 11 ", exercise_id: ids[212]}, {
        equipment_id: " 10 ",
        exercise_id: ids[213]
      }, {equipment_id: " 15 ", exercise_id: ids[214]}, {
        equipment_id: " 3 ",
        exercise_id: ids[215]
      }, {equipment_id: " 11 ", exercise_id: ids[216]}, {
        equipment_id: " 1 ",
        exercise_id: ids[217]
      }, {equipment_id: " 3 ", exercise_id: ids[218]}, {
        equipment_id: " 3 ",
        exercise_id: ids[219]
      }, {equipment_id: " 1 ", exercise_id: ids[220]}, {
        equipment_id: " 1 ",
        exercise_id: ids[221]
      }, {equipment_id: " 1 ", exercise_id: ids[222]}, {
        equipment_id: " 1 ",
        exercise_id: ids[223]
      }, {equipment_id: " 16 ", exercise_id: ids[224]}, {
        equipment_id: " 16 ",
        exercise_id: ids[225]
      }, {equipment_id: " 10 ", exercise_id: ids[226]}, {
        equipment_id: " 10 ",
        exercise_id: ids[227]
      }, {equipment_id: " 16 ", exercise_id: ids[228]}, {
        equipment_id: " 16 ",
        exercise_id: ids[229]
      }, {equipment_id: " 16 ", exercise_id: ids[230]}, {
        equipment_id: " 15 ",
        exercise_id: ids[231]
      }, {equipment_id: " 10 ", exercise_id: ids[232]}, {
        equipment_id: " 16 ",
        exercise_id: ids[233]
      }, {equipment_id: " 10 ", exercise_id: ids[234]}, {
        equipment_id: " 15 ",
        exercise_id: ids[235]
      }, {equipment_id: " 3 ", exercise_id: ids[236]}, {
        equipment_id: " 11 ",
        exercise_id: ids[237]
      }, {equipment_id: " 10 ", exercise_id: ids[238]}, {
        equipment_id: " 1 ",
        exercise_id: ids[239]
      }, {equipment_id: " 1 ", exercise_id: ids[240]}, {
        equipment_id: " 3 ",
        exercise_id: ids[241]
      }, {equipment_id: " 10 ", exercise_id: ids[242]}, {
        equipment_id: " 10 ",
        exercise_id: ids[243]
      }, {equipment_id: " 1 ", exercise_id: ids[244]}, {
        equipment_id: " 1 ",
        exercise_id: ids[245]
      }, {equipment_id: " 10 ", exercise_id: ids[246]}, {
        equipment_id: " 1 ",
        exercise_id: ids[247]
      }, {equipment_id: " 6 ", exercise_id: ids[248]}, {
        equipment_id: " 6 ",
        exercise_id: ids[249]
      }, {equipment_id: " 16 ", exercise_id: ids[250]}, {
        equipment_id: " 3 ",
        exercise_id: ids[251]
      }, {equipment_id: " 3 ", exercise_id: ids[252]}, {
        equipment_id: " 11 ",
        exercise_id: ids[253]
      }, {equipment_id: " 10 ", exercise_id: ids[254]}, {
        equipment_id: " 11 ",
        exercise_id: ids[255]
      }, {equipment_id: " 6 ", exercise_id: ids[256]}, {
        equipment_id: " 11 ",
        exercise_id: ids[257]
      }, {equipment_id: " 10 ", exercise_id: ids[258]}, {
        equipment_id: " 3 ",
        exercise_id: ids[259]
      }, {equipment_id: " 3 ", exercise_id: ids[260]}, {
        equipment_id: " 15 ",
        exercise_id: ids[261]
      }, {equipment_id: " 1 ", exercise_id: ids[262]}, {
        equipment_id: " 11 ",
        exercise_id: ids[263]
      }, {equipment_id: " 15 ", exercise_id: ids[264]}, {
        equipment_id: " 15 ",
        exercise_id: ids[265]
      }, {equipment_id: " 3 ", exercise_id: ids[266]}, {
        equipment_id: " 15 ",
        exercise_id: ids[267]
      }, {equipment_id: " 1 ", exercise_id: ids[268]}, {
        equipment_id: " 3 ",
        exercise_id: ids[269]
      }, {equipment_id: " 3 ", exercise_id: ids[270]}, {
        equipment_id: " 3 ",
        exercise_id: ids[271]
      }, {equipment_id: " 6 ", exercise_id: ids[272]}, {
        equipment_id: " 3 ",
        exercise_id: ids[273]
      }, {equipment_id: " 11 ", exercise_id: ids[274]}, {
        equipment_id: " 1 ",
        exercise_id: ids[275]
      }, {equipment_id: " 16 ", exercise_id: ids[276]}, {
        equipment_id: " 1 ",
        exercise_id: ids[277]
      }, {equipment_id: " 3 ", exercise_id: ids[278]}, {
        equipment_id: " 1 ",
        exercise_id: ids[279]
      }, {equipment_id: " 1 ", exercise_id: ids[280]}, {
        equipment_id: " 1 ",
        exercise_id: ids[281]
      }, {equipment_id: " 1 ", exercise_id: ids[282]}, {
        equipment_id: " 11 ",
        exercise_id: ids[283]
      }, {equipment_id: " 3 ", exercise_id: ids[284]}, {
        equipment_id: " 3 ",
        exercise_id: ids[285]
      }, {equipment_id: " 6 ", exercise_id: ids[286]}, {
        equipment_id: " 6 ",
        exercise_id: ids[287]
      }, {equipment_id: " 3 ", exercise_id: ids[288]}, {
        equipment_id: " 11 ",
        exercise_id: ids[289]
      }, {equipment_id: " 15 ", exercise_id: ids[290]}, {
        equipment_id: " 11 ",
        exercise_id: ids[291]
      }, {equipment_id: " 10 ", exercise_id: ids[292]}, {
        equipment_id: " 11 ",
        exercise_id: ids[293]
      }, {equipment_id: " 11 ", exercise_id: ids[294]}, {
        equipment_id: " 10 ",
        exercise_id: ids[295]
      }, {equipment_id: " 16 ", exercise_id: ids[296]}, {
        equipment_id: " 11 ",
        exercise_id: ids[297]
      }, {equipment_id: " 10 ", exercise_id: ids[298]}, {
        equipment_id: " 1 ",
        exercise_id: ids[299]
      }, {equipment_id: " 1 ", exercise_id: ids[300]}, {
        equipment_id: " 1 ",
        exercise_id: ids[301]
      }, {equipment_id: " 3 ", exercise_id: ids[302]}, {
        equipment_id: " 11 ",
        exercise_id: ids[303]
      }, {equipment_id: " 1 ", exercise_id: ids[304]}, {
        equipment_id: " 16 ",
        exercise_id: ids[305]
      }, {equipment_id: " 11 ", exercise_id: ids[306]}, {
        equipment_id: " 1 ",
        exercise_id: ids[307]
      }, {equipment_id: " 6 ", exercise_id: ids[308]}, {
        equipment_id: " 6 ",
        exercise_id: ids[309]
      }, {equipment_id: " 6 ", exercise_id: ids[310]}, {
        equipment_id: " 6 ",
        exercise_id: ids[311]
      }, {equipment_id: " 3 ", exercise_id: ids[312]}, {
        equipment_id: " 16 ",
        exercise_id: ids[313]
      }, {equipment_id: " 1 ", exercise_id: ids[314]}, {
        equipment_id: " 16 ",
        exercise_id: ids[315]
      }, {equipment_id: " 16 ", exercise_id: ids[316]}, {
        equipment_id: " 16 ",
        exercise_id: ids[317]
      }, {equipment_id: " 16 ", exercise_id: ids[318]}, {
        equipment_id: " 1 ",
        exercise_id: ids[319]
      }, {equipment_id: " 11 ", exercise_id: ids[320]}, {
        equipment_id: " 10 ",
        exercise_id: ids[321]
      }, {equipment_id: " 10 ", exercise_id: ids[322]}, {
        equipment_id: " 10 ",
        exercise_id: ids[323]
      }, {equipment_id: " 3 ", exercise_id: ids[324]}, {
        equipment_id: " 9 ",
        exercise_id: ids[325]
      }, {equipment_id: " 11 ", exercise_id: ids[326]}, {
        equipment_id: " 11 ",
        exercise_id: ids[327]
      }, {equipment_id: " 10 ", exercise_id: ids[328]}, {
        equipment_id: " 1 ",
        exercise_id: ids[329]
      }, {equipment_id: " 10 ", exercise_id: ids[330]}, {
        equipment_id: " 3 ",
        exercise_id: ids[331]
      }, {equipment_id: " 6 ", exercise_id: ids[332]}, {
        equipment_id: " 3 ",
        exercise_id: ids[333]
      }, {equipment_id: " 3 ", exercise_id: ids[334]}, {
        equipment_id: " 3 ",
        exercise_id: ids[335]
      }, {equipment_id: " 10 ", exercise_id: ids[336]}, {
        equipment_id: " 15 ",
        exercise_id: ids[337]
      }, {equipment_id: " 11 ", exercise_id: ids[338]}, {
        equipment_id: " 1 ",
        exercise_id: ids[339]
      }, {equipment_id: " 15 ", exercise_id: ids[340]}, {
        equipment_id: " 3 ",
        exercise_id: ids[341]
      }, {equipment_id: " 11 ", exercise_id: ids[342]}, {
        equipment_id: " 1 ",
        exercise_id: ids[343]
      }, {equipment_id: " 1 ", exercise_id: ids[344]}, {
        equipment_id: " 3 ",
        exercise_id: ids[345]
      }, {equipment_id: " 3 ", exercise_id: ids[346]}, {
        equipment_id: " 15 ",
        exercise_id: ids[347]
      }, {equipment_id: " 1 ", exercise_id: ids[348]}, {
        equipment_id: " 16 ",
        exercise_id: ids[349]
      }, {equipment_id: " 11 ", exercise_id: ids[350]}, {
        equipment_id: " 1 ",
        exercise_id: ids[351]
      }, {equipment_id: " 3 ", exercise_id: ids[352]}, {
        equipment_id: " 3 ",
        exercise_id: ids[353]
      }, {equipment_id: " 15 ", exercise_id: ids[354]}, {
        equipment_id: " 11 ",
        exercise_id: ids[355]
      }, {equipment_id: " 10 ", exercise_id: ids[356]}, {
        equipment_id: " 16 ",
        exercise_id: ids[357]
      }, {equipment_id: " 16 ", exercise_id: ids[358]}, {
        equipment_id: " 1 ",
        exercise_id: ids[359]
      }, {equipment_id: " 15 ", exercise_id: ids[360]}, {
        equipment_id: " 3 ",
        exercise_id: ids[361]
      }, {equipment_id: " 10 ", exercise_id: ids[362]}, {
        equipment_id: " 10 ",
        exercise_id: ids[363]
      }, {equipment_id: " 1 ", exercise_id: ids[364]}, {
        equipment_id: " 11 ",
        exercise_id: ids[365]
      }, {equipment_id: " 1 ", exercise_id: ids[366]}, {
        equipment_id: " 3 ",
        exercise_id: ids[367]
      }, {equipment_id: " 1 ", exercise_id: ids[368]}, {
        equipment_id: " 10 ",
        exercise_id: ids[369]
      }, {equipment_id: " 16 ", exercise_id: ids[370]}, {
        equipment_id: " 11 ",
        exercise_id: ids[371]
      }, {equipment_id: " 15 ", exercise_id: ids[372]}, {
        equipment_id: " 16 ",
        exercise_id: ids[373]
      }, {equipment_id: " 10 ", exercise_id: ids[374]}, {
        equipment_id: " 11 ",
        exercise_id: ids[375]
      }, {equipment_id: " 10 ", exercise_id: ids[376]}, {
        equipment_id: " 1 ",
        exercise_id: ids[377]
      }, {equipment_id: " 1 ", exercise_id: ids[378]}, {
        equipment_id: " 1 ",
        exercise_id: ids[379]
      }, {equipment_id: " 10 ", exercise_id: ids[380]}, {
        equipment_id: " 10 ",
        exercise_id: ids[381]
      }, {equipment_id: " 9 ", exercise_id: ids[382]}, {
        equipment_id: " 10 ",
        exercise_id: ids[383]
      }, {equipment_id: " 16 ", exercise_id: ids[384]}, {
        equipment_id: " 1 ",
        exercise_id: ids[385]
      }, {equipment_id: " 1 ", exercise_id: ids[386]}, {
        equipment_id: " 1 ",
        exercise_id: ids[387]
      }, {equipment_id: " 16 ", exercise_id: ids[388]}, {
        equipment_id: " 6 ",
        exercise_id: ids[389]
      }, {equipment_id: " 15 ", exercise_id: ids[390]}, {
        equipment_id: " 15 ",
        exercise_id: ids[391]
      }, {equipment_id: " 10 ", exercise_id: ids[392]}, {
        equipment_id: " 15 ",
        exercise_id: ids[393]
      }, {equipment_id: " 11 ", exercise_id: ids[394]}, {
        equipment_id: " 1 ",
        exercise_id: ids[395]
      }, {equipment_id: " 1 ", exercise_id: ids[396]}, {
        equipment_id: " 11 ",
        exercise_id: ids[397]
      }, {equipment_id: " 10 ", exercise_id: ids[398]}, {
        equipment_id: " 15 ",
        exercise_id: ids[399]
      }, {equipment_id: " 16 ", exercise_id: ids[400]}, {
        equipment_id: " 3 ",
        exercise_id: ids[401]
      }, {equipment_id: " 3 ", exercise_id: ids[402]}, {
        equipment_id: " 1 ",
        exercise_id: ids[403]
      }, {equipment_id: " 1 ", exercise_id: ids[404]}, {
        equipment_id: " 6 ",
        exercise_id: ids[405]
      }, {equipment_id: " 6 ", exercise_id: ids[406]}, {
        equipment_id: " 3 ",
        exercise_id: ids[407]
      }, {equipment_id: " 3 ", exercise_id: ids[408]}, {
        equipment_id: " 16 ",
        exercise_id: ids[409]
      }, {equipment_id: " 16 ", exercise_id: ids[410]}, {
        equipment_id: " 15 ",
        exercise_id: ids[411]
      }, {equipment_id: " 16 ", exercise_id: ids[412]}, {
        equipment_id: " 1 ",
        exercise_id: ids[413]
      }, {equipment_id: " 10 ", exercise_id: ids[414]}, {
        equipment_id: " 3 ",
        exercise_id: ids[415]
      }, {equipment_id: " 3 ", exercise_id: ids[416]}, {
        equipment_id: " 11 ",
        exercise_id: ids[417]
      }, {equipment_id: " 1 ", exercise_id: ids[418]}, {
        equipment_id: " 3 ",
        exercise_id: ids[419]
      }, {equipment_id: " 10 ", exercise_id: ids[420]}, {
        equipment_id: " 10 ",
        exercise_id: ids[421]
      }, {equipment_id: " 15 ", exercise_id: ids[422]}, {
        equipment_id: " 1 ",
        exercise_id: ids[423]
      }, {equipment_id: " 1 ", exercise_id: ids[424]}, {
        equipment_id: " 1 ",
        exercise_id: ids[425]
      }, {equipment_id: " 3 ", exercise_id: ids[426]}, {
        equipment_id: " 10 ",
        exercise_id: ids[427]
      }, {equipment_id: " 16 ", exercise_id: ids[428]}, {
        equipment_id: " 1 ",
        exercise_id: ids[429]
      }, {equipment_id: " 10 ", exercise_id: ids[430]}, {
        equipment_id: " 3 ",
        exercise_id: ids[431]
      }, {equipment_id: " 11 ", exercise_id: ids[432]}, {
        equipment_id: " 1 ",
        exercise_id: ids[433]
      }, {equipment_id: " 9 ", exercise_id: ids[434]}, {
        equipment_id: " 1 ",
        exercise_id: ids[435]
      }, {equipment_id: " 15 ", exercise_id: ids[436]}, {
        equipment_id: " 15 ",
        exercise_id: ids[437]
      }, {equipment_id: " 3 ", exercise_id: ids[438]}, {
        equipment_id: " 3 ",
        exercise_id: ids[439]
      }, {equipment_id: " 12 ", exercise_id: ids[440]}, {
        equipment_id: " 16 ",
        exercise_id: ids[441]
      }, {equipment_id: " 16 ", exercise_id: ids[442]}, {
        equipment_id: " 3 ",
        exercise_id: ids[443]
      }, {equipment_id: " 11 ", exercise_id: ids[444]}, {
        equipment_id: " 10 ",
        exercise_id: ids[445]
      }, {equipment_id: " 10 ", exercise_id: ids[446]}, {
        equipment_id: " 3 ",
        exercise_id: ids[447]
      }, {equipment_id: " 1 ", exercise_id: ids[448]}, {
        equipment_id: " 1 ",
        exercise_id: ids[449]
      }, {equipment_id: " 11 ", exercise_id: ids[450]}, {
        equipment_id: " 11 ",
        exercise_id: ids[451]
      }, {equipment_id: " 16 ", exercise_id: ids[452]}, {
        equipment_id: " 11 ",
        exercise_id: ids[453]
      }, {equipment_id: " 1 ", exercise_id: ids[454]}, {
        equipment_id: " 6 ",
        exercise_id: ids[455]
      }, {equipment_id: " 15 ", exercise_id: ids[456]}, {
        equipment_id: " 15 ",
        exercise_id: ids[457]
      }, {equipment_id: " 16 ", exercise_id: ids[458]}, {
        equipment_id: " 11 ",
        exercise_id: ids[459]
      }, {equipment_id: " 11 ", exercise_id: ids[460]}, {
        equipment_id: " 10 ",
        exercise_id: ids[461]
      }, {equipment_id: " 16 ", exercise_id: ids[462]}, {
        equipment_id: " 1 ",
        exercise_id: ids[463]
      }, {equipment_id: " 10 ", exercise_id: ids[464]}, {
        equipment_id: " 10 ",
        exercise_id: ids[465]
      }, {equipment_id: " 15 ", exercise_id: ids[466]}, {
        equipment_id: " 3 ",
        exercise_id: ids[467]
      }, {equipment_id: " 10 ", exercise_id: ids[468]}, {
        equipment_id: " 3 ",
        exercise_id: ids[469]
      }, {equipment_id: " 1 ", exercise_id: ids[470]}, {
        equipment_id: " 15 ",
        exercise_id: ids[471]
      }, {equipment_id: " 11 ", exercise_id: ids[472]}, {
        equipment_id: " 3 ",
        exercise_id: ids[473]
      }, {equipment_id: " 15 ", exercise_id: ids[474]}, {
        equipment_id: " 3 ",
        exercise_id: ids[475]
      }, {equipment_id: " 1 ", exercise_id: ids[476]}, {
        equipment_id: " 6 ",
        exercise_id: ids[477]
      }, {equipment_id: " 14 ", exercise_id: ids[478]}, {
        equipment_id: " 3 ",
        exercise_id: ids[479]
      }, {equipment_id: " 12 ", exercise_id: ids[480]}, {
        equipment_id: " 11 ",
        exercise_id: ids[481]
      }, {equipment_id: " 15 ", exercise_id: ids[482]}, {
        equipment_id: " 11 ",
        exercise_id: ids[483]
      }, {equipment_id: " 3 ", exercise_id: ids[484]}, {
        equipment_id: " 6 ",
        exercise_id: ids[485]
      }, {equipment_id: " 15 ", exercise_id: ids[486]}, {
        equipment_id: " 3 ",
        exercise_id: ids[487]
      }, {equipment_id: " 3 ", exercise_id: ids[488]}, {
        equipment_id: " 11 ",
        exercise_id: ids[489]
      }, {equipment_id: " 11 ", exercise_id: ids[490]}, {
        equipment_id: " 1 ",
        exercise_id: ids[491]
      }, {equipment_id: " 16 ", exercise_id: ids[492]}, {
        equipment_id: " 3 ",
        exercise_id: ids[493]
      }, {equipment_id: " 16 ", exercise_id: ids[494]}, {
        equipment_id: " 10 ",
        exercise_id: ids[495]
      }, {equipment_id: " 1 ", exercise_id: ids[496]}, {
        equipment_id: " 11 ",
        exercise_id: ids[497]
      }, {equipment_id: " 10 ", exercise_id: ids[498]}, {
        equipment_id: " 15 ",
        exercise_id: ids[499]
      }, {equipment_id: " 1 ", exercise_id: ids[500]}, {
        equipment_id: " 3 ",
        exercise_id: ids[501]
      }, {equipment_id: " 13 ", exercise_id: ids[502]}, {
        equipment_id: " 13 ",
        exercise_id: ids[503]
      }, {equipment_id: " 11 ", exercise_id: ids[504]}, {
        equipment_id: " 11 ",
        exercise_id: ids[505]
      }, {equipment_id: " 3 ", exercise_id: ids[506]}, {
        equipment_id: " 3 ",
        exercise_id: ids[507]
      }, {equipment_id: " 15 ", exercise_id: ids[508]}, {
        equipment_id: " 15 ",
        exercise_id: ids[509]
      }, {equipment_id: " 11 ", exercise_id: ids[510]}, {
        equipment_id: " 15 ",
        exercise_id: ids[511]
      }, {equipment_id: " 1 ", exercise_id: ids[512]}, {
        equipment_id: " 15 ",
        exercise_id: ids[513]
      }, {equipment_id: " 16 ", exercise_id: ids[514]}, {
        equipment_id: " 3 ",
        exercise_id: ids[515]
      }, {equipment_id: " 16 ", exercise_id: ids[516]}, {
        equipment_id: " 3 ",
        exercise_id: ids[517]
      }, {equipment_id: " 16 ", exercise_id: ids[518]}, {
        equipment_id: " 10 ",
        exercise_id: ids[519]
      }, {equipment_id: " 15 ", exercise_id: ids[520]}, {
        equipment_id: " 10 ",
        exercise_id: ids[521]
      }, {equipment_id: " 16 ", exercise_id: ids[522]}, {
        equipment_id: " 15 ",
        exercise_id: ids[523]
      }, {equipment_id: " 6 ", exercise_id: ids[524]}, {
        equipment_id: " 10 ",
        exercise_id: ids[525]
      }, {equipment_id: " 10 ", exercise_id: ids[526]}, {
        equipment_id: " 12 ",
        exercise_id: ids[527]
      }, {equipment_id: " 10 ", exercise_id: ids[528]}, {
        equipment_id: " 15 ",
        exercise_id: ids[529]
      }, {equipment_id: " 1 ", exercise_id: ids[530]}, {
        equipment_id: " 1 ",
        exercise_id: ids[531]
      }, {equipment_id: " 6 ", exercise_id: ids[532]}, {
        equipment_id: " 16 ",
        exercise_id: ids[533]
      }, {equipment_id: " 15 ", exercise_id: ids[534]}, {
        equipment_id: " 11 ",
        exercise_id: ids[535]
      }, {equipment_id: " 15 ", exercise_id: ids[536]}, {
        equipment_id: " 11 ",
        exercise_id: ids[537]
      }, {equipment_id: " 3 ", exercise_id: ids[538]}, {
        equipment_id: " 6 ",
        exercise_id: ids[539]
      }, {equipment_id: " 15 ", exercise_id: ids[540]}, {
        equipment_id: " 10 ",
        exercise_id: ids[541]
      }, {equipment_id: " 15 ", exercise_id: ids[542]}, {
        equipment_id: " 1 ",
        exercise_id: ids[543]
      }, {equipment_id: " 16 ", exercise_id: ids[544]}, {
        equipment_id: " 11 ",
        exercise_id: ids[545]
      }, {equipment_id: " 1 ", exercise_id: ids[546]}, {
        equipment_id: " 3 ",
        exercise_id: ids[547]
      }, {equipment_id: " 16 ", exercise_id: ids[548]}, {
        equipment_id: " 15 ",
        exercise_id: ids[549]
      }, {equipment_id: " 12 ", exercise_id: ids[550]}, {
        equipment_id: " 11 ",
        exercise_id: ids[551]
      }, {equipment_id: " 13 ", exercise_id: ids[552]}, {
        equipment_id: " 11 ",
        exercise_id: ids[553]
      }, {equipment_id: " 16 ", exercise_id: ids[554]}, {
        equipment_id: " 3 ",
        exercise_id: ids[555]
      }, {equipment_id: " 16 ", exercise_id: ids[556]}, {
        equipment_id: " 3 ",
        exercise_id: ids[557]
      }, {equipment_id: " 9 ", exercise_id: ids[558]}, {
        equipment_id: " 16 ",
        exercise_id: ids[559]
      }, {equipment_id: " 6 ", exercise_id: ids[560]}, {
        equipment_id: " 10 ",
        exercise_id: ids[561]
      }, {equipment_id: " 10 ", exercise_id: ids[562]}, {
        equipment_id: " 10 ",
        exercise_id: ids[563]
      }, {equipment_id: " 1 ", exercise_id: ids[564]}, {
        equipment_id: " 11 ",
        exercise_id: ids[565]
      }, {equipment_id: " 1 ", exercise_id: ids[566]}, {
        equipment_id: " 1 ",
        exercise_id: ids[567]
      }, {equipment_id: " 15 ", exercise_id: ids[568]}, {
        equipment_id: " 10 ",
        exercise_id: ids[569]
      }, {equipment_id: " 3 ", exercise_id: ids[570]}, {
        equipment_id: " 11 ",
        exercise_id: ids[571]
      }, {equipment_id: " 15 ", exercise_id: ids[572]}, {
        equipment_id: " 15 ",
        exercise_id: ids[573]
      }, {equipment_id: " 6 ", exercise_id: ids[574]}, {
        equipment_id: " 10 ",
        exercise_id: ids[575]
      }, {equipment_id: " 6 ", exercise_id: ids[576]}, {
        equipment_id: " 6 ",
        exercise_id: ids[577]
      }, {equipment_id: " 10 ", exercise_id: ids[578]}, {
        equipment_id: " 3 ",
        exercise_id: ids[579]
      }, {equipment_id: " 16 ", exercise_id: ids[580]}, {
        equipment_id: " 3 ",
        exercise_id: ids[581]
      }, {equipment_id: " 10 ", exercise_id: ids[582]}, {
        equipment_id: " 11 ",
        exercise_id: ids[583]
      }, {equipment_id: " 1 ", exercise_id: ids[584]}, {
        equipment_id: " 1 ",
        exercise_id: ids[585]
      }, {equipment_id: " 3 ", exercise_id: ids[586]}, {
        equipment_id: " 1 ",
        exercise_id: ids[587]
      }, {equipment_id: " 16 ", exercise_id: ids[588]}, {
        equipment_id: " 16 ",
        exercise_id: ids[589]
      }, {equipment_id: " 10 ", exercise_id: ids[590]}, {
        equipment_id: " 10 ",
        exercise_id: ids[591]
      }, {equipment_id: " 16 ", exercise_id: ids[592]}, {
        equipment_id: " 3 ",
        exercise_id: ids[593]
      }, {equipment_id: " 16 ", exercise_id: ids[594]}, {
        equipment_id: " 3 ",
        exercise_id: ids[595]
      }, {equipment_id: " 3 ", exercise_id: ids[596]}, {
        equipment_id: " 3 ",
        exercise_id: ids[597]
      }, {equipment_id: " 1 ", exercise_id: ids[598]}, {
        equipment_id: " 10 ",
        exercise_id: ids[599]
      }, {equipment_id: " 1 ", exercise_id: ids[600]}, {
        equipment_id: " 6 ",
        exercise_id: ids[601]
      }, {equipment_id: " 1 ", exercise_id: ids[602]}, {
        equipment_id: " 10 ",
        exercise_id: ids[603]
      }, {equipment_id: " 11 ", exercise_id: ids[604]}, {
        equipment_id: " 10 ",
        exercise_id: ids[605]
      }, {equipment_id: " 16 ", exercise_id: ids[606]}, {
        equipment_id: " 6 ",
        exercise_id: ids[607]
      }, {equipment_id: " 10 ", exercise_id: ids[608]}, {
        equipment_id: " 10 ",
        exercise_id: ids[609]
      }, {equipment_id: " 10 ", exercise_id: ids[610]}, {
        equipment_id: " 10 ",
        exercise_id: ids[611]
      }, {equipment_id: " 15 ", exercise_id: ids[612]}, {
        equipment_id: " 10 ",
        exercise_id: ids[613]
      }, {equipment_id: " 1 ", exercise_id: ids[614]}, {
        equipment_id: " 1 ",
        exercise_id: ids[615]
      }, {equipment_id: " 6 ", exercise_id: ids[616]}, {
        equipment_id: " 15 ",
        exercise_id: ids[617]
      }, {equipment_id: " 16 ", exercise_id: ids[618]}, {
        equipment_id: " 3 ",
        exercise_id: ids[619]
      }, {equipment_id: " 3 ", exercise_id: ids[620]}, {
        equipment_id: " 10 ",
        exercise_id: ids[621]
      }, {equipment_id: " 10 ", exercise_id: ids[622]}, {
        equipment_id: " 1 ",
        exercise_id: ids[623]
      }, {equipment_id: " 16 ", exercise_id: ids[624]}, {
        equipment_id: " 11 ",
        exercise_id: ids[625]
      }, {equipment_id: " 10 ", exercise_id: ids[626]}, {
        equipment_id: " 15 ",
        exercise_id: ids[627]
      }, {equipment_id: " 10 ", exercise_id: ids[628]}, {
        equipment_id: " 10 ",
        exercise_id: ids[629]
      }, {equipment_id: " 6 ", exercise_id: ids[630]}, {
        equipment_id: " 16 ",
        exercise_id: ids[631]
      }, {equipment_id: " 16 ", exercise_id: ids[632]}, {
        equipment_id: " 1 ",
        exercise_id: ids[633]
      }, {equipment_id: " 11 ", exercise_id: ids[634]}, {
        equipment_id: " 3 ",
        exercise_id: ids[635]
      }, {equipment_id: " 3 ", exercise_id: ids[636]}, {
        equipment_id: " 15 ",
        exercise_id: ids[637]
      }, {equipment_id: " 6 ", exercise_id: ids[638]}, {
        equipment_id: " 16 ",
        exercise_id: ids[639]
      }, {equipment_id: " 10 ", exercise_id: ids[640]}, {
        equipment_id: " 11 ",
        exercise_id: ids[641]
      }, {equipment_id: " 10 ", exercise_id: ids[642]}, {
        equipment_id: " 15 ",
        exercise_id: ids[643]
      }, {equipment_id: " 1 ", exercise_id: ids[644]}, {
        equipment_id: " 16 ",
        exercise_id: ids[645]
      }, {equipment_id: " 3 ", exercise_id: ids[646]}, {
        equipment_id: " 15 ",
        exercise_id: ids[647]
      }, {equipment_id: " 15 ", exercise_id: ids[648]}, {
        equipment_id: " 11 ",
        exercise_id: ids[649]
      }, {equipment_id: " 16 ", exercise_id: ids[650]}, {
        equipment_id: " 3 ",
        exercise_id: ids[651]
      }, {equipment_id: " 16 ", exercise_id: ids[652]}, {
        equipment_id: " 16 ",
        exercise_id: ids[653]
      }, {equipment_id: " 16 ", exercise_id: ids[654]}, {
        equipment_id: " 10 ",
        exercise_id: ids[655]
      }, {equipment_id: " 11 ", exercise_id: ids[656]}, {
        equipment_id: " 11 ",
        exercise_id: ids[657]
      }, {equipment_id: " 16 ", exercise_id: ids[658]}, {
        equipment_id: " 10 ",
        exercise_id: ids[659]
      }, {equipment_id: " 11 ", exercise_id: ids[660]}, {
        equipment_id: " 10 ",
        exercise_id: ids[661]
      }, {equipment_id: " 16 ", exercise_id: ids[662]}, {
        equipment_id: " 1 ",
        exercise_id: ids[663]
      }, {equipment_id: " 15 ", exercise_id: ids[664]}, {
        equipment_id: " 10 ",
        exercise_id: ids[665]
      }, {equipment_id: " 3 ", exercise_id: ids[666]}, {
        equipment_id: " 16 ",
        exercise_id: ids[667]
      }, {equipment_id: " 10 ", exercise_id: ids[668]}, {
        equipment_id: " 3 ",
        exercise_id: ids[669]
      }, {equipment_id: " 6 ", exercise_id: ids[670]}, {
        equipment_id: " 6 ",
        exercise_id: ids[671]
      }, {equipment_id: " 6 ", exercise_id: ids[672]}, {
        equipment_id: " 15 ",
        exercise_id: ids[673]
      }, {equipment_id: " 16 ", exercise_id: ids[674]}, {
        equipment_id: " 15 ",
        exercise_id: ids[675]
      }, {equipment_id: " 10 ", exercise_id: ids[676]}, {
        equipment_id: " 16 ",
        exercise_id: ids[677]
      }, {equipment_id: " 16 ", exercise_id: ids[678]}, {
        equipment_id: " 16 ",
        exercise_id: ids[679]
      }, {equipment_id: " 12 ", exercise_id: ids[680]}, {
        equipment_id: " 6 ",
        exercise_id: ids[681]
      }, {equipment_id: " 3 ", exercise_id: ids[682]}, {
        equipment_id: " 3 ",
        exercise_id: ids[683]
      }, {equipment_id: " 16 ", exercise_id: ids[684]}, {
        equipment_id: " 3 ",
        exercise_id: ids[685]
      }, {equipment_id: " 16 ", exercise_id: ids[686]}, {
        equipment_id: " 16 ",
        exercise_id: ids[687]
      }, {equipment_id: " 16 ", exercise_id: ids[688]}, {
        equipment_id: " 16 ",
        exercise_id: ids[689]
      }, {equipment_id: " 16 ", exercise_id: ids[690]}, {
        equipment_id: " 3 ",
        exercise_id: ids[691]
      }, {equipment_id: " 10 ", exercise_id: ids[692]}, {
        equipment_id: " 14 ",
        exercise_id: ids[693]
      }, {equipment_id: " 16 ", exercise_id: ids[694]}, {
        equipment_id: " 11 ",
        exercise_id: ids[695]
      }, {equipment_id: " 10 ", exercise_id: ids[696]}, {
        equipment_id: " 10 ",
        exercise_id: ids[697]
      }, {equipment_id: " 3 ", exercise_id: ids[698]}, {
        equipment_id: " 10 ",
        exercise_id: ids[699]
      }, {equipment_id: " 1 ", exercise_id: ids[700]}, {
        equipment_id: " 3 ",
        exercise_id: ids[701]
      }, {equipment_id: " 3 ", exercise_id: ids[702]}, {
        equipment_id: " 16 ",
        exercise_id: ids[703]
      }, {equipment_id: " 10 ", exercise_id: ids[704]}, {
        equipment_id: " 9 ",
        exercise_id: ids[705]
      }, {equipment_id: " 16 ", exercise_id: ids[706]}, {
        equipment_id: " 14 ",
        exercise_id: ids[707]
      }, {equipment_id: " 16 ", exercise_id: ids[708]}, {
        equipment_id: " 10 ",
        exercise_id: ids[709]
      }, {equipment_id: " 16 ", exercise_id: ids[710]}, {
        equipment_id: " 16 ",
        exercise_id: ids[711]
      }, {equipment_id: " 1 ", exercise_id: ids[712]}, {
        equipment_id: " 6 ",
        exercise_id: ids[713]
      }, {equipment_id: " 16 ", exercise_id: ids[714]}, {
        equipment_id: " 6 ",
        exercise_id: ids[715]
      }, {equipment_id: " 1 ", exercise_id: ids[716]}, {
        equipment_id: " 3 ",
        exercise_id: ids[717]
      }, {equipment_id: " 3 ", exercise_id: ids[718]}, {
        equipment_id: " 16 ",
        exercise_id: ids[719]
      }, {equipment_id: " 16 ", exercise_id: ids[720]}, {
        equipment_id: " 16 ",
        exercise_id: ids[721]
      }, {equipment_id: " 6 ", exercise_id: ids[722]}, {
        equipment_id: " 14 ",
        exercise_id: ids[723]
      }, {equipment_id: " 16 ", exercise_id: ids[724]}, {
        equipment_id: " 16 ",
        exercise_id: ids[725]
      }, {equipment_id: " 16 ", exercise_id: ids[726]}, {
        equipment_id: " 12 ",
        exercise_id: ids[727]
      }, {equipment_id: " 3 ", exercise_id: ids[728]}, {
        equipment_id: " 16 ",
        exercise_id: ids[729]
      }, {equipment_id: " 16 ", exercise_id: ids[730]}, {
        equipment_id: " 16 ",
        exercise_id: ids[731]
      }, {equipment_id: " 12 ", exercise_id: ids[732]}, {
        equipment_id: " 15 ",
        exercise_id: ids[733]
      }, {equipment_id: " 14 ", exercise_id: ids[734]}, {
        equipment_id: " 10 ",
        exercise_id: ids[735]
      }, {equipment_id: " 16 ", exercise_id: ids[736]}, {
        equipment_id: " 16 ",
        exercise_id: ids[737]
      }, {equipment_id: " 16 ", exercise_id: ids[738]}, {
        equipment_id: " 16 ",
        exercise_id: ids[739]
      }, {equipment_id: " 14 ", exercise_id: ids[740]}, {
        equipment_id: " 10 ",
        exercise_id: ids[741]
      }, {equipment_id: " 9 ", exercise_id: ids[742]}, {
        equipment_id: " 16 ",
        exercise_id: ids[743]
      }, {equipment_id: " 16 ", exercise_id: ids[744]}, {
        equipment_id: " 16 ",
        exercise_id: ids[745]
      }, {equipment_id: " 14 ", exercise_id: ids[746]}, {
        equipment_id: " 3 ",
        exercise_id: ids[747]
      }, {equipment_id: " 16 ", exercise_id: ids[748]}, {
        equipment_id: " 6 ",
        exercise_id: ids[749]
      }, {equipment_id: " 12 ", exercise_id: ids[750]}, {
        equipment_id: " 1 ",
        exercise_id: ids[751]
      }, {equipment_id: " 16 ", exercise_id: ids[752]}, {
        equipment_id: " 16 ",
        exercise_id: ids[753]
      }, {equipment_id: " 16 ", exercise_id: ids[754]}, {
        equipment_id: " 16 ",
        exercise_id: ids[755]
      }, {equipment_id: " 16 ", exercise_id: ids[756]}, {
        equipment_id: " 16 ",
        exercise_id: ids[757]
      }, {equipment_id: " 16 ", exercise_id: ids[758]}, {
        equipment_id: " 16 ",
        exercise_id: ids[759]
      }, {equipment_id: " 16 ", exercise_id: ids[760]}, {
        equipment_id: " 16 ",
        exercise_id: ids[761]
      }, {equipment_id: " 16 ", exercise_id: ids[762]}, {
        equipment_id: " 10 ",
        exercise_id: ids[763]
      }, {equipment_id: " 10 ", exercise_id: ids[764]}, {
        equipment_id: " 10 ",
        exercise_id: ids[765]
      }, {equipment_id: " 9 ", exercise_id: ids[766]}, {
        equipment_id: " 11 ",
        exercise_id: ids[767]
      }, {equipment_id: " 16 ", exercise_id: ids[768]}, {
        equipment_id: " 16 ",
        exercise_id: ids[769]
      }, {equipment_id: " 16 ", exercise_id: ids[770]}, {
        equipment_id: " 16 ",
        exercise_id: ids[771]
      }, {equipment_id: " 6 ", exercise_id: ids[772]}, {
        equipment_id: " 16 ",
        exercise_id: ids[773]
      }, {equipment_id: " 16 ", exercise_id: ids[774]}, {
        equipment_id: " 16 ",
        exercise_id: ids[775]
      }, {equipment_id: " 16 ", exercise_id: ids[776]}, {
        equipment_id: " 1 ",
        exercise_id: ids[777]
      }, {equipment_id: " 16 ", exercise_id: ids[778]}, {
        equipment_id: " 16 ",
        exercise_id: ids[779]
      }, {equipment_id: " 3 ", exercise_id: ids[780]}, {
        equipment_id: " 16 ",
        exercise_id: ids[781]
      }, {equipment_id: " 14 ", exercise_id: ids[782]}, {
        equipment_id: " 16 ",
        exercise_id: ids[783]
      }, {equipment_id: " 14 ", exercise_id: ids[784]}, {
        equipment_id: " 10 ",
        exercise_id: ids[785]
      }, {equipment_id: " 16 ", exercise_id: ids[786]}, {
        equipment_id: " 16 ",
        exercise_id: ids[787]
      }, {equipment_id: " 16 ", exercise_id: ids[788]}, {
        equipment_id: " 16 ",
        exercise_id: ids[789]
      }, {equipment_id: " 16 ", exercise_id: ids[790]}, {
        equipment_id: " 16 ",
        exercise_id: ids[791]
      }, {equipment_id: " 16 ", exercise_id: ids[792]}, {
        equipment_id: " 16 ",
        exercise_id: ids[793]
      }, {equipment_id: " 16 ", exercise_id: ids[794]}, {
        equipment_id: " 16 ",
        exercise_id: ids[795]
      }, {equipment_id: " 16 ", exercise_id: ids[796]}, {
        equipment_id: " 9 ",
        exercise_id: ids[797]
      }, {equipment_id: " 14 ", exercise_id: ids[798]}, {
        equipment_id: " 16 ",
        exercise_id: ids[799]
      }, {equipment_id: " 16 ", exercise_id: ids[800]}, {
        equipment_id: " 16 ",
        exercise_id: ids[801]
      }, {equipment_id: " 16 ", exercise_id: ids[802]}, {
        equipment_id: " 16 ",
        exercise_id: ids[803]
      }, {equipment_id: " 16 ", exercise_id: ids[804]}, {
        equipment_id: " 16 ",
        exercise_id: ids[805]
      }, {equipment_id: " 9 ", exercise_id: ids[806]}, {
        equipment_id: " 16 ",
        exercise_id: ids[807]
      }, {equipment_id: " 16 ", exercise_id: ids[808]}, {
        equipment_id: " 16 ",
        exercise_id: ids[809]
      }, {equipment_id: " 16 ", exercise_id: ids[810]}, {
        equipment_id: " 12 ",
        exercise_id: ids[811]
      }, {equipment_id: " 10 ", exercise_id: ids[812]}, {
        equipment_id: " 16 ",
        exercise_id: ids[813]
      }, {equipment_id: " 16 ", exercise_id: ids[814]}, {
        equipment_id: " 16 ",
        exercise_id: ids[815]
      }, {equipment_id: " 10 ", exercise_id: ids[816]}, {
        equipment_id: " 14 ",
        exercise_id: ids[817]
      }, {equipment_id: " 16 ", exercise_id: ids[818]}, {
        equipment_id: " 16 ",
        exercise_id: ids[819]
      }, {equipment_id: " 16 ", exercise_id: ids[820]}, {
        equipment_id: " 10 ",
        exercise_id: ids[821]
      }, {equipment_id: " 16 ", exercise_id: ids[822]}, {
        equipment_id: " 6 ",
        exercise_id: ids[823]
      }, {equipment_id: " 10 ", exercise_id: ids[824]}, {
        equipment_id: " 16 ",
        exercise_id: ids[825]
      }, {equipment_id: " 16 ", exercise_id: ids[826]}, {
        equipment_id: " 16 ",
        exercise_id: ids[827]
      }, {equipment_id: " 16 ", exercise_id: ids[828]}, {
        equipment_id: " 16 ",
        exercise_id: ids[829]
      }, {equipment_id: " 10 ", exercise_id: ids[830]}, {
        equipment_id: " 16 ",
        exercise_id: ids[831]
      }, {equipment_id: " 16 ", exercise_id: ids[832]}, {
        equipment_id: " 10 ",
        exercise_id: ids[833]
      }, {equipment_id: " 9 ", exercise_id: ids[834]}, {
        equipment_id: " 16 ",
        exercise_id: ids[835]
      }, {equipment_id: " 16 ", exercise_id: ids[836]}, {
        equipment_id: " 16 ",
        exercise_id: ids[837]
      }, {equipment_id: " 3 ", exercise_id: ids[838]}, {
        equipment_id: " 6 ",
        exercise_id: ids[839]
      }, {equipment_id: " 6 ", exercise_id: ids[840]}, {
        equipment_id: " 6 ",
        exercise_id: ids[841]
      }, {equipment_id: " 6 ", exercise_id: ids[842]}, {
        equipment_id: " 6 ",
        exercise_id: ids[843]
      }, {equipment_id: " 6 ", exercise_id: ids[844]}, {
        equipment_id: " 6 ",
        exercise_id: ids[845]
      }, {equipment_id: " 6 ", exercise_id: ids[846]}, {
        equipment_id: " 6 ",
        exercise_id: ids[847]
      }, {equipment_id: " 16 ", exercise_id: ids[848]}, {
        equipment_id: " 3 ",
        exercise_id: ids[849]
      }, {equipment_id: " 16 ", exercise_id: ids[850]}, {
        equipment_id: " 3 ",
        exercise_id: ids[851]
      }, {equipment_id: " 3 ", exercise_id: ids[852]}, {
        equipment_id: " 3 ",
        exercise_id: ids[853]
      }, {equipment_id: " 16 ", exercise_id: ids[854]}, {
        equipment_id: " 16 ",
        exercise_id: ids[855]
      }, {equipment_id: " 3 ", exercise_id: ids[856]}, {
        equipment_id: " 3 ",
        exercise_id: ids[857]
      }, {equipment_id: " 16 ", exercise_id: ids[858]}, {
        equipment_id: " 16 ",
        exercise_id: ids[859]
      }, {equipment_id: " 3 ", exercise_id: ids[860]}, {
        equipment_id: " 3 ",
        exercise_id: ids[861]
      }, {equipment_id: " 3 ", exercise_id: ids[862]}, {
        equipment_id: " 3 ",
        exercise_id: ids[863]
      }, {equipment_id: " 3 ", exercise_id: ids[864]}, {
        equipment_id: " 3 ",
        exercise_id: ids[865]
      }, {equipment_id: " 3 ", exercise_id: ids[866]}, {
        equipment_id: " 3 ",
        exercise_id: ids[867]
      }, {equipment_id: " 12 ", exercise_id: ids[868]}, {
        equipment_id: " 16 ",
        exercise_id: ids[869]
      }, {equipment_id: " 12 ", exercise_id: ids[870]}, {
        equipment_id: " 16 ",
        exercise_id: ids[871]
      }, {equipment_id: " 16 ", exercise_id: ids[872]}, {
        equipment_id: " 12 ",
        exercise_id: ids[873]
      }, {equipment_id: " 16 ", exercise_id: ids[874]}, {
        equipment_id: " 16 ",
        exercise_id: ids[875]
      }, {equipment_id: " 16 ", exercise_id: ids[876]}, {
        equipment_id: " 16 ",
        exercise_id: ids[877]
      }, {equipment_id: " 16 ", exercise_id: ids[878]}, {
        equipment_id: " 16 ",
        exercise_id: ids[879]
      }, {equipment_id: " 16 ", exercise_id: ids[880]}, {
        equipment_id: " 16 ",
        exercise_id: ids[881]
      }, {equipment_id: " 3 ", exercise_id: ids[882]}, {
        equipment_id: " 16 ",
        exercise_id: ids[883]
      }, {equipment_id: " 16 ", exercise_id: ids[884]}, {
        equipment_id: " 16 ",
        exercise_id: ids[885]
      }, {equipment_id: " 10 ", exercise_id: ids[886]}, {
        equipment_id: " 15 ",
        exercise_id: ids[887]
      }, {equipment_id: " 6 ", exercise_id: ids[888]}, {
        equipment_id: " 10 ",
        exercise_id: ids[889]
      }, {equipment_id: " 10 ", exercise_id: ids[890]}, {
        equipment_id: " 10 ",
        exercise_id: ids[891]
      }, {equipment_id: " 10 ", exercise_id: ids[892]}, {
        equipment_id: " 10 ",
        exercise_id: ids[893]
      }, {equipment_id: " 10 ", exercise_id: ids[894]}, {
        equipment_id: " 10 ",
        exercise_id: ids[895]
      }, {equipment_id: " 10 ", exercise_id: ids[896]}, {
        equipment_id: " 10 ",
        exercise_id: ids[897]
      }, {equipment_id: " 3 ", exercise_id: ids[898]}, {
        equipment_id: " 16 ",
        exercise_id: ids[899]
      }, {equipment_id: " 10 ", exercise_id: ids[900]}, {
        equipment_id: " 3 ",
        exercise_id: ids[901]
      }, {equipment_id: " 3 ", exercise_id: ids[902]}, {
        equipment_id: " 3 ",
        exercise_id: ids[903]
      }, {equipment_id: " 10 ", exercise_id: ids[904]}, {
        equipment_id: " 10 ",
        exercise_id: ids[905]
      }, {equipment_id: " 10 ", exercise_id: ids[906]}, {
        equipment_id: " 16 ",
        exercise_id: ids[907]
      }, {equipment_id: " 16 ", exercise_id: ids[908]}, {
        equipment_id: " 12 ",
        exercise_id: ids[909]
      }, {equipment_id: " 12 ", exercise_id: ids[910]}, {
        equipment_id: " 12 ",
        exercise_id: ids[911]
      }, {equipment_id: " 16 ", exercise_id: ids[912]}, {
        equipment_id: " 16 ",
        exercise_id: ids[913]
      }, {equipment_id: " 10 ", exercise_id: ids[914]}, {
        equipment_id: " 10 ",
        exercise_id: ids[915]
      }, {equipment_id: " 16 ", exercise_id: ids[916]}, {
        equipment_id: " 1 ",
        exercise_id: ids[917]
      }, {equipment_id: " 1 ", exercise_id: ids[918]}, {
        equipment_id: " 1 ",
        exercise_id: ids[919]
      }, {equipment_id: " 3 ", exercise_id: ids[920]}, {
        equipment_id: " 12 ",
        exercise_id: ids[921]
      }, {equipment_id: " 16 ", exercise_id: ids[922]}, {
        equipment_id: " 10 ",
        exercise_id: ids[923]
      }, {equipment_id: " 9 ", exercise_id: ids[924]}, {
        equipment_id: " 1 ",
        exercise_id: ids[925]
      }, {equipment_id: " 16 ", exercise_id: ids[926]}, {
        equipment_id: " 16 ",
        exercise_id: ids[927]
      }, {equipment_id: " 3 ", exercise_id: ids[928]}, {
        equipment_id: " 10 ",
        exercise_id: ids[929]
      }, {equipment_id: " 6 ", exercise_id: ids[930]}, {
        equipment_id: " 3 ",
        exercise_id: ids[931]
      }, {equipment_id: " 16 ", exercise_id: ids[932]}, {
        equipment_id: " 9 ",
        exercise_id: ids[933]
      }, {equipment_id: " 10 ", exercise_id: ids[934]}, {
        equipment_id: " 10 ",
        exercise_id: ids[935]
      }, {equipment_id: " 10 ", exercise_id: ids[936]}, {
        equipment_id: " 10 ",
        exercise_id: ids[937]
      }, {equipment_id: " 16 ", exercise_id: ids[938]}, {
        equipment_id: " 10 ",
        exercise_id: ids[939]
      }, {equipment_id: " 1 ", exercise_id: ids[940]}, {
        equipment_id: " 1 ",
        exercise_id: ids[941]
      }, {equipment_id: " 10 ", exercise_id: ids[942]}, {
        equipment_id: " 1 ",
        exercise_id: ids[943]
      }, {equipment_id: " 3 ", exercise_id: ids[944]}, {
        equipment_id: " 15 ",
        exercise_id: ids[945]
      }, {equipment_id: " 16 ", exercise_id: ids[946]}, {
        equipment_id: " 16 ",
        exercise_id: ids[947]
      }, {equipment_id: " 16 ", exercise_id: ids[948]}, {
        equipment_id: " 16 ",
        exercise_id: ids[949]
      }, {equipment_id: " 10 ", exercise_id: ids[950]}, {
        equipment_id: " 10 ",
        exercise_id: ids[951]
      }, {equipment_id: " 6 ", exercise_id: ids[952]}, {
        equipment_id: " 6 ",
        exercise_id: ids[953]
      }, {equipment_id: " 10 ", exercise_id: ids[954]}, {
        equipment_id: " 10 ",
        exercise_id: ids[955]
      }, {equipment_id: " 10 ", exercise_id: ids[956]}, {
        equipment_id: " 10 ",
        exercise_id: ids[957]
      }, {equipment_id: " 10 ", exercise_id: ids[958]}, {
        equipment_id: " 10 ",
        exercise_id: ids[959]
      }, {equipment_id: " 10 ", exercise_id: ids[960]}, {
        equipment_id: " 16 ",
        exercise_id: ids[961]
      }, {equipment_id: " 1 ", exercise_id: ids[962]}, {
        equipment_id: " 10 ",
        exercise_id: ids[963]
      }, {equipment_id: " 10 ", exercise_id: ids[964]}, {
        equipment_id: " 10 ",
        exercise_id: ids[965]
      }, {equipment_id: " 6 ", exercise_id: ids[966]}, {
        equipment_id: " 6 ",
        exercise_id: ids[967]
      }, {equipment_id: " 6 ", exercise_id: ids[968]}, {
        equipment_id: " 6 ",
        exercise_id: ids[969]
      }, {equipment_id: " 6 ", exercise_id: ids[970]}, {
        equipment_id: " 10 ",
        exercise_id: ids[971]
      }, {equipment_id: " 16 ", exercise_id: ids[972]}, {
        equipment_id: " 3 ",
        exercise_id: ids[973]
      }, {equipment_id: " 3 ", exercise_id: ids[974]}, {
        equipment_id: " 16 ",
        exercise_id: ids[975]
      }, {equipment_id: " 3 ", exercise_id: ids[976]}, {
        equipment_id: " 3 ",
        exercise_id: ids[977]
      }, {equipment_id: " 3 ", exercise_id: ids[978]}, {
        equipment_id: " 3 ",
        exercise_id: ids[979]
      }, {equipment_id: " 16 ", exercise_id: ids[980]}, {
        equipment_id: " 3 ",
        exercise_id: ids[981]
      }, {equipment_id: " 3 ", exercise_id: ids[982]}, {
        equipment_id: " 16 ",
        exercise_id: ids[983]
      }, {equipment_id: " 12 ", exercise_id: ids[984]}, {
        equipment_id: " 12 ",
        exercise_id: ids[985]
      }, {equipment_id: " 16 ", exercise_id: ids[986]}, {
        equipment_id: " 16 ",
        exercise_id: ids[987]
      }, {equipment_id: " 16 ", exercise_id: ids[988]}, {
        equipment_id: " 12 ",
        exercise_id: ids[989]
      }, {equipment_id: " 16 ", exercise_id: ids[990]}, {
        equipment_id: " 16 ",
        exercise_id: ids[991]
      }, {equipment_id: " 16 ", exercise_id: ids[992]}, {
        equipment_id: " 10 ",
        exercise_id: ids[993]
      }, {equipment_id: " 12 ", exercise_id: ids[994]}, {
        equipment_id: " 15 ",
        exercise_id: ids[995]
      }, {equipment_id: " 16 ", exercise_id: ids[996]}, {
        equipment_id: " 16 ",
        exercise_id: ids[997]
      }, {equipment_id: " 16 ", exercise_id: ids[998]}, {
        equipment_id: " 16 ",
        exercise_id: ids[999]
      }, {equipment_id: " 16 ", exercise_id: ids[1000]}, {
        equipment_id: " 3 ",
        exercise_id: ids[1001]
      }, {equipment_id: " 10 ", exercise_id: ids[1002]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1003]
      }, {equipment_id: " 10 ", exercise_id: ids[1004]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1005]
      }, {equipment_id: " 10 ", exercise_id: ids[1006]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1007]
      }, {equipment_id: " 10 ", exercise_id: ids[1008]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1009]
      }, {equipment_id: " 10 ", exercise_id: ids[1010]}, {
        equipment_id: " 3 ",
        exercise_id: ids[1011]
      }, {equipment_id: " 16 ", exercise_id: ids[1012]}, {
        equipment_id: " 3 ",
        exercise_id: ids[1013]
      }, {equipment_id: " 16 ", exercise_id: ids[1014]}, {
        equipment_id: " 3 ",
        exercise_id: ids[1015]
      }, {equipment_id: " 9 ", exercise_id: ids[1016]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1017]
      }, {equipment_id: " 10 ", exercise_id: ids[1018]}, {
        equipment_id: " 16 ",
        exercise_id: ids[1019]
      }, {equipment_id: " 10 ", exercise_id: ids[1020]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1021]
      }, {equipment_id: " 3 ", exercise_id: ids[1022]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1023]
      }, {equipment_id: " 10 ", exercise_id: ids[1024]}, {
        equipment_id: " 16 ",
        exercise_id: ids[1025]
      }, {equipment_id: " 3 ", exercise_id: ids[1026]}, {
        equipment_id: " 16 ",
        exercise_id: ids[1027]
      }, {equipment_id: " 16 ", exercise_id: ids[1028]}, {
        equipment_id: " 16 ",
        exercise_id: ids[1029]
      }, {equipment_id: " 16 ", exercise_id: ids[1030]}, {
        equipment_id: " 9 ",
        exercise_id: ids[1031]
      }, {equipment_id: " 10 ", exercise_id: ids[1032]}, {
        equipment_id: " 1 ",
        exercise_id: ids[1033]
      }, {equipment_id: " 10 ", exercise_id: ids[1034]}, {
        equipment_id: " 1 ",
        exercise_id: ids[1035]
      }, {equipment_id: " 3 ", exercise_id: ids[1036]}, {
        equipment_id: " 6 ",
        exercise_id: ids[1037]
      }, {equipment_id: " 16 ", exercise_id: ids[1038]}, {
        equipment_id: " 16 ",
        exercise_id: ids[1039]
      }, {equipment_id: " 10 ", exercise_id: ids[1040]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1041]
      }, {equipment_id: " 1 ", exercise_id: ids[1042]}, {
        equipment_id: " 16 ",
        exercise_id: ids[1043]
      }, {equipment_id: " 16 ", exercise_id: ids[1044]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1045]
      }, {equipment_id: " 10 ", exercise_id: ids[1046]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1047]
      }, {equipment_id: " 16 ", exercise_id: ids[1048]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1049]
      }, {equipment_id: " 10 ", exercise_id: ids[1050]}, {
        equipment_id: " 3 ",
        exercise_id: ids[1051]
      }, {equipment_id: " 3 ", exercise_id: ids[1052]}, {
        equipment_id: " 3 ",
        exercise_id: ids[1053]
      }, {equipment_id: " 16 ", exercise_id: ids[1054]}, {
        equipment_id: " 16 ",
        exercise_id: ids[1055]
      }, {equipment_id: " 10 ", exercise_id: ids[1056]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1057]
      }, {equipment_id: " 16 ", exercise_id: ids[1058]}, {
        equipment_id: " 16 ",
        exercise_id: ids[1059]
      }, {equipment_id: " 16 ", exercise_id: ids[1060]}, {
        equipment_id: " 1 ",
        exercise_id: ids[1061]
      }, {equipment_id: " 1 ", exercise_id: ids[1062]}, {
        equipment_id: " 1 ",
        exercise_id: ids[1063]
      }, {equipment_id: " 1 ", exercise_id: ids[1064]}, {
        equipment_id: " 1 ",
        exercise_id: ids[1065]
      }, {equipment_id: " 16 ", exercise_id: ids[1066]}, {
        equipment_id: " 16 ",
        exercise_id: ids[1067]
      }, {equipment_id: " 16 ", exercise_id: ids[1068]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1069]
      }, {equipment_id: " 16 ", exercise_id: ids[1070]}, {
        equipment_id: " 12 ",
        exercise_id: ids[1071]
      }, {equipment_id: " 12 ", exercise_id: ids[1072]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1073]
      }, {equipment_id: " 3 ", exercise_id: ids[1074]}, {
        equipment_id: " 3 ",
        exercise_id: ids[1075]
      }, {equipment_id: " 16 ", exercise_id: ids[1076]}, {
        equipment_id: " 1 ",
        exercise_id: ids[1077]
      }, {equipment_id: " 1 ", exercise_id: ids[1078]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1079]
      }, {equipment_id: " 10 ", exercise_id: ids[1080]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1081]
      }, {equipment_id: " 10 ", exercise_id: ids[1082]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1083]
      }, {equipment_id: " 10 ", exercise_id: ids[1084]}, {
        equipment_id: " 12 ",
        exercise_id: ids[1085]
      }, {equipment_id: " 6 ", exercise_id: ids[1086]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1087]
      }, {equipment_id: " 16 ", exercise_id: ids[1088]}, {
        equipment_id: " 10 ",
        exercise_id: ids[1089]
      }, {equipment_id: " 6 ", exercise_id: ids[1090]}, {equipment_id: " 16 ", exercise_id: ids[1091]},
      ])
    });
  });
};

exports.down = function (knex, Promise) {

};
