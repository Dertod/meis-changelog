
exports.up = function(knex, Promise) {
  return knex.schema.createTable('companies', table => {
    table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
    table.string('name').notNullable();
    table.text('logo_url');
    table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
    table.timestamp('updated_at', true).defaultTo(knex.raw('now()')).notNullable();
  }).then(() => {
    return knex.schema.createTable('companies_members_types', table => {
      table.increments('id').notNullable().primary();
      table.string('title').notNullable().unique();
    })
  }).then(() => {
    return knex('companies_members_types').insert({title: 'OWNER'});
  }).then(() => {
    return knex.schema.createTable('companies_admins', table => {
      table.bigint('company_id').references('companies.id').onDelete('cascade');
      table.bigint('admin_id').references('users.id').onDelete('cascade');
      table.integer('company_member_type').references('companies_members_types.id').notNullable();
      table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
    })
  })
};

exports.down = function(knex, Promise) {

};
