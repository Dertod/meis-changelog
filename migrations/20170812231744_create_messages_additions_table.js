
exports.up = function(knex, Promise) {
  return knex.schema.createTable('messages_additions', table => {
    table.increments('id').notNullable().primary();
    table.bigint('message_id').notNullable().references('messages.id').onDelete('cascade');
    table.bigint('other_id').notNullable();
    table.bigint('messages_additions_types_id').notNullable().references('messages_additions_types.id').onDelete('cascade');
  })
};

exports.down = function(knex, Promise) {

};
