
exports.up = function(knex, Promise) {
  return knex.schema.createTable('programs_statuses', table => {
    table.increments('id').notNullable().primary();
    table.string('status').notNullable().unique();
  }).then(() => {
    return knex('programs_statuses').insert([{status: 'PUBLISHED'}, {status: 'UNPUBLISHED'}, {status: 'DELETED'}])
  }).then(() => {
    return knex.schema.table('programs', table => {
      table.dropColumn('is_published');
      table.integer('program_status_id').unsigned().references('programs_statuses.id');
    })
  })
};

exports.down = function(knex, Promise) {

};
