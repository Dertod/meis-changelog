
exports.up = function(knex, Promise) {
  return knex.schema.table('exercises_settings', table => {
    table.dropColumns('sets', 'duration', 'duration_measurement_type_id', 'rest_time', 'weight');
  }).then(() => {
    return knex.schema.createTable('sets', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.bigint('exercise_settings_id').references('exercises_settings.id').notNullable().onDelete('cascade');
      table.integer('duration');
      table.integer('duration_measurement_type_id').references('measurements_units_types');
      table.integer('rest_time');
      table.integer('weight');
      table.integer('order');
    })
  });
};

exports.down = function(knex, Promise) {

};
