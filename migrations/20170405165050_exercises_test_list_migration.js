
exports.up = function(knex, Promise) {
  return knex('exercises').insert([
    {
      name: 'Barbell bench press',
      description: 'The barbell bench press is an upper body pressing drill that builds size and strength in the upper body, specifically in the chest, triceps, and shoulders. Lying flat on a bench allows for improved stability. The exercise allows for the greatest amount of weight to used, which makes it ideal for building strength, size, and power.'
    },
    {
      name: 'Heavy Dips',
      description: 'For most, dips are traditionally perceived as a triceps exercise. However, the truth is that with a minor tweak to the position of your body during the downward portion of this exercise, you can target your pectoral muscles.'
    },
    {
      name: 'Cable Crossover',
      description: 'Because it stretches the pecs from the start position, hitting the outer pec muscle fibers. Your pulley position is determined by the area of the chest you want to target.'
    }
  ])
};

exports.down = function(knex, Promise) {
  
};
