
exports.up = function(knex, Promise) {
  return knex.schema.table('exercises', table => {
    table.boolean('has_body_weight').defaultTo(false);
    table.boolean('has_barbell').defaultTo(false);
  })
};

exports.down = function(knex, Promise) {

};
