
exports.up = function(knex, Promise) {
  return knex.schema.table('coaches_athletes', table => {
    table.unique(['coach_id', 'athlete_id']);
  }).then(() => {
    return knex.schema.table('teams', table => {
      table.bigint('team_coach_id').references('users.id').onDelete('cascade');
    })
  })
};

exports.down = function(knex, Promise) {

};
