
exports.up = function (knex, Promise) {
  return knex.schema.createTable('tickets_statuses', table => {
    table.increments('id').notNullable().primary();
    table.string('name').notNullable().unique();
  }).then(() => {
    return knex('tickets_statuses').insert([{name: 'OPENED'}, {name: 'CLOSED'}, {name: 'PENDING'}, {name: 'APPROVED'}, {name: 'DECLINED'}]);
  }).then(() => {
    return knex.schema.createTable('tickets_types', table => {
      table.increments('id').notNullable().primary();
      table.string('name').notNullable().unique();
    })
  }).then(() => {
    return knex('tickets_types').insert([{name: 'Idea'}, {name: 'Bug'}]);
  }).then(() => {
    return knex.schema.createTable('tickets', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.bigint('user_id').references('users.id').notNullable().unsigned();
      table.string('title').notNullable();
      table.string('description').notNullable();
      table.string('image_url');
      table.integer('ticket_type').references('tickets_types.id').notNullable();
      table.integer('ticket_status').references('tickets_statuses.id').notNullable();
      table.string('created_at').notNullable();
      table.string('updated_at');
    })
  }).then(() => {
    return knex.schema.createTable('comments', table => {
      table.increments('id').notNullable().primary();
      table.integer('ticket_id').references('tickets.id').notNullable().onDelete('cascade');
      table.bigint('user_id').references('users.id').notNullable().unsigned();
      table.string('text').notNullable();
      table.string('created_at').notNullable();
    })
  }).then(() => {
    return knex.schema.createTable('tickets_votes', table => {
      table.increments('id').notNullable().primary();
      table.bigint('user_id').references('users.id').notNullable().onDelete('cascade');
      table.integer('ticket_id').references('tickets.id').notNullable().onDelete('cascade');
    })
  })
};

exports.down = function (knex, Promise) {

};

