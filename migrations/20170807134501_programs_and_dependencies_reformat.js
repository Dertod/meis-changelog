
exports.up = function(knex, Promise) {
  return knex.schema.table('exercises_settings', (table) => {
    table.dropColumns('duration', 'sets', 'day_id');
    table.renameColumn('duration_measurement_type_id', 'duration_type_id');
    table.integer('day_number').unsigned();
    table.bigint('program_id').references('programs.id').notNullable().onDelete('cascade');
  }).then(() => {
    return knex.schema.createTable('sets', table => {
      table.bigint('id').defaultTo(knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`)).notNullable().primary();
      table.integer('sets').unsigned();
      table.string('duration').unsigned();
      table.integer('weight').unsigned();
      table.integer('week_number').unsigned();
      table.bigint('exercise_setting_id').references('exercises_settings.id').notNullable().onDelete('cascade');
    })
  }).then(() => {
    return knex.schema.dropTable('days');
  }).then(() => {
    return knex.schema.dropTable('weeks')
  })
};

exports.down = function(knex, Promise) {

};
