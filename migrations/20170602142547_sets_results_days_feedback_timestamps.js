
exports.up = function(knex, Promise) {
  return knex.schema.table('sets_results', table => {
    table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
    table.timestamp('updated_at', true).defaultTo(knex.raw('now()')).notNullable();
  }).then(() => {
    return knex.schema.table('days_feedback', table => {
      table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
      table.timestamp('updated_at', true).defaultTo(knex.raw('now()')).notNullable();
    })
  })
};

exports.down = function(knex, Promise) {
  
};
