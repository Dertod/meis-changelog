
exports.up = function(knex, Promise) {
  return knex.schema.table('exercises_results', table => {
    table.integer('day_number');
    table.integer('weight');
    table.dropColumn('exercise_settings_id');
    table.bigint('program_id').references('programs.id').onDelete('cascade').notNullable();
  }).then(() => {
    return knex.schema.table('days_feedback', (table) => {
      table.bigint('program_id').references('programs.id').onDelete('cascade').notNullable();
      table.integer('day_number');
      table.dropColumn('day_id');
    })
  })
};

exports.down = function(knex, Promise) {

};
