exports.up = function (knex, Promise) {

  return knex.schema.createTable('exercises_categories_types', table => {
    table.increments('id').notNullable().primary();
    table.string('category_name').notNullable();
  }).then(()=> {
    return knex('exercises_categories_types').insert([
      {category_name: 'Arms'},
      {category_name: 'Legs'},
      {category_name: 'Abs'},
      {category_name: 'Chest'},
      {category_name: 'Back'},
      {category_name: 'Shoulders'},
      {category_name: 'Calves'},
      {category_name: 'Cardio'},
    ]);
  }).then(()=> {
    return knex.schema.table('exercises', table => {
      table.integer('category_type_id').references('exercises_categories_types.id');
      table.dropColumn('has_barbell');
      table.dropColumn('has_body_weight');
      table.dropColumn('repeatly');
      table.dropColumn('movement_type_id');
    }).then(()=> {
      return knex.schema.createTable('exercises_equipments_types', table => {
        table.increments('id').notNullable().primary();
        table.string('equipment_name').notNullable();
      }).then(()=> {
        return knex('exercises_equipments_types').insert([
          {equipment_name: 'Dumbbell'},
          {equipment_name: 'Bench'},
          {equipment_name: 'Barbell'},
          {equipment_name: 'Gym mat'},
          {equipment_name: 'Incline bench'},
          {equipment_name: 'Kettlebell'},
          {equipment_name: 'Pull-up bar'},
          {equipment_name: 'SZ-Bar'},
          {equipment_name: 'Exercise Ball'},
          {equipment_name: 'Bodyweight'},
          {equipment_name: 'Cable'},
          {equipment_name: 'Medicine Ball'},
          {equipment_name: 'E-Z Curl Bar'},
          {equipment_name: 'Foam Roll'},
          {equipment_name: 'Machine'},
          {equipment_name: 'Other'},
        ]);
      }).then(()=> {
        return knex.schema.createTable('exercises_equipments', table => {
          table.bigint('equipment_id').references('exercises_equipments_types.id').notNullable().onDelete('cascade');
          table.bigint('exercise_id').references('exercises.id').notNullable().onDelete('cascade');
          table.primary(['equipment_id', 'exercise_id']);
        }).then(()=> {
          return knex.schema.createTable('muscles_group', table => {
            table.increments('id').notNullable().primary();
            table.string('muscles_group_name').notNullable();
          }).then(()=> {
            return knex('muscles_group').insert([
              {muscles_group_name: 'Main muscles'},
              {muscles_group_name: 'Secondary muscles'},
            ]);
          }).then(()=> {
            return knex.schema.createTable('exercises_muscles_types', table => {
              table.increments('id').notNullable().primary();
              table.string('muscle_name').notNullable();
              table.string('muscle_image').notNullable();
            }).then(()=> {
              return knex('exercises_muscles_types').insert([
                {muscle_name: 'Chest', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Chest.gif'},
                {muscle_name: 'Forearms', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Forearms.gif'},
                {muscle_name: 'Lats', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Lats.gif'},
                {muscle_name: 'Middle Back', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Middle%20Back.gif'},
                {muscle_name: 'Lower Back', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Lower%20Back.gif'},
                {muscle_name: 'Neck', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Neck.gif'},
                {muscle_name: 'Quadriceps', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Quadriceps.gif'},
                {muscle_name: 'Hamstrings', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Hamstrings.gif'},
                {muscle_name: 'Calves', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Calves.gif'},
                {muscle_name: 'Triceps', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Triceps.gif'},
                {muscle_name: 'Traps', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Traps.gif'},
                {muscle_name: 'Shoulders', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Shoulders.gif'},
                {muscle_name: 'Abdominals', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Abdominals.gif'},
                {muscle_name: 'Glutes', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Glutes.gif'},
                {muscle_name: 'Adductors', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Adductors.gif'},
                {muscle_name: 'Abductors', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Abductors.gif'},
                {muscle_name: 'Biceps', muscle_image: 'https://storage.googleapis.com/meisterfit/Muscles/Biceps.gif'},
              ]).then(()=> {
                return knex.schema.createTable('exercises_muscles', table => {
                  table.bigint('muscle_id').references('exercises_muscles_types.id').notNullable().onDelete('cascade');
                  table.bigint('exercise_id').references('exercises.id').notNullable().onDelete('cascade');
                  table.bigint('muscle_group').references('muscles_group.id').notNullable().onDelete('cascade');
                  table.primary(['muscle_id', 'exercise_id']);
                }).then(()=>{
                  return knex.schema.dropTable('exercise_movement_types');
                });
              });
            });
          });
        })
      });
    });
  });
};

exports.down = function (knex, Promise) {

};
