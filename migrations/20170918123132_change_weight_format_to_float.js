
exports.up = function(knex, Promise) {
  return knex.schema.table('draft_sets', table => {
    table.dropColumn('weight');
  }).then(() => {
    return knex.schema.table('exercises_results_sets', table => {
      table.dropColumn('weight');
    }).then(() => {
      return knex.schema.table('sets', table => {
        table.dropColumn('weight');
      })
    }).then(() => {
      return knex.schema.table('users_settings', table => {
        table.dropColumn('weight');
      })
    })
  }).then(() => {
    return knex.schema.table('draft_sets', table => {
      table.float('weight');
    }).then(() => {
      return knex.schema.table('exercises_results_sets', table => {
        table.float('weight');
      })
    }).then(() => {
      return knex.schema.table('sets', table => {
        table.float('weight');
      })
    }).then(() => {
      return knex.schema.table('users_settings', table => {
        table.float('weight');
      })
    })
  })
};

exports.down = function(knex, Promise) {

};
