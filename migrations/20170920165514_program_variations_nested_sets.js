
exports.up = function(knex, Promise) {
  return knex.schema.table('programs', table => {
    table.integer('left_key').notNullable().defaultTo(1);
    table.integer('right_key').notNullable().defaultTo(2);
    table.integer('level').notNullable().defaultTo(0);
    table.index(['left_key', 'right_key', 'level'], 'left_key');
    table.bigint('main_program_id').references('programs.id').onDelete('set null');
  }).then(() => {
    return knex.schema.table('draft_programs', table => {
      table.integer('left_key').notNullable().defaultTo(1);
      table.integer('right_key').notNullable().defaultTo(2);
      table.integer('level').notNullable().defaultTo(0);
      table.index(['left_key', 'right_key', 'level'], 'draft_left_key');
      table.bigint('main_program_id').references('programs.id').onDelete('set null');
    })
  })
};

exports.down = function(knex, Promise) {

};
