
const allLanguages = [
  {title: 'ab', value: 'Abkhazian'},
  {title: 'aa', value: 'Afar'},
  {title: 'af', value: 'Afrikaans'},
  {title: 'sq', value: 'Albanian'},
  {title: 'am', value: 'Amharic'},
  {title: 'ar', value: 'Arabic'},
  {title: 'an', value: 'Aragonese'},
  {title: 'hy', value: 'Armenian'},
  {title: 'as', value: 'Assamese'},
  {title: 'ay', value: 'Aymara'},
  {title: 'az', value: 'Azerbaijani'},
  {title: 'ba', value: 'Bashkir'},
  {title: 'eu', value: 'Basque'},
  {title: 'bn', value: 'Bengali'},
  {title: 'dz', value: 'Bhutani'},
  {title: 'bh', value: 'Bihari'},
  {title: 'bi', value: 'Bislama'},
  {title: 'br', value: 'Breton'},
  {title: 'bg', value: 'Bulgarian'},
  {title: 'my', value: 'Burmese'},
  {title: 'be', value: 'Belarusian'},
  {title: 'km', value: 'Cambodian'},
  {title: 'ca', value: 'Catalan'},
  {title: 'zh', value: 'Chinese'},
  {title: 'co', value: 'Corsican'},
  {title: 'hr', value: 'Croatian'},
  {title: 'cs', value: 'Czech'},
  {title: 'da', value: 'Danish'},
  {title: 'nl', value: 'Dutch'},
  {title: 'eo', value: 'Esperanto'},
  {title: 'et', value: 'Estonian'},
  {title: 'fo', value: 'Faeroese'},
  {title: 'fa', value: 'Farsi'},
  {title: 'fj', value: 'Fiji'},
  {title: 'fi', value: 'Finnish'},
  {title: 'fr', value: 'French'},
  {title: 'fy', value: 'Frisian'},
  {title: 'gl', value: 'Galician'},
  {title: 'gd', value: 'Gaelic'},
  {title: 'gv', value: 'Gaelic'},
  {title: 'ka', value: 'Georgian'},
  {title: 'de', value: 'German'},
  {title: 'el', value: 'Greek'},
  {title: 'kl', value: 'Greenlandic'},
  {title: 'gn', value: 'Guarani'},
  {title: 'gu', value: 'Gujarati'},
  {title: 'ht', value: 'Haitian Creole'},
  {title: 'ha', value: 'Hausa'},
  {title: 'he', value: 'Hebrew'},
  {title: 'hi', value: 'Hindi'},
  {title: 'hu', value: 'Hungarian'},
  {title: 'is', value: 'Icelandic'},
  {title: 'io', value: 'Ido'},
  {title: 'id', value: 'Indonesian'},
  {title: 'ia', value: 'Interlingua'},
  {title: 'ie', value: 'Interlingue'},
  {title: 'iu', value: 'Inuktitut'},
  {title: 'ik', value: 'Inupiak'},
  {title: 'ga', value: 'Irish'},
  {title: 'it', value: 'Italian'},
  {title: 'ja', value: 'Japanese'},
  {title: 'jv', value: 'Javanese'},
  {title: 'kn', value: 'Kannada'},
  {title: 'ks', value: 'Kashmiri'},
  {title: 'kk', value: 'Kazakh'},
  {title: 'rw', value: 'Kinyarwanda'},
  {title: 'ky', value: 'Kirghiz'},
  {title: 'rn', value: 'Kirundi'},
  {title: 'ko', value: 'Korean'},
  {title: 'ku', value: 'Kurdish'},
  {title: 'lo', value: 'Laothian'},
  {title: 'la', value: 'Latin'},
  {title: 'lv', value: 'Latvian'},
  {title: 'li', value: 'Limburgish'},
  {title: 'ln', value: 'Lingala'},
  {title: 'lt', value: 'Lithuanian'},
  {title: 'mk', value: 'Macedonian'},
  {title: 'mg', value: 'Malagasy'},
  {title: 'ms', value: 'Malay'},
  {title: 'ml', value: 'Malayalam'},
  {title: 'mt', value: 'Maltese'},
  {title: 'mi', value: 'Maori'},
  {title: 'mr', value: 'Marathi'},
  {title: 'mo', value: 'Moldavian'},
  {title: 'mn', value: 'Mongolian'},
  {title: 'na', value: 'Nauru'},
  {title: 'ne', value: 'Nepali'},
  {title: 'no', value: 'Norwegian'},
  {title: 'oc', value: 'Occitan'},
  {title: 'or', value: 'Oriya'},
  {title: 'om', value: 'Oromo'},
  {title: 'ps', value: 'Pashto'},
  {title: 'pl', value: 'Polish'},
  {title: 'pt', value: 'Portuguese'},
  {title: 'pa', value: 'Punjabi'},
  {title: 'qu', value: 'Quechua'},
  {title: 'rm', value: 'Rhaeto-Romance'},
  {title: 'ro', value: 'Romanian'},
  {title: 'sm', value: 'Samoan'},
  {title: 'sg', value: 'Sangro'},
  {title: 'sa', value: 'Sanskrit'},
  {title: 'sr', value: 'Serbian'},
  {title: 'sh', value: 'Serbo-Croatian'},
  {title: 'st', value: 'Sesotho'},
  {title: 'tn', value: 'Setswana'},
  {title: 'sn', value: 'Shona'},
  {title: 'ii', value: 'Sichuan Yi'},
  {title: 'sd', value: 'Sindhi'},
  {title: 'si', value: 'Sinhalese'},
  {title: 'ss', value: 'Siswati'},
  {title: 'sk', value: 'Slovak'},
  {title: 'sl', value: 'Slovenian'},
  {title: 'so', value: 'Somali'},
  {title: 'es', value: 'Spanish'},
  {title: 'su', value: 'Sundanese'},
  {title: 'sw', value: 'Swahili'},
  {title: 'sv', value: 'Swedish'},
  {title: 'tl', value: 'Tagalog'},
  {title: 'tg', value: 'Tajik'},
  {title: 'ta', value: 'Tamil'},
  {title: 'tt', value: 'Tatar'},
  {title: 'te', value: 'Telugu'},
  {title: 'th', value: 'Thai'},
  {title: 'bo', value: 'Tibetan'},
  {title: 'ti', value: 'Tigrinya'},
  {title: 'to', value: 'Tonga'},
  {title: 'ts', value: 'Tsonga'},
  {title: 'tr', value: 'Turkish'},
  {title: 'tk', value: 'Turkmen'},
  {title: 'tw', value: 'Twi'},
  {title: 'ug', value: 'Uighur'},
  {title: 'uk', value: 'Ukrainian'},
  {title: 'ur', value: 'Urdu'},
  {title: 'uz', value: 'Uzbek'},
  {title: 'vi', value: 'Vietnamese'},
  {title: 'vo', value: 'Volapük'},
  {title: 'wa', value: 'Wallon'},
  {title: 'cy', value: 'Welsh'},
  {title: 'wo', value: 'Wolof'},
  {title: 'xh', value: 'Xhosa'},
  {title: 'yi', value: 'Yiddish'},
  {title: 'yo', value: 'Yoruba'},
  {title: 'zu', value: 'Zulu'}
];
const emailTemplates = [
  {
    html: `<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title></title>
  <style type="text/css">

    #outlook a {
      padding: 0;
    }

    .ReadMsgBody {
      width: 100%;
    }

    .ExternalClass {
      width: 100%;
    }

    .ExternalClass * {
      line-height: 100%;
    }

    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table, td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    p {
      display: block;
      margin: 13px 0;
    }

  </style>
  <!--[if !mso]><!-->
  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300);
  </style>
  <style type="text/css">
    @media only screen and (max-width: 480px) {
      @-ms-viewport {
        width: 320px;
      }
      @viewport {
        width: 320px;
      }
    }
  </style>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300" rel="stylesheet" type="text/css">
  <!--<![endif]-->
  <style type="text/css">
    @media only screen and (min-width: 480px) {
      .mj-column-per-100, * [aria-labelledby="mj-column-per-100"] {
        width: 100% !important;
      }
    }</style>
</head>
<body id="YIELD_MJML" style="">
<div class="mj-body"><!--[if mso]>
  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="width:100%;">
    <tr>
      <td>
  <![endif]-->
  <div style="margin:0 auto;max-width:100%;background:#EDEDF1;">
    <table class="" cellpadding="0" cellspacing="0" style="width:100%;font-size:0px;background:#ffffff;" align="center">
      <tbody>
      <tr>
        <td style="text-align:center;vertical-align:top;font-size:0;padding:20px 0;"><!--[if mso]>
          <table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td style="width:100%;">
          <![endif]-->
          <div style="vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;"
               class="mj-column-per-100" aria-labelledby="mj-column-per-100">
            <table width="100%">
              <tbody>
              <tr>
                <td colspan="3" style="font-size:0;padding:10px 25px;" align="center">
                  <table cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;"
                         align="center">
                    <tbody>
                    <tr>
                      <td></td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td colspan="3"
                    style="font-size:0;padding:10px 25px;-moz-border-radius:5px;-webkit-border-radius:5px; border-radius:5px;">
                  <!--[if mso]>
                  <table style="font-size: 1px; margin: 0px; border-top: 4px solid #FFFFFF; width: 100%;"></table>
                  <![endif]--></td>
              </tr>

              <tr >
                <td style="width: 25%"></td>
                <td 
                  style="width: 50%;font-size:0;padding:10px 25px;border: 2px solid #f2f2f2;background:#FBFBFB;-moz-border-radius:5px;-webkit-border-radius:5px; border-radius:5px;"
                  align="left">
                  <div class="mj-content"
                       style="padding: 0px 0px 80px 0px;cursor:auto;color:#000000;font-family:helvetica;font-size:14px;line-height:22px; min-width: 470px">` +
                    '<p>Dear ${name},</p><br><p>You have been added as an administrator to MeisterFit service.</p><p>Click on the following link to set a password and get access your account:</p> <p><a href="${link}">${link}</a></p>' +
                  `</div>
                </td>
                <td style="width: 25%"></td>
              </tr>
              </tbody>
            </table>
          </div>
          <!--[if mso]>
          </td></tr></table>
          <![endif]--></td>
      </tr>
      </tbody>
    </table>
  </div>
  <!--[if mso]>
  </td></tr></table>
  <![endif]--></div>
</body>
</html>
`,
  subject: 'Welcome to MeisterFit',
  email_template_type_id: 1,
  language_id: 1
  },
  {html: `<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title></title>
  <style type="text/css">

    #outlook a {
      padding: 0;
    }

    .ReadMsgBody {
      width: 100%;
    }

    .ExternalClass {
      width: 100%;
    }

    .ExternalClass * {
      line-height: 100%;
    }

    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table, td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    p {
      display: block;
      margin: 13px 0;
    }

  </style>
  <!--[if !mso]><!-->
  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300);
  </style>
  <style type="text/css">
    @media only screen and (max-width: 480px) {
      @-ms-viewport {
        width: 320px;
      }
      @viewport {
        width: 320px;
      }
    }
  </style>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300" rel="stylesheet" type="text/css">
  <!--<![endif]-->
  <style type="text/css">
    @media only screen and (min-width: 480px) {
      .mj-column-per-100, * [aria-labelledby="mj-column-per-100"] {
        width: 100% !important;
      }
    }</style>
</head>
<body id="YIELD_MJML" style="">
<div class="mj-body"><!--[if mso]>
  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="width:100%;">
    <tr>
      <td>
  <![endif]-->
  <div style="margin:0 auto;max-width:100%;background:#EDEDF1;">
    <table class="" cellpadding="0" cellspacing="0" style="width:100%;font-size:0px;background:#ffffff;" align="center">
      <tbody>
      <tr>
        <td style="text-align:center;vertical-align:top;font-size:0;padding:20px 0;"><!--[if mso]>
          <table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td style="width:100%;">
          <![endif]-->
          <div style="vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;"
               class="mj-column-per-100" aria-labelledby="mj-column-per-100">
            <table width="100%">
              <tbody>
              <tr>
                <td colspan="3" style="font-size:0;padding:10px 25px;" align="center">
                  <table cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;"
                         align="center">
                    <tbody>
                    <tr>
                      <td></td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td colspan="3"
                    style="font-size:0;padding:10px 25px;-moz-border-radius:5px;-webkit-border-radius:5px; border-radius:5px;">
                  <!--[if mso]>
                  <table style="font-size: 1px; margin: 0px; border-top: 4px solid #FFFFFF; width: 100%;"></table>
                  <![endif]--></td>
              </tr>

              <tr >
                <td style="width: 25%"></td>
                <td 
                  style="width: 50%;font-size:0;padding:10px 25px;border: 2px solid #f2f2f2;background:#FBFBFB;-moz-border-radius:5px;-webkit-border-radius:5px; border-radius:5px;"
                  align="left">
                  <div class="mj-content"
                       style="padding: 0px 0px 80px 0px;cursor:auto;color:#000000;font-family:helvetica;font-size:14px;line-height:22px; min-width: 470px">` +
  '<p>Dear ${name},</p><br><p>You have requested the recovery password in MeisterFit service.</p><p>Click on the following link to reset your password:</p> <p><a href="${link}">${link}</a></p>' +
  `</div>
                </td>
                <td style="width: 25%"></td>
              </tr>
              </tbody>
            </table>
          </div>
          <!--[if mso]>
          </td></tr></table>
          <![endif]--></td>
      </tr>
      </tbody>
    </table>
  </div>
  <!--[if mso]>
  </td></tr></table>
  <![endif]--></div>
</body>
</html>
`,
    subject: 'MeisterFit Recovery.',
    email_template_type_id: 3,
    language_id: 1
  },
  {html: `<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title></title>
  <style type="text/css">

    #outlook a {
      padding: 0;
    }

    .ReadMsgBody {
      width: 100%;
    }

    .ExternalClass {
      width: 100%;
    }

    .ExternalClass * {
      line-height: 100%;
    }

    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table, td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    p {
      display: block;
      margin: 13px 0;
    }

  </style>
  <!--[if !mso]><!-->
  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300);
  </style>
  <style type="text/css">
    @media only screen and (max-width: 480px) {
      @-ms-viewport {
        width: 320px;
      }
      @viewport {
        width: 320px;
      }
    }
  </style>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300" rel="stylesheet" type="text/css">
  <!--<![endif]-->
  <style type="text/css">
    @media only screen and (min-width: 480px) {
      .mj-column-per-100, * [aria-labelledby="mj-column-per-100"] {
        width: 100% !important;
      }
    }</style>
</head>
<body id="YIELD_MJML" style="">
<div class="mj-body"><!--[if mso]>
  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="width:100%;">
    <tr>
      <td>
  <![endif]-->
  <div style="margin:0 auto;max-width:100%;background:#EDEDF1;">
    <table class="" cellpadding="0" cellspacing="0" style="width:100%;font-size:0px;background:#ffffff;" align="center">
      <tbody>
      <tr>
        <td style="text-align:center;vertical-align:top;font-size:0;padding:20px 0;"><!--[if mso]>
          <table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td style="width:100%;">
          <![endif]-->
          <div style="vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;"
               class="mj-column-per-100" aria-labelledby="mj-column-per-100">
            <table width="100%">
              <tbody>
              <tr>
                <td colspan="3" style="font-size:0;padding:10px 25px;" align="center">
                  <table cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;"
                         align="center">
                    <tbody>
                    <tr>
                      <td></td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td colspan="3"
                    style="font-size:0;padding:10px 25px;-moz-border-radius:5px;-webkit-border-radius:5px; border-radius:5px;">
                  <!--[if mso]>
                  <table style="font-size: 1px; margin: 0px; border-top: 4px solid #FFFFFF; width: 100%;"></table>
                  <![endif]--></td>
              </tr>

              <tr >
                <td style="width: 25%"></td>
                <td 
                  style="width: 50%;font-size:0;padding:10px 25px;border: 2px solid #f2f2f2;background:#FBFBFB;-moz-border-radius:5px;-webkit-border-radius:5px; border-radius:5px;"
                  align="left">
                  <div class="mj-content"
                       style="padding: 0px 0px 80px 0px;cursor:auto;color:#000000;font-family:helvetica;font-size:14px;line-height:22px; min-width: 470px">` +
  '<p>Dear ${name},</p><br><p>You have been changed your email in Compomatic system to following: ${newEmail}</p><p>If you didn\'t do this or change your email by mistake, you can return to your first email ${defaultEmail} by clicking:</p> <p><a href="${link}">${link}</a></p><p>If everything is OK, then you can ignore this letter.</p>' +
  `</div>
                </td>
                <td style="width: 25%"></td>
              </tr>
              </tbody>
            </table>
          </div>
          <!--[if mso]>
          </td></tr></table>
          <![endif]--></td>
      </tr>
      </tbody>
    </table>
  </div>
  <!--[if mso]>
  </td></tr></table>
  <![endif]--></div>
</body>
</html>
`,
    subject: 'Email was changed',
    email_template_type_id: 4,
    language_id: 1
  },
  {
    html: `<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title></title>
  <style type="text/css">

    #outlook a {
      padding: 0;
    }

    .ReadMsgBody {
      width: 100%;
    }

    .ExternalClass {
      width: 100%;
    }

    .ExternalClass * {
      line-height: 100%;
    }

    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table, td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    p {
      display: block;
      margin: 13px 0;
    }

  </style>
  <!--[if !mso]><!-->
  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300);
  </style>
  <style type="text/css">
    @media only screen and (max-width: 480px) {
      @-ms-viewport {
        width: 320px;
      }
      @viewport {
        width: 320px;
      }
    }
  </style>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300" rel="stylesheet" type="text/css">
  <!--<![endif]-->
  <style type="text/css">
    @media only screen and (min-width: 480px) {
      .mj-column-per-100, * [aria-labelledby="mj-column-per-100"] {
        width: 100% !important;
      }
    }</style>
</head>
<body id="YIELD_MJML" style="">
<div class="mj-body"><!--[if mso]>
  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="width:100%;">
    <tr>
      <td>
  <![endif]-->
  <div style="margin:0 auto;max-width:100%;background:#EDEDF1;">
    <table class="" cellpadding="0" cellspacing="0" style="width:100%;font-size:0px;background:#ffffff;" align="center">
      <tbody>
      <tr>
        <td style="text-align:center;vertical-align:top;font-size:0;padding:20px 0;"><!--[if mso]>
          <table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td style="width:100%;">
          <![endif]-->
          <div style="vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;"
               class="mj-column-per-100" aria-labelledby="mj-column-per-100">
            <table width="100%">
              <tbody>
              <tr>
                <td colspan="3" style="font-size:0;padding:10px 25px;" align="center">
                  <table cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;"
                         align="center">
                    <tbody>
                    <tr>
                      <td></td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td colspan="3"
                    style="font-size:0;padding:10px 25px;-moz-border-radius:5px;-webkit-border-radius:5px; border-radius:5px;">
                  <!--[if mso]>
                  <table style="font-size: 1px; margin: 0px; border-top: 4px solid #FFFFFF; width: 100%;"></table>
                  <![endif]--></td>
              </tr>

              <tr >
                <td style="width: 25%"></td>
                <td 
                  style="width: 50%;font-size:0;padding:10px 25px;border: 2px solid #f2f2f2;background:#FBFBFB;-moz-border-radius:5px;-webkit-border-radius:5px; border-radius:5px;"
                  align="left">
                  <div class="mj-content"
                       style="padding: 0px 0px 80px 0px;cursor:auto;color:#000000;font-family:helvetica;font-size:14px;line-height:22px; min-width: 470px">` +
  '<p>Dear ${name},</p><br><p>You have singed up in MeisterFit system.</p><p>Click on the following link confirm your email:</p> <p><a href="${link}">${link}</a></p>' +
  `</div>
                </td>
                <td style="width: 25%"></td>
              </tr>
              </tbody>
            </table>
          </div>
          <!--[if mso]>
          </td></tr></table>
          <![endif]--></td>
      </tr>
      </tbody>
    </table>
  </div>
  <!--[if mso]>
  </td></tr></table>
  <![endif]--></div>
</body>
</html>
`,
    subject: 'Confirm your email',
    email_template_type_id: 2,
    language_id: 1
  }
];

exports.up = function(knex, Promise) {
  return knex.schema.createTable('email_templates_types', table => {
    table.increments('id').notNullable().unsigned().primary();
    table.string('title');
  }).then(() => {
    return knex('email_templates_types').insert([{title: 'INVITE NEW ADMIN'}, {title: 'CONFIRM EMAIL'}, {title: 'FORGOT PASSWORD'}, {title: 'DECLINE EMAIL CHANGE'}])
  }).then(() => {
    return knex.schema.createTable('languages', table => {
      table.increments('id').notNullable().unsigned().primary();
      table.string('title');
      table.string('value');
    }).then(() => {
      return knex('languages').insert(allLanguages);
    })
  }).then(() => {
    return knex.schema.createTable('email_templates', table => {
      table.increments('id').notNullable().unsigned().primary();
      table.text('html', 'longtext').notNullable();
      table.text('subject').notNullable();
      table.integer('email_template_type_id').references('email_templates_types.id').unsigned().notNullable();
      table.integer('language_id').references('languages.id').unsigned().notNullable();
      table.timestamp('created_at', true).defaultTo(knex.raw('now()')).notNullable();
      table.timestamp('updated_at', true).defaultTo(knex.raw('now()')).notNullable();
    }).then(() => {
      return knex('email_templates').insert(emailTemplates);
    })
  })
};

exports.down = function(knex, Promise) {

};
