# Before connect to database need to enable extensions
``` sql 
create extension pgcrypto; 
create extension cube;
create extension earthdistance;
``` 

# Authorization and authentication

**Need to add in header**:
```javascript
{
  Authorization: Bearer [token]
}
```

### POST /auth/signin
  
**Body**

```javascript
{
  "email": string,
  "password" : string
}
```  
  
**Authentication**
  
No authentication headers are required.
  
**Response**
  
```javascript
{
  "token": string,
  "ttl": number
}

```

# Registration

### POST /auth/signup

**Body**

```javascript
{
  "email": string,
  "password" : string,
  "user_type": string,
  "name": string
}
```

**Authentication**

No authentication headers are required.

**Response**

```javascript
{
  "token": string,
  "ttl": number
}

```

# Confirmation of registered user

### POST /auth/email/confirm

**Body**

```javascript
{
  "token": string
}
```

**Authentication**

No authentication headers are required.

**Response**

```javascript
{
  "token": string,
  "ttl": number
}

```

# Invite new admin

### POST /admins

**Body**

```javascript
{
  "email": string,
  "name": string
}
```

**Need to add in header**:
```javascript
{
  Authorization: Bearer [token]
}
```

**Response**

```javascript
{
  "email": string,
  "name": string
}

```

# Create password of an invited admin

### POST /auth/password

**Body**

```javascript
{
  "token": string,
  "password": string
}
```

**Authentication**

No authentication headers are required.

**Response**

```javascript
{
  "email": string
}

```

# Create new password instead of forgotten one

### POST /auth/password/forgot

**Body**

```javascript
{
  "token": string,
  "password": string
}
```

**Authentication**

No authentication headers are required.

**Response**

```javascript
{
  "email": string
}
```

# Create new athlete

### POST /athletes

**Need to add in header**:
```javascript
{
  Authorization: Bearer [token]
}
```
**Body**

```javascript
{
  "email": string,
  "name": string
}
```
**Response**

```javascript
{
  "id": string
}
```

# Get a list of athletes

### GET /athletes

**Need to add in header**:
```javascript
{
  Authorization: Bearer [token]
}
```
**Response**

```javascript
{
  "title": string,
  "user_role": string
}
```

# Delete of the athlete

### DELETE /athletes/:athleteId

**Need to add in header**:
```javascript
{
  Authorization: Bearer [token]
}
```
**Body**

```javascript
{
  "email": string,
  "name": string
}
```
**Response**

```javascript
{
  "coach_id": string,
  "athlete_id": string
}
```

# Get data of the athlete

### GET /athletes/:athleteId

**Need to add in header**:
```javascript
{
  Authorization: Bearer [token]
}
```
**Response**

```javascript
{
  "id": string,
  "name": string,
  "email": string,
  "hash_password": string,
  "user_role": string,
  "created_at": date,
  "updated_at": date,
  "is_email_confirmed": string,
  "user_settings_id": string,
  "profile_image_url": string,
  "weight": number,
  "height": number,
  "age": number
}
```