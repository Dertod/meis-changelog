pipeline {
    agent {
        label 'k8s'
    }

    environment {
      NAMESPACE = "development"
      APP_NAME = "meisterfit-api"
      REPOSITORY_URL = "eu.gcr.io/meisterfit/${env.APP_NAME}"
      GOOGLE_APPLICATION_CREDENTIALS = "${env.HOME}gcloud-service-key.json"
      imageTag = "${env.REPOSITORY_URL}:${env.BRANCH_NAME}-${env.BUILD_NUMBER}"
    }

    stages {
        stage('Set environment') {
          when {
            branch 'prod'
          }
          steps {
            script {
              env.NAMESPACE = 'production'
            }
          }
        }

        stage('Init') {
            steps {
              sh "mkdir ${HOME}/.ssh/"
              sh "echo ${env.GCLOUD_SERVICE_KEY} | python -m base64 -d > ${env.GOOGLE_APPLICATION_CREDENTIALS}"
              sh "echo ${env.SSH_BITBUCKET_PUBLIC} | python -m base64 -d > ${HOME}.ssh/id_rsa.pub"
              sh "echo ${env.SSH_BITBUCKET_PRIVATE} | python -m base64 -d > ${HOME}.ssh/id_rsa"
              sh "echo ${env.SSH_BITBUCKET_CONFIG} | python -m base64 -d > ${HOME}.ssh/config"
              sh "ssh-keyscan bitbucket.org >> ${HOME}.ssh/known_hosts"
              sh "chmod 600 ${HOME}.ssh/id_rsa"
              sh "gcloud auth activate-service-account --key-file ${env.GOOGLE_APPLICATION_CREDENTIALS}"
              sh "docker login -u _json_key -p \"\$(cat ${GOOGLE_APPLICATION_CREDENTIALS})\" https://eu.gcr.io"
              sh "git clone git@bitbucket.org:kingmuffin/meisterfit-kubernetes.git ${HOME}meisterfit-kubernetes"
            }
        }
        stage('Build docker image') {
            steps {
              sh "docker build --build-arg BUILD_NUMBER=${env.BUILD_NUMBER} --build-arg DEFAULT_NODE_ENV=${env.NAMESPACE} -t ${env.imageTag} ."
            }
        }
        stage('Run tests') {
            steps {
              sh "docker run ${env.imageTag} /bin/bash -c 'yarn install && node ./node_modules/better-npm-run test'"
            }
        }
        stage('Push image to registry') {
            when {
              anyOf { branch 'dev'; branch 'prod' }
            }
            steps {
              sh "gcloud docker -- push ${env.imageTag}"
            }
        }
        stage('Deploy application') {
            when {
              anyOf { branch 'dev'; branch 'prod' }
            }
            steps {
              sh "cat ${HOME}meisterfit-kubernetes/meisterfit/deployment.yaml | ktmpl - --parameter NAMESPACE ${env.NAMESPACE} --parameter APP_NAME ${env.APP_NAME} --parameter IMAGE_URL ${env.imageTag} | kubectl apply -f -"
              sh "python ${HOME}meisterfit-kubernetes/meisterfit/gcloudImages.py ${env.APP_NAME} ${env.BRANCH_NAME}"
            }
        }
    }

    post {
        failure {
          echo "The Pipeline failed :("
        }
    }
}
