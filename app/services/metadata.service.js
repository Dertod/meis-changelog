'use strict';
const _ = require('lodash');

class MetadataService {
  async createOrUpdateMetadata({metadata, companyId}, knex) {
    let metadataArray = metadata.map(metaObject => {
      return {..._.pick(metaObject, ['key', 'value']), company_id: companyId}
    });

    let query = knex('metadata').insert(metadataArray).toString();
    query += ' ON CONFLICT (key, value, company_id) DO UPDATE set company_id = EXCLUDED.company_id returning id;';

    return await knex.raw(query);
  }

  async assignMetadataToAthlete({metadata, companyId, athleteId}, knex) {
    let metadataIds = await this.createOrUpdateMetadata({metadata, companyId}, knex);

    let query = knex('athletes_metadata').insert(metadataIds.rows.map(metadataId => {
      return {metadata_id: metadataId.id, athlete_id: athleteId}
    })).toString();
    query += ' ON CONFLICT (metadata_id, athlete_id) DO UPDATE set metadata_id = EXCLUDED.metadata_id;';

    await knex.raw(query);
  }

  async unAssignAllMetadataFromAthlete({athleteId}, knex) {
    return await knex('athletes_metadata')
      .delete()
      .where({athlete_id: athleteId})
      .returning('metadata_id');
  }

  async assignMetadataToExercise({metadata, authorId, exerciseId}, knex) {
    let [companyId] = await knex('companies_admins').select('company_id').where({admin_id: authorId});

    let metadataIds = await this.createOrUpdateMetadata({metadata, companyId: companyId.company_id}, knex);

    let query = knex('exercises_metadata').insert(metadataIds.rows.map(metadataId => {
      return {metadata_id: metadataId.id, exercise_id: exerciseId}
    })).toString();
    query += ' ON CONFLICT (metadata_id, exercise_id) DO UPDATE set metadata_id = EXCLUDED.metadata_id;';

    await knex.raw(query);
  }

  async unAssignAllMetadataFromExercise({exerciseId}, knex) {
    return await knex('exercises_metadata')
      .delete()
      .where({exercise_id: exerciseId})
      .returning('metadata_id');
  }
}

module.exports = new MetadataService();
