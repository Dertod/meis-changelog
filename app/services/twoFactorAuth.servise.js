'use strict';
const knex = require('../../config/lib/knex').db;
const speakeasy = require('speakeasy');

const confirmToken = (secret, token) => speakeasy.totp.verify({
  secret: secret,
  encoding: 'base32',
  token: token
});

class TFAServise {

  async createSecretToken(userId) {
    const secretToken = speakeasy.generateSecret({length: 20});
    return secretToken
  }

  async confirmSecretToken(body, userId) {

    const [secretToken] = await knex('two_factor_auth_tokens').select('token')
      .where('other_id', userId);
    if (secretToken) {
      const verified = confirmToken(secretToken.token, body.user_token);
      if (verified) {
        return {
          activated: true,
          confirm: true,
          step: 3,
          showModal:false
        }
      } else {
        return {
          data: {message: 'Sorry, but your current key is not correct'},
          code: 400
        }
      }
    }
    if (!secretToken && body.user_secret_token) {
      const verified = confirmToken(body.user_secret_token, body.user_token);
      if (verified) {
        await knex('two_factor_auth_tokens')
          .insert({
            token: body.user_secret_token,
            other_id: userId,
          });
        return {
          activated: true,
          confirm: true,
          step: 3,
          showModal:false
        }
      } else {
        return {
          data: {message: 'Sorry, but your current key is not correct'},
          code: 400
        }

      }

    }
    return {
      data: {message: 'Validation failed'},
      code: 400
    }
  }


  async deleteSecretToken(body, userId) {
    const [secretToken] = await knex('two_factor_auth_tokens').select('token')
      .where('other_id', userId);
    const verified = confirmToken(secretToken.token, body.user_token);
    if (verified) {
      await knex('two_factor_auth_tokens')
        .delete().where('other_id', userId);
      return {
        activated: false,
        confirm: false,
        step: 1,
        showModal:false
      }
    }
    return {
      data: {message: 'Sorry, but your current key is not correct'},
      code: 400
    }
  }
}

module.exports = new TFAServise();
