'use strict';

const seneca = require('meisterfit-service-core/config/lib/seneca');

class EmailService {
  constructor() {
    this.seneca = {};

    seneca.init().then((seneca) => {
      this.seneca = seneca;
    });
  }

  async inviteNewAdmin({name, email, link}) {
    this.seneca.act({
      role: 'queue', cmd: 'enqueue', msg: {
        task: `${process.env.NODE_ENV}-email-invite-new-admin`, param: {
          name, email, link
        }
      }
    });
  }

  async inviteNewAthlete({email, link}) {
    this.seneca.act({
      role: 'queue', cmd: 'enqueue', msg: {
        task: `${process.env.NODE_ENV}-email-invite-new-athlete`, param: {
          email, link
        }
      }
    });
  }

  async sendConfirmationOfEmail({name, email, link, language}) {
    this.seneca.act({
      role: 'queue', cmd: 'enqueue', msg: {
        task: `${process.env.NODE_ENV}-email-confirm-email`, param: {
          name, email, link, language
        }
      }
    })
  }

  async sendDeclineEmailChange({name, email, newEmail, defaultEmail, link, language}) {
    this.seneca.act({
      role: 'queue', cmd: 'enqueue', msg: {
        task: `${process.env.NODE_ENV}-email-decline-email-change`, param: {
          name, email, newEmail, defaultEmail, link, language
        }
      }
    })
  }

  async recoveryPassword({name, email, link, language}) {
    this.seneca.act({
      role: 'queue', cmd: 'enqueue', msg: {
        task: `${process.env.NODE_ENV}-email-recovery-password`, param: {
          name, email, link, language
        }
      }
    });
  }

  async recoveryPasswordMobile({name, email, code, language}) {
    this.seneca.act({
      role: 'queue', cmd: 'enqueue', msg: {
        task: `${process.env.NODE_ENV}-email-recovery-password-mobile`, param: {
          name, email, code, language
        }
      }
    });
  }

}

module.exports = new EmailService();
