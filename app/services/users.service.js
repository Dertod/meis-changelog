'use strict';
const knex = require('../../config/lib/knex').db;
const FB = require('fb');

  class UsersService {
  async inviteNewUser({email, name}, userRole) {
    return await knex.transaction(async (trx) => {
      const ids = await trx
        .insert({
          email: email,
          name: name,
          is_email_confirmed: userRole === 1,
          user_role: userRole
        }, 'id')
        .into('users');

      let token = await trx
        .returning('token')
        .insert({
          other_id: ids[0],
          token: knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`),
          email_token_type_id: 1
        })
        .into('emails_tokens');

      return {user_id: ids[0], token: token[0]}
    });
  }
}

module.exports = new UsersService();
