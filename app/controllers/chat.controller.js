'use strict';

const Response = require('../../config/lib/response');
const knex = require('../../config/lib/knex').db;
const messageAmount = 10;

class ChatController extends Response {

  async getLastDirectMessages({params, query}) {
    const {user1, user2} = query;
    const count = 10, offset = 0;
    return await this.getDirectMessages({user1, user2, count, offset});
  }

  async getDirectMessages({user1, user2, count, offset}) {
    return await knex('messages as m')
      .select('m.message as message', 'm.id', 'm.message_type_id as type',
        'pr.name as program_name', 'mesad.messages_additions_types_id as id_type',
        'sender.name as fromName', 'sender.id as fromId',
        'receiver.name as to', 'receiver.id as toId', 'm.created_at as created_at',
        'us_sender.profile_image_url as sender_avatar')
      .where('m.from_user_id', user1).andWhere('m.to_id', user2)
      .orWhere('m.from_user_id', user2).andWhere('m.to_id', user1)
      .orderBy('m.created_at', 'desc')
      .limit(count)
      .offset(offset)
      .leftJoin(knex.raw(`(select * from messages_additions where messages_additions.messages_additions_types_id = 1)
                          as mesad on mesad.message_id = m.id`))
      .leftJoin(knex.raw(`(select * from programs) as pr on pr.id = mesad.other_id`))
      .leftJoin('users as sender', 'sender.id', 'm.from_user_id')
      .leftJoin('users_settings as us_sender', 'us_sender.user_id', 'm.from_user_id')
      .leftJoin('users as receiver', 'receiver.id', 'm.to_id')
      .then((rows) => rows.reverse());
  }

  async getUserCoachMessages({params, query}) {
    const {athleteId, companyId} = params;
    const [coach] = await knex('companies_admins as ca')
      .select('ca.admin_id as id')
      .where({company_id: companyId});

    const user1 = athleteId;
    const user2 = coach.id;

    const count = messageAmount;
    const offset = query.offset;
    const messages = await this.getDirectMessages({
      user1,
      user2,
      offset,
      count
    });
    return {
      athleteId,
      messages
    }
  }

}

module.exports = new ChatController();
