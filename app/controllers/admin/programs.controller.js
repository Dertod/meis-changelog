'use strict';

/**
 * Module dependencies.
 */
const Response = require('../../../config/lib/response');
const knex = require('../../../config/lib/knex').db;
const emailService = require('../../services/email.service');
const usersService = require('../../services/users.service');
const config = require('../../../config/config');
const _ = require('lodash');

class ProgramsController extends Response {

  async createProgram({body, user, params}) {
    let programId;

    [programId] = await knex('draft_programs').insert({
      name: body.title,
      description: body.description,
      creator_id: user.id,
      company_owner_id: params.companyId
    }, 'id');

    //await this.saveExercisesSettings(body.exercises, programId);

    return {message: 'Program successfully saved', id: programId}
  }

  async publishProgram({body, params}) {
    return await knex('programs')
      .update({description: body.description, price: body.price, is_published: true, category_id: body.category_id})
      .where({id: params.programId})
      .returning('id');
  }

  async unpublishProgram({params}) {
    return await knex('programs').update({is_published: false}).where({id: params.programId}).returning('id');
  }

  async getAllPrograms({params}) {
    let programs = await knex('programs as p').select('p.id', 'p.name as title', 'p.description', 'p.created_at', 'p.updated_at',
      'p.left_key', 'p.right_key', 'p.level', 'p.main_program_id', 'p.parent_program_id as parent_id', 'p.is_variation',
      'u.id as creator_id', 'u.name as creator_name', 'c.name as company_name', 'c.id as company_owner_id',
      knex.raw(`(select max(week_number) from sets
                   left join exercises_settings on exercises_settings.id = sets.exercise_setting_id
                   left join programs_exercises_days on programs_exercises_days.exercise_setting_id = exercises_settings.id
                   where programs_exercises_days.program_id = p.id) as duration`),
      knex.raw(`(select count(*) from programs_athletes as pa where pa.program_id = p.id) as athletes_amount`),
      knex.raw(`(select count(*) from programs_exercises_days as ped where ped.program_id = p.id) as exercises_amount`),
      knex.raw(`(select count(*) from programs_exercises_days as ped_a
       where ped_a.exercise_setting_id in 
       (select exercise_setting_id from programs_exercises_days where programs_exercises_days.program_id = p.parent_program_id)
       and ped_a.program_id = p.id) as equal_exercises`))
      .where(function () {
        this.whereNot('program_status_id', 3).orWhereNull('program_status_id')
      })
      .andWhere({company_owner_id: params.companyId})
      .andWhereRaw('p.id not in (select id from draft_programs)')
      //.leftJoin('programs_exercises_days as ped', 'p.id', )
      .leftJoin('users as u', 'u.id', 'p.creator_id')
      .leftJoin('companies as c', 'c.id', 'p.company_owner_id');

    let draftPrograms = await knex('draft_programs as p').select('p.id', 'p.name as title', 'p.description', 'p.created_at', 'p.updated_at',
      'p.left_key', 'p.right_key', 'p.level', 'p.main_program_id', 'p.parent_program_id as parent_id',
      'u.id as creator_id', 'u.name as creator_name', 'c.name as company_name', 'c.id as company_owner_id', knex.raw('true as is_draft'),
      knex.raw(`(select max(week_number) from draft_sets
                   left join draft_exercises_settings on draft_exercises_settings.id = draft_sets.exercise_setting_id
                   left join draft_programs_exercises_days on draft_programs_exercises_days.exercise_setting_id = draft_exercises_settings.id
                   where draft_programs_exercises_days.program_id = p.id) as duration`),
      knex.raw(`(select count(*) from programs_athletes as pa where pa.program_id = p.id) as athletes_amount`),
      knex.raw(`(select exists(select * from programs where programs.id = p.id)) as exists`))
      .where(function () {
        this.whereNot('program_status_id', 3).orWhereNull('program_status_id')
      })
      .andWhere({company_owner_id: params.companyId})
      .leftJoin('users as u', 'u.id', 'p.creator_id')
      .leftJoin('companies as c', 'c.id', 'p.company_owner_id');

    return _.concat(programs, draftPrograms);
  }

  async getProgramById({params}) {
    const [program] = await knex('draft_programs')
      .select('id')
      .where({id: params.programId});
    let programId = program ? program.id : params.programId;
    let tablePrefix = program ? 'draft_' : '';

    return await knex.transaction(async trx => {

      let [settings] = await trx(tablePrefix + 'programs as p')
        .select('p.id', 'p.name as title', 'p.description', 'p.created_at', 'p.updated_at',
          'p.left_key', 'p.right_key', 'p.level', 'p.main_program_id',
          'u.id as creator_id', 'u.name as creator_name', knex.raw(`(select exists(select * from programs where programs.id = p.id)) as exist`))
        .leftJoin('users as u', 'u.id', 'p.creator_id')
        .where(function () {
          this.whereNot('program_status_id', 3).orWhereNull('program_status_id')
        })
        .andWhere('p.id', programId);

      settings.isDraft = !!program;

      if (settings && settings.id) {
        let exercisesSettings = await trx(tablePrefix + 'exercises_settings as es')
          .select('es.id', 'es.duration_type_id', 'es.rest_time', 'es.characters_in_sets', 'es.exercise_id', 'es.day_number',
            'es.is_warmup', 'es.tempo', 'es.order', 'es.resistance_type_id', 'es.resistance', 'es.description as description',
            'e.name', 'e.description as exercise_description', 'e.video_link', 'e.logo_image_url', 'mtu.title', 'ert.resistance_type',
            'es.original_exercise_setting_id')
          .leftJoin('exercises as e', 'e.id', 'es.exercise_id')
          .leftJoin('measurements_units_types as mtu', 'es.duration_type_id', 'mtu.id')
          .leftJoin('exercise_resistance_types as ert', 'es.resistance_type_id', 'ert.id')
          .leftJoin(`${tablePrefix}programs_exercises_days as ped`, 'ped.exercise_setting_id', 'es.id')
          .where('ped.program_id', programId)
          .orderBy('es.order', 'asc');

        let sets = await trx(tablePrefix + 'sets as s').select('s.id', 's.sets', 's.duration', 's.weight', 's.exercise_setting_id', 's.week_number')
          .leftJoin(tablePrefix + 'exercises_settings as es', 's.exercise_setting_id', 'es.id')
          .leftJoin(tablePrefix + 'programs_exercises_days as ped', 's.exercise_setting_id', 'ped.exercise_setting_id')
          .where('ped.program_id', programId)
          .orderBy('s.week_number', 'asc');

        exercisesSettings.forEach(exercise => {
          exercise.sets = sets.filter(set => set.exercise_setting_id === exercise.id);
        });

        return {...settings, exercises: exercisesSettings};
      } else {
        return {
          data: {message: "This program doesn't exist"},
          code: 400
        };
      }
    })
  }

  async editProgramById({body, params}) {

    if (!params.mainExist) {
      let [draft] = await knex('draft_programs')
        .select('*')
        .where({id: params.programId});

      await knex('programs').insert({...draft});
    }

    let [variation] = await knex('programs').select('is_variation').where({id: params.programId});

    if (variation.is_variation) {
      return await this.editVariationProgram({body, params});
    }

    await knex.transaction(async trx => {
      await trx('programs').update({
        name: body.title,
        description: body.description
      })
        .where(function () {
          this.whereNot('program_status_id', 3).orWhereNull('program_status_id')
        })
        .andWhere({id: params.programId});

      let exerciseSettings = await trx('draft_exercises_settings')
        .delete()
        .whereIn('id', knex('draft_programs_exercises_days')
          .select('exercise_setting_id')
          .where({program_id: params.programId})).returning(['id', 'original_exercise_setting_id']);

      let settingIds = [];
      exerciseSettings.forEach(exercise => {
        settingIds.push(exercise.id);
        settingIds.push(exercise.original_exercise_setting_id);
      });

      await trx('draft_sets')
        .delete()
        .whereIn('exercise_setting_id', settingIds).returning('id');

      await trx('exercises_settings')
        .delete()
        .whereIn('id', knex('programs_exercises_days')
          .select('exercise_setting_id')
          .where({program_id: params.programId}));

      await trx('draft_programs').delete().where({id: params.programId});
    });

    await this.saveExercisesSettings(body.exercises, params.programId, '');


    return {message: 'Program successfully saved', id: params.programId}
  }

  async editVariationProgram({body, params}) {
    //Select all old exercises for checking with new ones
    let oldExercises = await knex('exercises_settings as es')
      .whereIn('es.id', knex('programs_exercises_days').select('exercise_setting_id').where({program_id: params.programId}))
      .select('es.id', 'es.duration_type_id', 'es.rest_time', 'es.characters_in_sets', 'es.day_number', 'es.is_warmup',
        'es.tempo', 'es.order', 'es.resistance_type_id', 'es.resistance', 'es.description', 'es.exercise_id', 's.id as set_id',
        's.sets', 's.duration', 's.weight', 's.exercise_setting_id', 's.week_number')
      .leftJoin('sets as s', 's.exercise_setting_id', 'es.id');

    let [program] = await knex.transaction(async trx => {
      let exerciseSettings = await trx('draft_exercises_settings')
        .delete()
        .whereIn('id', knex('draft_programs_exercises_days')
          .select('exercise_setting_id')
          .where({program_id: params.programId})).returning(['id', 'original_exercise_setting_id']);

      let settingIds = [];
      exerciseSettings.forEach(exercise => {
        settingIds.push(exercise.id);
        settingIds.push(exercise.original_exercise_setting_id);
      });

      await trx('draft_sets')
        .delete()
        .whereIn('exercise_setting_id', settingIds).returning('id');

      await trx('draft_programs').delete().where({id: params.programId});

      return await trx('programs').update({
        name: body.title,
        description: body.description
      }, ['id', 'right_key', 'left_key', 'level', 'main_program_id'])
        .where(function () {
          this.whereNot('program_status_id', 3).orWhereNull('program_status_id')
        })
        .andWhere({id: params.programId});
    });

    //Select all programs that located lower by the tree and should be changed
    let lowerTreeIds = await knex('programs').select('id')
      .where(function () {
        this.where('main_program_id', program.main_program_id || program.id).orWhere('id', program.id)
      })
      .andWhere('left_key', '>=', program.left_key).andWhere('right_key', '<=', program.right_key);

    body.exercises.forEach(async (exercise, i) => {
      exercise.day_number = exercise.is_warmup ? null : exercise.day_number;
      if (!exercise.id) {
        //Add new exercises
        await this.saveExercisesSettings([exercise], params.programId, '')
      } else {
        let isExerciseRemoved = false;
        exercise.sets.forEach(async (newSet) => {
          //Empty iteration for already removed exercises
          if (isExerciseRemoved) {
            return true;
          }

          //Search old sets for checking sets equality
          let oldSet = _.find(oldExercises, old => old.set_id === newSet.id);

          //Checking sets equality
          if (!this.areSetsEqual(exercise, newSet, oldSet)) {
            //Remove old exercises from array to find deleted and set flag on true
            _.remove(oldExercises, old => old.id === exercise.id);
            isExerciseRemoved = true;

            //Delete all connection from the programs located below by the tree
            await knex('programs_exercises_days').delete().whereIn('program_id', lowerTreeIds.map(id => id.id))
              .andWhere('exercise_setting_id', exercise.id);

            //Create new unchained exercises in the tree for lower programs
            let [exerciseSettingsId] = await knex('exercises_settings').insert({
              ..._.pick(exercise,
                [
                  'duration_type_id', 'rest_time', 'characters_in_sets', 'exercise_id', 'day_number', 'tempo',
                  'order', 'resistance_type_id', 'resistance', 'description', 'is_warmup'
                ])
            }).returning('id');

            await knex('sets').insert(exercise.sets.map(set => {
              return {
                ..._.pick(set, ['sets', 'duration', 'week_number', 'weight']),
                exercise_setting_id: exerciseSettingsId
              }
            }));

            //Connect new exercises to lower program's tree
            await knex('programs_exercises_days').insert(lowerTreeIds.map(program => {
              return {program_id: program.id, exercise_setting_id: exerciseSettingsId}
            }));
          } else {
            //Delete equal sets from array
            _.remove(oldExercises, old => old.set_id === newSet.id);
          }

        });
      }
    });
    //Delete functionality for exercises
    if (oldExercises && oldExercises.length) {
      (_.uniqBy(oldExercises, 'id')).map(async old => {
        await knex('programs_exercises_days')
          .whereIn('program_id', lowerTreeIds.map(program => program.id))
          .andWhere({exercise_setting_id: old.id})
          .delete();

        let leftPrograms = await knex('programs_exercises_days').select('*').where({exercise_setting_id: old.id});

        //Delete exercises that not connected to any program
        if (!leftPrograms || !leftPrograms.length) {
          await knex('exercises_settings').where('id', old.id).delete();
        }
      })
    }

    return {message: 'Program successfully saved', id: params.programId}
  }

  async createDraft(params) {
    return await knex.transaction(async trx => {
      let [program] = await trx('programs')
        .select('*')
        .where({id: params.programId});
      let exercisesSettings = await trx('exercises_settings')
        .select('*')
        .whereIn('id', knex('programs_exercises_days').select('exercise_setting_id').where({program_id: program.id}));
      let sets = await trx('sets')
        .select('*')
        .whereIn('exercise_setting_id', _.map(exercisesSettings, 'id'));

      await trx.insert({...program}).into('draft_programs');

      let draftedExercises = await trx.insert(exercisesSettings.map(exercise => {
        return {...exercise, original_exercise_setting_id: exercise.id, id: undefined}
      })).into('draft_exercises_settings').returning(['id', 'original_exercise_setting_id']);

      await trx('draft_programs_exercises_days').insert(draftedExercises.map(exercise => {
        return {exercise_setting_id: exercise.id, program_id: params.programId}
      }));

      await trx.insert(sets.map(set => {
        return {...set}
      })).into('draft_sets');
    });
  }


  async autosaveProgramById({body, params}) {
    if (!params.draftExist) {
      await this.createDraft(params);
    }

    await knex.transaction(async trx => {
      await trx('draft_programs').update({
        name: body.title,
        description: body.description
      })
        .where(function () {
          this.whereNot('program_status_id', 3).orWhereNull('program_status_id')
        })
        .andWhere({id: params.programId});

      let exerciseSettings = await trx('draft_exercises_settings')
        .delete()
        .whereIn('id', knex('draft_programs_exercises_days')
          .select('exercise_setting_id')
          .where({program_id: params.programId})).returning(['id', 'original_exercise_setting_id']);

      let settingIds = [];
      exerciseSettings.forEach(exercise => {
        settingIds.push(exercise.id);
        settingIds.push(exercise.original_exercise_setting_id);
      });

      await trx('draft_sets')
        .delete()
        .whereIn('exercise_setting_id', settingIds).returning('id');
    });

    await this.saveExercisesSettings(body.exercises, params.programId, 'draft_');
    return {message: 'Program successfully saved', id: params.programId}
  }

  async saveExercisesSettings(exercisesArray, programId, tablePrefix) {
    exercisesArray.forEach(async (exercise) => {
      exercise.day_number = exercise.is_warmup ? null : exercise.day_number;
      if (tablePrefix === 'draft_') {
        exercise.original_exercise_setting_id = exercise.original_exercise_setting_id ? exercise.original_exercise_setting_id : exercise.id;
      } else {
        exercise.original_exercise_setting_id = null;
      }

      let [exerciseSettings] = await knex(tablePrefix + 'exercises_settings').insert({
        ..._.pick(exercise,
          [
            'duration_type_id', 'rest_time', 'characters_in_sets', 'exercise_id', 'day_number', 'tempo',
            'order', 'resistance_type_id', 'resistance', 'description', 'is_warmup', 'original_exercise_setting_id'
          ])
      }).returning(['id', 'original_exercise_setting_id']);

      await knex(tablePrefix + 'programs_exercises_days').insert({
        exercise_setting_id: exerciseSettings.id,
        program_id: programId
      });

      await knex(tablePrefix + 'sets').insert(exercise.sets.map(set => {
        return {
          ...set,
          exercise_setting_id: tablePrefix === 'draft_' && exerciseSettings.original_exercise_setting_id ?
            exerciseSettings.original_exercise_setting_id :
            exerciseSettings.id
        }
      }))
    });
  }


  areSetsEqual(newExercise, newSet, oldSet) {
    return ((newExercise.exercise_id === oldSet.exercise_id) && (newExercise.characters_in_sets === newExercise.characters_in_sets)
    && (newExercise.tempo === oldSet.tempo) && (newExercise.description === oldSet.description) && (newExercise.duration_type_id === oldSet.duration_type_id)
    && (newExercise.resistance_type_id === oldSet.resistance_type_id) && (newExercise.day_number === oldSet.day_number)
    && (newExercise.is_warmup === oldSet.is_warmup) && (newExercise.rest_time === oldSet.rest_time)
    && (newSet.sets === oldSet.sets) && (newSet.duration === oldSet.duration) && (newSet.week_number === oldSet.week_number)
    && (newSet.weight === oldSet.weight));
  }

  async deleteProgramById({params, user}) {
    let [program] = await knex('programs').select('*').where({id: params.programId});

    if (program && program.length && !program.main_program_id && program.is_variation
      && (await knex('programs').select('*').where({main_program_id: program.id, program_status_id: null})).length) {
      return {
        data: {message: `Top level program cannot be deleted if there is at least 1 variation`},
        code: 403
      };
    }

    const athletes = await knex('programs_athletes as pa')
      .select('u.id', 'ca.id as ca_id')
      .where({program_id: params.programId})
      .leftJoin('companies_athletes as ca', 'pa.company_athlete_id', 'ca.id')
      .leftJoin('users as u', 'ca.athlete_id', 'u.id');

    const messages_logs = [];
    const companyAthleteIds = athletes.map(athlete => {
      messages_logs.push({
        message_type_id: 3,
        from_user_id: user.id,
        to_id: athlete.id,
        message: `You have been removed from program: ${params.programId}`,
        company_id: params.companyId
      });
      return athlete.ca_id;
    });

    await knex.transaction(async trx => {
      await trx('messages').insert(messages_logs);
      await trx('programs_athletes').whereIn('company_athlete_id', companyAthleteIds).andWhere({
        program_id: params.programId
      }).del();
      let [deletedProgram] = await trx('programs')
        .where(function () {
          this.whereNot('program_status_id', 3).orWhereNull('program_status_id')
        })
        .andWhere({id: params.programId})
        .update({program_status_id: 3})
        .returning(['id', 'right_key', 'left_key', 'level', 'main_program_id', 'parent_program_id']);

      if (deletedProgram && deletedProgram.length && deletedProgram.main_program_id) {
        await knex('programs')
          .update({parent_program_id: deletedProgram.parent_program_id})
          .where({parent_program_id: deletedProgram.id});

        await knex.raw(`UPDATE programs SET left_key = (CASE WHEN left_key > ${deletedProgram.left_key} 
        THEN left_key - (${deletedProgram.right_key} - ${deletedProgram.left_key} + 1) ELSE left_key END),
       right_key = (right_key - (${deletedProgram.right_key} - ${deletedProgram.left_key} + 1)) 
       WHERE right_key > ${deletedProgram.right_key} 
       AND (main_program_id = ${deletedProgram.main_program_id} OR id = ${deletedProgram.main_program_id})
       AND program_status_id IS NULL`);
      }

      await trx('draft_exercises_settings as es')
        .delete()
        .whereIn('es.id', knex('draft_programs_exercises_days').select('exercise_setting_id').where({program_id: params.programId}));

      await trx('draft_programs')
        .where({id: params.programId})
        .del();
    });

    return {
      data: {message: `Program was successfully deleted`},
      code: 200
    };
  }

  async assignProgramToAthlete({email, programId, user, companyId}) {
    let result;

    result = await usersService.inviteNewUser({email: email, name: email.split('@')[0]}, 3);

    emailService.inviteNewAthlete({
      email: email,
      link: `${config.adminUrl}/athlete/create?token=${result.token}`
    });


    await knex.transaction(async trx => {

      let ids = await trx('companies_athletes')
        .insert({
          company_id: companyId,
          athlete_id: result.user_id,
          company_athlete_relationship_type_id: 3,
          creator_id: user.id
        })
        .returning('id');

      const [messageId] = await trx('messages').insert({
        message_type_id: 3,
        from_user_id: user.id,
        to_id: result.user_id,
        message: `You have been added to program: `,
        company_id: companyId
      }).returning('id');
      await trx('messages_additions').insert({
        message_id: messageId,
        other_id: programId,
        messages_additions_types_id: 1
      });

      return await trx('programs_athletes').insert({company_athlete_id: ids[0], program_id: programId})
        .returning(['company_athlete_id', 'program_id']);
    });

    return result.user_id;
  }

  async assignProgramToAthletes({body, params, user, program}) {
    const programsAthletesEmails = (await knex('programs_athletes as pa').select('u.id', 'u.email')
      .where({program_id: program.id})
      .andWhere('ca.company_id', params.companyId)
      .leftJoin('companies_athletes as ca', 'ca.id', 'pa.company_athlete_id')
      .leftJoin('users as u', 'u.id', 'ca.athlete_id')).map(athlete => athlete.email);
    const emailsWithoutAssigned = body.emails.filter(email => !programsAthletesEmails.includes(email));
    let existingAthletes = await knex('users').select('id', 'email').whereIn('email', emailsWithoutAssigned).andWhere('user_role', 3);
    let nonExistingAthletes = _.difference(emailsWithoutAssigned, existingAthletes.map((athlete => athlete.email)));
    let nonExistingAthletesIds = [];
    let newAthletes = [];
    if (nonExistingAthletes && nonExistingAthletes.length) {
      nonExistingAthletesIds = await Promise.all(nonExistingAthletes.map(async (email) => {
        return await this.assignProgramToAthlete({email, programId: program.id, companyId: params.companyId, user});
      }));

      newAthletes = await knex('users as u').select('u.id', 'u.email', 'u.name', 'u.last_seen', 'us.profile_image_url',
        knex.raw(`json_agg(COALESCE(json_build_object(
          'name', p.name,
          'id', p.id))
           )FILTER (WHERE  p.id IS NOT NULL)
            as programs`))
        .whereIn('u.id', nonExistingAthletesIds)
        .leftJoin('users_settings as us', 'u.id', 'us.user_id')
        .leftJoin('companies_athletes as ca', 'ca.athlete_id', 'u.id')
        .where('ca.company_id', params.companyId)
        .leftJoin('programs_athletes as pa', 'pa.company_athlete_id', 'ca.id')
        .leftJoin('programs as p', 'pa.program_id', 'p.id')
        .groupBy('u.id', 'u.email', 'u.name', 'u.last_seen', 'us.profile_image_url')
    }

    if (existingAthletes && existingAthletes.length) {
      await knex.transaction(async trx => {
        let messagesLogs = [];
        let messagesAdditionLogs = [];
        let companiesAthletes = existingAthletes.map(athlete => {
          messagesLogs.push({
            message_type_id: 3,
            from_user_id: user.id,
            to_id: athlete.id,
            message: `You have been added to program: `,
            company_id: params.companyId
          });
          messagesAdditionLogs.push({
            other_id: program.id,
            messages_additions_types_id: 1
          });
          return {
            company_id: params.companyId,
            athlete_id: athlete.id,
            company_athlete_relationship_type_id: 3,
            creator_id: user.id
          }
        });

        const messagesIds = await trx('messages').insert(messagesLogs).returning('id');
        const messageAdditions = messagesAdditionLogs.map((item, index) => ({...item, message_id: messagesIds[index]}));
        await trx('messages_additions').insert(messageAdditions);

        let query = trx('companies_athletes').insert(companiesAthletes).toString();
        query += ' ON CONFLICT (company_id, athlete_id) DO UPDATE set company_athlete_relationship_type_id = EXCLUDED.company_athlete_relationship_type_id, creator_id = EXCLUDED.creator_id returning id;';
        let ids = await trx.raw(query);

        let queryProgramsAthletes = trx('programs_athletes').insert(ids.rows.map((id) => {
          return {company_athlete_id: id.id, program_id: params.programId}
        })).toString();
        queryProgramsAthletes += ' ON CONFLICT (company_athlete_id, program_id) DO UPDATE SET company_athlete_id=EXCLUDED.company_athlete_id;';
        await trx.raw(queryProgramsAthletes);
      });
    }
    const athletesIds = [...nonExistingAthletesIds, ...existingAthletes.map(athlete => athlete.id)];
    return {
      athletesIds,
      program: {id: program.id, name: program.name},
      newAthletes: newAthletes
    };
  }

  async detachAthletesFromProgram({params, body, user, program}) {
    let existingAthletes = await knex('users').select('id', 'email').whereIn('email', body.emails).andWhere('user_role', 3);
    let existingAthletesIds = [];

    if (existingAthletes && existingAthletes.length) {
      await knex.transaction(async trx => {
        let messagesLogs = [];
        let messagesAdditionLogs = [];
        existingAthletesIds = existingAthletes.map(existingAthlete => {
          messagesLogs.push({
            message_type_id: 3,
            from_user_id: user.id,
            to_id: existingAthlete.id,
            message: `You have been removed from program: `,
            company_id: params.companyId
          });
          messagesAdditionLogs.push({
            other_id: program.id,
            messages_additions_types_id: 1
          });
          return existingAthlete.id;
        });

        const messagesIds = await trx('messages').insert(messagesLogs).returning('id');
        const messageAdditions = messagesAdditionLogs.map((item, index) => ({...item, message_id: messagesIds[index]}));
        await trx('messages_additions').insert(messageAdditions);

        let companiesAthletesIds = await trx('companies_athletes').select('id')
          .whereIn('athlete_id', existingAthletesIds)
          .where({
            company_id: params.companyId,
            company_athlete_relationship_type_id: 3,
          });

        try {
          return await trx('programs_athletes')
            .whereIn('company_athlete_id', companiesAthletesIds.map(item => item.id))
            .where({
              program_id: params.programId
            }).del();
        } catch (e) {
          throw new Error(e);
        }
      });
    }
    return {
      program: {name: program.name, id: program.id},
      athletesIds: existingAthletesIds
    };
  }

  async unAssignAllFromProgram({params}) {
    let companyAthletesId = await knex('programs_athletes').delete().where({program_id: params.programId}).returning('company_athlete_id');

    return await knex('companies_athletes').delete().whereIn('id', companyAthletesId).returning(['company_id', 'athlete_id']);
  }

  async getProgramAthletes({params, query}) {
    if (query.belong === 'true') {
      return await knex('programs_athletes as pa').select('u.id', 'u.email', 'u.name', 'u_s.profile_image_url')
        .where({program_id: params.programId})
        .andWhere('ca.company_id', params.companyId)
        .leftJoin('companies_athletes as ca', 'ca.id', 'pa.company_athlete_id')
        .leftJoin('users as u', 'u.id', 'ca.athlete_id')
        .leftJoin('users_settings as u_s', 'u_s.user_id', 'u.id');
    }

    const athletesBelongsToProgram = await knex('programs_athletes as pa').select('ca.athlete_id')
      .where({program_id: params.programId})
      .join('companies_athletes as ca', 'ca.id', 'pa.company_athlete_id');

    return await knex('companies_athletes as ca').select('u.id', 'u.email', 'u.name', 'u_s.profile_image_url')
      .whereNotIn('ca.athlete_id', athletesBelongsToProgram.map(athlete => athlete.athlete_id))
      .andWhere('ca.company_id', params.companyId)
      .leftJoin('users as u', 'u.id', 'ca.athlete_id')
      .leftJoin('users_settings as u_s', 'u_s.user_id', 'u.id');
  }

  async cloneProgram({params, program, user}) {
    const [programClone] = await knex.insert({
      ..._.pick(program, ['name', 'is_active', 'category_id', 'description', 'price', 'program_status_id']),
      name: `${program.name}-copy`,
      parent_program_id: program.id,
      creator_id: user.id,
      company_owner_id: params.companyId
    }).into('programs')
      .returning(['id', 'name as title', 'description', 'created_at', 'parent_program_id as parent_id']);

    if (program.id) {
      let exercisesSettings = await knex('exercises_settings as es')
        .select('es.id', 'es.duration_type_id', 'es.rest_time', 'es.characters_in_sets', 'es.exercise_id', 'es.day_number',
          'es.is_warmup', 'es.tempo', 'es.order', 'es.resistance_type_id', 'es.resistance', 'es.description as description',
          'e.name', 'e.description as exercise_description', 'e.video_link', 'e.logo_image_url', 'mtu.title', 'ert.resistance_type')
        .leftJoin('exercises as e', 'e.id', 'es.exercise_id')
        .leftJoin('measurements_units_types as mtu', 'es.duration_type_id', 'mtu.id')
        .leftJoin('exercise_resistance_types as ert', 'es.resistance_type_id', 'ert.id')
        .leftJoin('programs_exercises_days as ped', 'es.id', 'ped.exercise_setting_id')
        .where('ped.program_id', program.id)
        .orderBy('es.order', 'asc');

      let sets = await knex('sets as s').select('s.sets', 's.duration', 's.weight', 's.exercise_setting_id', 's.week_number')
        .leftJoin('exercises_settings as es', 's.exercise_setting_id', 'es.id')
        .leftJoin('programs_exercises_days as ped', 's.exercise_setting_id', 'ped.exercise_setting_id')
        .where('ped.program_id', program.id)
        .orderBy('s.week_number', 'asc');

      exercisesSettings.forEach(exercise => {
        exercise.sets = sets.filter(set => set.exercise_setting_id === exercise.id);
      });

      await this.saveExercisesSettings(exercisesSettings, programClone.id, '');
    }
    return {...programClone, athletesAmount: 0};
  }

  async createVariation({user, params}) {
    let programVariation, parentProgram, exercisesId;

    [parentProgram] = await knex('programs').select('*').where({id: params.programId});
    exercisesId = await knex('programs_exercises_days').select('exercise_setting_id').where({program_id: params.programId});
    let mainProgramId = parentProgram.main_program_id || parentProgram.id;

    await knex.raw(`UPDATE programs SET right_key = right_key + 2, left_key = (CASE WHEN left_key > ${parentProgram.right_key} THEN left_key + 2 ELSE left_key END)
       WHERE right_key >= ${parentProgram.right_key} AND (main_program_id = ${mainProgramId} OR id = ${mainProgramId})`);

    await knex('programs').update({is_variation: true}).where({id: params.programId});

    [programVariation] = await knex('programs').insert({
      name: `${parentProgram.name}-variation`,
      creator_id: user.id,
      company_owner_id: params.companyId,
      left_key: parentProgram.right_key,
      right_key: parentProgram.right_key + 1,
      level: parentProgram.level + 1,
      parent_program_id: parentProgram.id,
      main_program_id: mainProgramId,
      is_variation: true
    }, ['id', 'name as title', 'description', 'created_at', 'parent_program_id as parent_id']);

    await knex('programs_exercises_days').insert(exercisesId.map(exercise => {
      return {program_id: programVariation.id, exercise_setting_id: exercise.exercise_setting_id}
    }));

    return {message: 'Program successfully saved', program: {...programVariation, athletesAmount: 0}}
  }

  async removeDraftProgram({params}) {
    await knex('draft_exercises_settings as es')
      .delete()
      .whereIn('es.id', knex('draft_programs_exercises_days').select('exercise_setting_id').where({program_id: params.programId}));

    return await knex('draft_programs')
      .where({id: params.programId})
      .returning('id')
      .del();
  }
}

module.exports = new ProgramsController();
