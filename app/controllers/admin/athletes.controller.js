const Response = require('../../../config/lib/response');
const knex = require('../../../config/lib/knex').db;
const _ = require('lodash');
const config = require('../../../config/config');
const emailService = require('../../services/email.service');
const usersService = require('../../services/users.service');
const metadataService = require('../../services/metadata.service');

/**
 * Methods for the athletes and their statistics
 */
class AthletesController extends Response {

  async createAthlete({body, user, params}) {

    let result = {};
    let [existAthlete] = await knex('users').where({email: body.email, user_role: 3}).select('id');

    if (existAthlete) {
      result.user_id = existAthlete.id;
    }
    else {
      result = await usersService.inviteNewUser({email: body.email, name: body.email.split('@')[0]}, 3);

      emailService.inviteNewAthlete({
        email: body.email,
        link: `${config.adminUrl}/athlete/create?token=${result.token}`
      });
    }

    await knex.transaction(async trx => {
      await trx('companies_athletes')
        .insert({
          company_id: params.companyId,
          athlete_id: result.user_id,
          company_athlete_relationship_type_id: 3,
          creator_id: user.id
        })
        .returning('id');

      if (body.metadata && body.metadata.length) {
        await metadataService.assignMetadataToAthlete({
          metadata: body.metadata,
          companyId: params.companyId,
          athleteId: result.user_id
        }, trx);
      }

      if (body.program) {
        const [program] = await knex('programs')
          .select('id')
          .where({id: body.program});

        await this.attachProgramToExistingAthlete(
          trx, user, result.user_id, {companyId: params.companyId, programId: program.id}
        );
      } else {
        await trx('messages').insert({
          message_type_id: 5,
          from_user_id: user.id,
          to_id: result.user_id,
          message: `You have been added to MeisterFit`,
          company_id: params.companyId
        });
      }
    });

    return {
      id: result.user_id,
      email: body.email,
    }
  }

  async getMyAthletes({params}) {
    return await knex.select('id', 'email', 'name', 'last_seen', 'is_online', 'title',
      'user_role', 'age', 'profile_image_url', 'is_email_confirmed',
      knex.raw(`COALESCE(json_agg(json_build_object('id', metadata_id, 'key', metadata_key, 'value', metadata_value))
       FILTER (WHERE metadata_id IS NOT NULL), '[]') as metadata`),
      knex.raw(`json_agg(COALESCE(json_build_object(
      'name', program_name,
      'id', program_id))
       )FILTER (WHERE program_id IS NOT NULL)
        as programs`))
      .from(function () {
        this.select('u.id', 'u.email', 'u.name', 'u.last_seen', 'u.is_online', 'u.is_email_confirmed', 'cart.title',
          'u.user_role', 'u_s.age', 'u_s.profile_image_url',
          'p.id as program_id', 'p.name as program_name', 'm.id as metadata_id',
          'm.key as metadata_key', 'm.value as metadata_value')
          .from('companies_athletes as ca')
          .where('ca.company_id', params.companyId)
          .leftJoin('users as u', 'u.id', 'ca.athlete_id')
          .leftJoin('companies_athletes_relationship_types as cart', 'cart.id', 'ca.company_athlete_relationship_type_id')
          .leftJoin('users_settings as u_s', 'u.id', 'u_s.user_id')
          .leftJoin('programs_athletes as pa', 'pa.company_athlete_id', 'ca.id')
          .leftJoin('programs as p', 'pa.program_id', 'p.id')
          .leftJoin('athletes_metadata as am', 'am.athlete_id', 'ca.athlete_id')
          .leftJoin('metadata as m', function () {
            this.on('m.id', '=', 'am.metadata_id').andOn('m.company_id', '=', 'ca.company_id')
          })
          .groupBy('u.id', 'p.id', 'cart.title', 'u_s.id', 'm.id').as('without_programs')
      }).groupBy('id', 'title', 'email', 'name', 'user_role', 'age', 'profile_image_url', 'last_seen', 'is_online', 'is_email_confirmed');
  }

  async getAthletesForDashboard({params}) {
    return await knex('users as u').select(knex.raw(`DISTINCT ON (u.id) u.id`), 'u.name', 'us.profile_image_url', 'df.athlete_status_id',
      knex.raw(`COALESCE(json_agg(json_build_object('id', m.id,'key', m.key, 'value' , m.value)) FILTER (WHERE m.id IS NOT NULL), '[]') as metadata`),
      knex.raw(`((df.week_number + 1) * (df.day_number + 1)) as program_progress`), 'p.name as program_name',
      knex.raw(`((max(s.week_number) + 1) * (max(es.day_number) + 1)) as program_duration`), 'p.id as program_id',
      knex.raw(`(select exists(select * from programs where programs.id = p.id and (programs.program_status_id != 3 or programs.program_status_id IS NULL))) as program_exist`))
      .where('ca.company_id', params.companyId)
      .andWhere(knex.raw('df.created_at = (SELECT max(created_at) FROM days_feedback WHERE program_id = p.id and athlete_id = u.id)'))
      .andWhere('df.created_at', '>', knex.raw(`now() - '10 day'::interval`))
      .leftJoin('companies_athletes as ca', 'u.id', 'ca.athlete_id')
      .leftJoin('users_settings as us', 'u.id', 'us.user_id')
      .leftJoin('athletes_metadata as am', 'am.athlete_id', 'ca.athlete_id')
      .leftJoin('metadata as m', function () {
        this.on('m.id', '=', 'am.metadata_id').andOn('m.company_id', '=', 'ca.company_id')
      })
      .leftJoin('days_feedback as df', 'df.athlete_id', 'ca.athlete_id')
      .leftJoin('programs as p', 'df.program_id', 'p.id')
      .leftJoin('programs_exercises_days as ped', 'ped.program_id', 'p.id')
      .leftJoin('exercises_settings as es', 'es.id', 'ped.exercise_setting_id')
      .leftJoin('sets as s', 's.exercise_setting_id', 'es.id')
      .groupBy('u.id', 'us.id', 'ca.athlete_id', 'p.id', 'df.id', 'm.id', 'es.id', 's.id');
  }

  async getAthleteById({params}) {
    return (await knex('users as u').select('u.id', 'u.name', 'u.email', 'u.created_at', 'u.updated_at', 'us.profile_image_url',
      'us.weight', 'us.height', 'us.age', 'u.is_email_confirmed', 'p.id as program_id', 'p.name as program_name',
      knex.raw(`COALESCE(json_agg(json_build_object('id', m.id,'key', m.key, 'value' , m.value)) FILTER (WHERE m.id IS NOT NULL), '[]') as metadata`))
      .where({'u.id': params.athleteId, 'u.user_role': 3})
      .leftJoin('users_settings as us', 'us.user_id', 'u.id')
      .leftJoin('athletes_metadata as am', 'am.athlete_id', 'u.id')
      .leftJoin('metadata as m', 'm.id', 'am.metadata_id')
      .leftJoin('companies_athletes as ca', 'ca.athlete_id', 'u.id')
      .leftJoin('programs_athletes as pa', 'pa.company_athlete_id', 'ca.id')
      .leftJoin('programs as p', 'p.id', 'pa.program_id')
      .groupBy('u.id', 'us.id', 'p.id'))[0];
  }

  async editAthlete({body, params}) {
    let updateUserData = _.pick(body, ['email', 'name']);
    let [updatedUser] = await knex('users')
      .update({
        email: updateUserData.email,
        name: updateUserData.name
      }).where({id: params.athleteId}).returning(['id', 'email', 'name']);

    let updateSettingsData = _.pick(body, ['weight', 'height', 'age', 'profile_image_url']);
    if (_.keys(updateSettingsData).length > 0) {
      await knex('users_settings')
        .update(updateSettingsData)
        .where('user_id', params.athleteId);
    }

    if (body.metadata) {
      await metadataService.unAssignAllMetadataFromAthlete({athleteId: params.athleteId}, knex);

      if (body.metadata.length) {
        await metadataService.assignMetadataToAthlete({
          metadata: body.metadata,
          companyId: params.companyId,
          athleteId: params.athleteId
        }, knex);
      }
    }

    return updatedUser;
  }

  async removeAthlete({params, user}) {

    let messagesLogs = [];

    messagesLogs.push({
      message_type_id: 6,
      from_user_id: user.id,
      to_id: params.athleteId,
      message: `You have been removed from company: ${params.companyId}`,
      company_id: params.companyId
    });

    let [companyAthlete] = await knex('companies_athletes').select('id').where({
      athlete_id: params.athleteId,
      company_id: params.companyId
    });

    let delAthleteInfo = await knex.transaction(async trx => {
      let delAthleteFromPrograms = await trx('programs_athletes').delete().where('company_athlete_id', companyAthlete.id).returning('program_id');
      await trx('companies_athletes').delete().where({athlete_id: params.athleteId, company_id: params.companyId});
      let [messages] = await trx('messages').insert(messagesLogs).returning('id');
      return {
        programsId: delAthleteFromPrograms,
        messagesId: messages,
      }
    });
    return {
      athleteId: params.athleteId,
      athleteFromCompanyId: params.companyId,
      athleteFromProgramsId: delAthleteInfo.programsId,
      athleteMessagesId: delAthleteInfo.messagesId
    }
  }

  async attachProgramToAthlete({params, user, program}) {
    let [existingAthlete] = await knex('users').select('id', 'email').whereIn('id', params.athleteId);

    await knex.transaction(
      trx => this.attachProgramToExistingAthlete(trx, user, existingAthlete.id, params)
    );

    return {
      program: {name: program.name, id: program.id},
      athleteId: existingAthlete.id
    }
  }

  async attachProgramToExistingAthlete(trx, user, existingAthleteId, params) {
    let messagesLogs = [];
    let messagesAdditionLogs = [];

    messagesLogs.push({
      message_type_id: 3,
      from_user_id: user.id,
      to_id: existingAthleteId,
      message: `You have been added to program: `,
      company_id: params.companyId
    });
    messagesAdditionLogs.push({
      other_id: params.programId,
      messages_additions_types_id: 1
    });
    let companiesAthletes = {
      company_id: params.companyId,
      athlete_id: existingAthleteId,
      company_athlete_relationship_type_id: 3,
      creator_id: user.id
    };

    const messagesIds = await trx('messages').insert(messagesLogs).returning('id');
    const messageAdditions = messagesAdditionLogs.map((item, index) => ({...item, message_id: messagesIds[index]}));
    await trx('messages_additions').insert(messageAdditions);

    let query = trx('companies_athletes').insert(companiesAthletes).toString();
    query += ' ON CONFLICT (company_id, athlete_id) DO UPDATE set company_athlete_relationship_type_id = EXCLUDED.company_athlete_relationship_type_id, creator_id = EXCLUDED.creator_id returning id;';
    let ids = await trx.raw(query);
    try {
      return await trx('programs_athletes').insert({
        company_athlete_id: ids.rows[0].id, program_id: params.programId
      }).returning(['company_athlete_id', 'program_id']);
    } catch (e) {
      throw new Error(e);
    }
  };

  async detachProgramFromAthlete({params, user, program}) {
    let [existingAthlete] = await knex('users').select('id', 'email').whereIn('id', params.athleteId);

    await knex.transaction(async trx => {
      let messagesLogs = [];
      let messagesAdditionLogs = [];

      messagesLogs.push({
        message_type_id: 3,
        from_user_id: user.id,
        to_id: existingAthlete.id,
        message: `You have been removed from program: `,
        company_id: params.companyId
      });
      messagesAdditionLogs.push({
        other_id: params.programId,
        messages_additions_types_id: 1
      });

      const messagesIds = await trx('messages').insert(messagesLogs).returning('id');
      const messageAdditions = messagesAdditionLogs.map((item, index) => ({...item, message_id: messagesIds[index]}));
      await trx('messages_additions').insert(messageAdditions);

      let ids = await trx('companies_athletes').select().where({
        company_id: params.companyId,
        athlete_id: existingAthlete.id,
        company_athlete_relationship_type_id: 3,
        creator_id: user.id
      });

      try {
        return await trx('programs_athletes').where({
          company_athlete_id: ids[0].id,
          program_id: params.programId
        }).del();
      } catch (e) {
        throw new Error(e);
      }
    });

    return {
      program: {name: program.name, id: program.id},
      athleteId: existingAthlete.id
    }
  }

  async searchAthletes({params}) {
    const athletes = await knex.select('email')
      .from(function () {
        this.select('u.email')
          .from('companies_athletes as ca')
          .leftJoin('users as u', 'u.id', 'ca.athlete_id')
          .where('ca.company_id', params.companyId)
          .andWhere('u.email', 'like', `%${params.term}%`)
          .andWhere('user_role', 3)
          .groupBy('u.id').as('users')
      })
      .returning('email');
    return athletes.map(athlete => ({value: athlete.email, label: athlete.email}));
  }


  async getAllAthleteProgress({params}) {
    let programs = await knex('programs as p').select('p.id', 'p.name',
      knex.raw(`((max(s.week_number) + 1) * (max(es.day_number) + 1)) as program_duration`))
      .where(function () {
        this.whereNot('p.program_status_id', 3).orWhereNull('p.program_status_id')
      })
      .andWhere('p.company_owner_id', params.companyId)
      .leftJoin('exercises_settings as es', 'es.program_id', 'p.id')
      .leftJoin('sets as s', 's.exercise_setting_id', 'es.id')
      .leftJoin('programs_athletes as pa', 'p.id', 'pa.program_id')
      .leftJoin('companies_athletes as ca', 'ca.id', 'pa.company_athlete_id')
      .where({'ca.athlete_id': params.athleteId})
      .groupBy('p.id');

    let progresses = await knex('days_feedback').select('program_id')
      .count('id as progress').min('created_at as start_date')
      .whereIn('program_id', programs.map(program => program.id))
      .andWhere({athlete_id: params.athleteId})
      .groupBy('program_id');

    programs.forEach(program => {
      let foundProgress = progresses.find(progress => progress.program_id === program.id);
      program.progress = foundProgress ? foundProgress.progress : 0;
      program.start_date = foundProgress ? foundProgress.start_date : null;
    });

    return programs;
  }

  async getAthletesRecentActivity({params}) {
    return await knex('exercises_results as er').select('er.time_elapsed_sec', 'er.rest_time', 'er.created_at',
      'er.updated_at', 'er.day_number', 'er.week_number', 'er.characters_in_sets', 'er.order', 'ers.duration', 'ers.weight', 'p.name as program_name',
      'e.logo_image_url', 'e.name as exercise_name', 'mtu.title as duration_title', 'ers.execution_order')
      .leftJoin('exercises_results_sets as ers', 'ers.exercise_result_id', 'er.id')
      .leftJoin('programs as p', 'p.id', 'er.program_id')
      .leftJoin('exercises as e', 'er.exercise_id', 'e.id')
      .leftJoin('measurements_units_types as mtu', 'mtu.id', 'ers.duration_measurement_type_id')
      .where({'er.athlete_id': params.athleteId})
      .groupBy('ers.id', 'er.id', 'p.id', 'e.id', 'mtu.id')
      .orderBy('er.order', 'asc');
  }

  async getAthletePrograms({params, query}) {
    if (query.belong === 'true') {
      return await knex('companies_athletes as ca').select('p.id', 'p.name')
        .where(function () {
          this.whereNot('p.program_status_id', 3).orWhereNull('p.program_status_id')
        })
        .andWhere('p.company_owner_id', params.companyId)
        .andWhere({athlete_id: params.athleteId})
        .join('programs_athletes as pa', 'pa.company_athlete_id', 'ca.id')
        .join('programs as p', 'p.id', 'pa.program_id');
    }

    const athleteProgramsIds = await knex('companies_athletes as ca').select('pa.program_id')
      .where({athlete_id: params.athleteId})
      .join('programs_athletes as pa', 'pa.company_athlete_id', 'ca.id');

    return await knex('programs as p').select('p.name', 'p.id')
      .where(function () {
        this.whereNot('program_status_id', 3).orWhereNull('program_status_id')
      })
      .andWhere({company_owner_id: params.companyId})
      .whereNotIn('p.id', athleteProgramsIds.map(program => program.program_id));
  }

  async addMetadataToAthlete({body, params}) {
    await metadataService.assignMetadataToAthlete({
      metadata: body.metadata,
      companyId: params.companyId,
      athleteId: params.athleteId
    }, knex);

    return {
      data: {message: "Assigned to athlete"},
      code: 200
    };
  }

  async reinviteAthlete({body, params}) {

    let [user] = await knex('emails_tokens')
      .select('token', 'other_id', 'u.email')
      .leftJoin(knex.raw(`users as u on u.id=other_id and u.user_role=3 `))
      .where('other_id', params.athleteId)
      .andWhere('email_token_type_id', 1);

    if (!_.isEmpty(user)) {
      emailService.inviteNewAthlete({
        email: user.email,
        link: `${config.adminUrl}/athlete/create?token=${user.token}`
      });
      return true;
    } else {
      return false;
    }

  }
}

module.exports = new AthletesController();
