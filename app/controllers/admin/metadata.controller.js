const Response = require('../../../config/lib/response');
const knex = require('../../../config/lib/knex').db;
const _ = require('lodash');
const config = require('../../../config/config');
const metadataService = require('../../services/metadata.service');

/**
 * Methods for the metadata
 */
class MetadataController extends Response {
  async createMetadata({body, params}) {

    return await metadataService.createOrUpdateMetadata({metadata: body.metadata, companyId: params.companyId}, knex);
  }

  async getAllMetadata({params}) {
    return await knex('metadata').select('key', 'value').where({company_id: params.companyId})
  }

  async deleteMetadata({params}) {
    return await knex('metadata').delete().where({id: params.metadataId}).returning('id')
  }

  async getAllAthletesMetadata({params}) {
    return await knex('metadata as m').select('m.key', 'm.value')
      .whereExists(function() {
        this.select('*').from('athletes_metadata').whereRaw('m.id = athletes_metadata.metadata_id');
      })
      .andWhere({'m.company_id': params.companyId});
  }

  async getAllExercisesMetadata() {
    return await knex('metadata as m').select('m.key', 'm.value')
      .whereExists(function() {
        this.select('*').from('exercises_metadata').whereRaw('m.id = exercises_metadata.metadata_id');
      });
  }
}

module.exports = new MetadataController();
