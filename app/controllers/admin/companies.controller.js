'use strict';

/**
 * Module dependencies.
 */
const Response = require('../../../config/lib/response');
const knex = require('../../../config/lib/knex').db;
const config = require('../../../config/config');

class CompaniesController extends Response {
  async createCompany({body}) {
    const company = await knex('companies').insert({
      name: body.name,
      logo_url: body.logo_url
    }).returning(['id', 'name', 'logo_url', 'created_at', 'updated_at']);

    return company[0];
  }

  async addAdminToCompany({params}) {
    await knex
      .insert({
        company_id: params.companyId,
        admin_id: params.adminId,
        company_member_type: 1
      }).into('companies_admins');

    const companiesAdmins = await knex('companies_admins')
      .select(
        'companies.id as companyId', 'companies.name as companyName',
        'users.id as userId', 'users.email as userEmail',
        'users.user_role as userRole',
        'users.name as userName'
      )
      .where({
        'companies_admins.admin_id': params.adminId,
        'companies_admins.company_id': params.companyId
      })
      .limit(1)
      .innerJoin('companies', 'companies.id', 'companies_admins.company_id')
      .innerJoin('users', 'companies_admins.admin_id', 'users.id');


    companiesAdmins[0]['userRole'] = companiesAdmins[0]['userRole'] === 1 ? 'admin' : 'coach';

    return companiesAdmins[0];
  }

  async deleteAdminFromCompany({params}) {
    await knex('companies_admins')
      .where({
        admin_id: params.adminId,
        company_id: params.companyId
      })
      .delete();

    return {
      companyId: params.companyId,
      userId: params.adminId
    }
  }

  async editCompany({body, params}) {
    const company = await knex('companies').update({
      name: body.name,
      logo_url: body.logo_url
    })
      .where({id: params.companyId})
      .returning(['id', 'name']);

    return company[0];
  }

  async removeCompany({params}) {
    return (await knex('companies').delete().where({id: params.companyId}).returning(['id']))[0];
  }

  async getCompanyCoaches({params}) {
    let coaches = await knex('companies_admins as ca')
      .select('u.id', 'u.name', 'u.email', 'us.profile_image_url', 'u.is_email_confirmed')
      .leftJoin('users as u', 'u.id', 'ca.admin_id')
      .leftJoin('users_settings as us', 'us.user_id', 'ca.admin_id')
      .where('ca.company_id', params.companyId);

    let company = (await knex('companies')
      .select('id', 'name', 'logo_url')
      .where('id', params.companyId))[0];

    let result = {company, coaches};

    return result;
  }

  async getCompanyProfile({params}) {
    const [company] = await knex('companies_admins')
      .select('companies_admins.company_id as company_id', 'companies.name as name', 'companies.logo_url as logo_url')
      .where('companies_admins.admin_id', params.user)
      .innerJoin('companies', 'id', 'company_id')
      .returning('company_id', 'name', 'logo_url');

    return {id: company.company_id, name: company.name, logo_url: company.logo_url};
  }
}

module.exports = new CompaniesController();
