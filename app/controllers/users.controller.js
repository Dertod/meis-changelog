'use strict';

/**
 * Module dependencies.
 */
const Response = require('../../config/lib/response');
const knex = require('../../config/lib/knex').db;
const bucket = require('../../config/lib/googleCloud');
const chalk = require('chalk');
const moment = require('moment');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');
const emailService = require('../services/email.service');
const usersService = require('../services/users.service');
const TFAServise = require('../services/twoFactorAuth.servise');


const generalFields = ['name', 'email'];

/**
 * Methods for the users and auth
 */
class UsersController extends Response {

  /**
   * Authorization route handler
   * @param {object} body: { {string} email, {string} password}
   * @returns {object} token - JWT token with credentials and company
   */
  async signin({body}) {
    const {email, password, user_type, social_id = false} = body;
    let userRole = user_type === 2 ? [1, 2] : [3];

    const [result] = await knex.select('id', 'email', 'name', 'profile_image_url', 'user_role'
    )
      .from(function () {
        this.select('users.id as id', 'email', 'users.name as name', 'profile_image_url', 'users.user_role as user_role',
          knex.raw(`(hash_password = crypt('${encodeURIComponent(password)}', hash_password)) as is_same_passwords`))
          .from('users')
          .leftJoin('users_settings as us', 'us.user_id', 'users.id')
          .where({
            email: email
          })
          .whereIn('user_role', userRole).as('ignored_alias')
          .limit(1);
      })
      .where({
        is_same_passwords: true
      });
    if (result && social_id) {
      await knex('users_settings')
        .where({'user_id': result.id})
        .update({
          facebook_id: social_id,
        });

    }
    if (!result) {
      if(social_id){
        return {
          fbError:true,
          data: {message: 'Invalid password'},
          code: 400
        };
      }
      return {
        data: {message: 'Invalid email or password'},
        code: 400
      };
    }

    const company = await this.getCompanyByUser(result.id, body.user_type);

    const twoFactorAuth = await this.getTwoFactorAuthByUser(result.id);

    return {
      token: jwt.sign(result, config.jwt.secret, {expiresIn: config.jwt.ttl}),
      ttl: config.jwt.ttl,
      company,
      twoFactorAuth: twoFactorAuth
    };
  }

  async signinViaSocial({body}) {
    const {social_id, provider, profile, user_type, fb_email_confirm = false} = body;
    let userData;
    const userRoles = user_type === 3 ? [3] : [1, 2];
    if (social_id) {
      [userData] = await knex('users')
        .select('users.id as id', 'email', 'users.name as name', 'profile_image_url', 'users.user_role as user_role', 'hash_password as password')
        .leftJoin('users_settings as us', 'us.user_id', 'users.id')
        .where({
          email: profile.email
        })
        .whereIn('user_role', userRoles);
    } else throw new Error(JSON.stringify({custom: 'Social id doesn\'t exists'}));

    if (!_.isEmpty(userData)) {
      if (fb_email_confirm) {
        if (userData.password) {
          return {
            fbActivated: true,
            fbEmailConfirm: false,
            fbPasswordConfirm: true,
          }
        } else {
          return {
            fbActivated: true,
            fbEmailConfirm: false,
            fbPasswordConfirm: false,
          }
        }
      }
      const company = await this.getCompanyByUser(userData.id, userData.user_role);
      const twoFactorAuth = await this.getTwoFactorAuthByUser(userData.id);
      return {
        is_new_user: false,
        token: jwt.sign(userData, config.jwt.secret, {expiresIn: config.jwt.ttl}),
        ttl: config.jwt.ttl,
        company,
        twoFactorAuth
      }
    } else {
      const newUser = await this.registerNewUser(body);
      const twoFactorAuth = {
        activated: false,
        confirm: false,
        step: 1,
        showModal: false
      };
      let company;
      if (user_type && user_type !== 3) {
        [company] = await knex.into('companies')
          .returning(['id', 'name'])
          .insert({
            name: profile.name
          });

        await knex
          .insert({
            company_id: company.id,
            admin_id: newUser.id,
            company_member_type: 1
          })
          .into('companies_admins');

        const [company_role] = await knex('companies_members_types')
          .select('title')
          .where({
            id: 1
          })
          .returning('title');

        company.role = company_role.title;
      }

      return {
        is_new_user: true,
        token: jwt.sign(newUser, config.jwt.secret, {expiresIn: config.jwt.ttl}),
        ttl: config.jwt.ttl,
        company,
        twoFactorAuth
      };
    }

  }

  async notEmailSignFacebook({query}) {
    const [userData] = await knex('users_settings as us')
      .select('u.id', 'u.name', 'u.email', 'u.user_role', 'us.profile_image_url', 'us.weight', 'us.height', 'us.age', 'us.facebook_id')
      .where({'us.facebook_id': query.facebookId})
      .leftJoin('users as u', 'u.id', 'us.user_id');
    if (!_.isEmpty(userData)) {
      const company = await this.getCompanyByUser(userData.id, userData.user_role);
      const twoFactorAuth = await this.getTwoFactorAuthByUser(userData.id);
      return {
        token: jwt.sign(userData, config.jwt.secret, {expiresIn: config.jwt.ttl}),
        ttl: config.jwt.ttl,
        company,
        twoFactorAuth
      }
    }
    else {
      return {
        fbActivated: true,
        fbEmailConfirm: true,
        fbPasswordConfirm: false,
      }
    }
  }

  async registerNewUser(body) {
    const {social_id, provider, profile, user_type, fb_email_confirm = false} = body;
    let user = await knex.transaction(async trx => {
      try {
        let userInfo = await trx
          .returning(['id', 'name', 'email', 'user_role'])
          .insert({
            name: profile.name,
            user_role: 2,
            email: profile.email.toLowerCase()
          })
          .into('users');

        let defaultSettings = await trx
          .returning(['profile_image_url'])
          .insert({
            profile_image_url: profile.img,
            user_id: userInfo[0].id,
            facebook_id: fb_email_confirm && social_id ||null
          })
          .into('users_settings');

        return userInfo;
      }
      catch (e) {
        throw new Error(e);
      }
    });

    return (await knex('users as u')
      .select('u.id', 'u.name', 'u.email', 'u.user_role', 'us.profile_image_url')
      .leftJoin('users_settings as us', 'us.user_id', 'u.id')
      .where('u.id', user[0].id))[0];
  }

  /**
   * Registration route handler
   * @param {object} body: { {string} email, {string} password, {string} user_type, {string} name}
   * @returns {object} token - JWT token with credentials and company
   */
  async signup({body}) {
    let user;
    let tokens;
    let company;
    let isRegisteredAdmin = !_.isEmpty(await knex('users').select('id').where({email: body.email, user_role: 1}));

    tokens = await knex.transaction(async trx => {
      if (body.user_type && body.user_type === 2 && isRegisteredAdmin) {
        throw new Error('Email already exists');
      }

      [user] = await trx
        .insert({
          email: body.email.toLowerCase(),
          user_role: body.user_type,
          name: body.name,
          hash_password: knex.raw(`crypt('${encodeURIComponent(body.password)}', gen_salt(\'md5\'))`),
        }, 'id').into('users').returning(['id', 'email', 'name', 'user_role']);

      if (body.user_type && body.user_type !== 3) {
        [company] = await trx.into('companies')
          .returning(['id', 'name'])
          .insert({
            name: body.name
          });

        await trx
          .insert({
            company_id: company.id,
            admin_id: user.id,
            company_member_type: 1
          })
          .into('companies_admins');

        const [company_role] = await knex('companies_members_types')
          .select('title')
          .where({
            id: 1
          })
          .returning('title');

        company.role = company_role.title;
      }

      return await trx
        .returning('token')
        .insert({
          other_id: user.id,
          token: knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`),
          email_token_type_id: 2
        })
        .into('emails_tokens');
    });

    emailService.sendConfirmationOfEmail({
      name: body.name,
      email: body.email.toLowerCase(),
      link: `${config.adminUrl}/users/confirm?token=${tokens[0]}`,
      language: 1
    });
    return {
      token: jwt.sign(user, config.jwt.secret, {expiresIn: config.jwt.ttl}),
      ttl: config.jwt.ttl,
      company
    }
  }

  /**
   * Confirmation of email of registered user
   * @param {object} body: { {string} token }
   * @returns {object} token - JWT token with credentials and company
   */
  async confirmEmail({body}) {
    let emailTokens = await knex('emails_tokens')
      .select('emails_tokens.token as token', 'emails_tokens.other_id as other_id', 'users.is_email_confirmed as is_confirmed')
      .where({
        token: body.token,
        email_token_type_id: 2
      })
      .innerJoin('users', 'users.id', 'emails_tokens.other_id');

    if (!emailTokens.length) {
      return {
        data: {message: 'User not found'},
        code: 401
      };
    }

    if (emailTokens.is_confirmed) {
      return {
        data: {message: `User's email was already confirmed`},
        code: 403
      };
    }


    let users = await knex('users')
      .returning(['id', 'email', 'name', 'user_role', 'is_email_confirmed'])
      .where({
        id: emailTokens[0].other_id
      })
      .update('is_email_confirmed', true);

    let [userSettings] = await knex('users_settings')
      .select('profile_image_url')
      .where({
        user_id: emailTokens[0].other_id
      });

    await knex('emails_tokens')
      .where({
        other_id: users[0].id,
        email_token_type_id: 2
      })
      .del();

    users[0].profile_image_url = userSettings ? userSettings.profile_image_url : undefined;

    const company = await this.getCompanyByUser(users[0].id, users[0].user_role);

    return {
      token: jwt.sign(users[0], config.jwt.secret, {expiresIn: config.jwt.ttl}),
      ttl: config.jwt.ttl,
      company
    };
  }

  /**
   * Invite new admin to company route's handler
   * @param body: { email, name}
   * @returns {object} { {string} email, {string} name}
   */
  async inviteCoachesToCompany({body, params, company}) {
    const existedCoaches = await knex('users as u').select('u.id', 'u.email')
      .whereIn('u.user_role', [1, 2])
      .whereIn('u.email', body.emails);

    const invitedCoaches = await knex('users as u').select('u.email')
      .leftJoin('companies_admins as ca', 'ca.admin_id', 'u.id')
      .where('ca.company_id', company.id)
      .whereIn('u.email', body.emails)
      .map(coach => coach.email);

    const errors = body.emails.filter(email => invitedCoaches.includes(email));
    if (errors.length > 0) {
      throw new Error(errors);
    }

    const existedCoachesEmails = existedCoaches.map(coach => coach.email);
    const newCoachesData = body.emails.filter(email => !existedCoachesEmails.includes(email))
      .map(email => ({email: email, name: email.split('@')[0]}));

    const newCoachesIds = await Promise.all(newCoachesData.map(async(coach) => {
      return await await usersService.inviteNewUser(coach, 2);
    }));

    await knex.insert(newCoachesIds.map(coach => ({
      company_id: company.id,
      admin_id: coach.user_id,
      company_member_type: 1
    })))
      .into('companies_admins');

    await knex.insert(existedCoaches.map(coach => ({
      company_id: company.id,
      admin_id: coach.id,
      company_member_type: 1
    })))
      .into('companies_admins');

    await Promise.all(newCoachesIds.map(async(coach, index) => {
      return await emailService.inviteNewAdmin({
        name: newCoachesData[index].name,
        email: newCoachesData[index].email,
        link: `${config.adminUrl}/password/create?token=${coach.token}`
      });
    }));

    return "Coaches have been invited successfully";
  }

  /**
   * Password creation for invited admins
   * @param body: { token, password }
   * @returns {email}
   */
  async createPassword({body}) {
    return await this.handlePassword(body, 1);
  }

  /**
   * Password editing for forgotten their passwords
   * @param body: { token, password }
   * @returns {email}
   */
  async updatePassword({body}) {
    return await this.handlePassword(body, 3);
  }

  /**
   * Handler for password editing operations
   * @param {object} body: { {string} token, {string} password}
   * @param {number} emailTokenTypeId - ID for appropriate email token type
   * @returns {email}
   */
  async handlePassword(body, emailTokenTypeId) {

    let emailTokens = await knex('emails_tokens')
      .select('token', 'emails_tokens.other_id as other_id')
      .where({
        token: body.token,
        email_token_type_id: emailTokenTypeId
      });

    if (!emailTokens.length) {
      return {
        data: {message: 'Authorization error'},
        code: 401
      };
    }

    let admins = await knex('users')
      .returning(['id', 'email', 'name', 'user_role'])
      .where({
        id: emailTokens[0].other_id
      })
      .update('hash_password', knex.raw(`crypt('${escape(body.password)}', gen_salt(\'md5\'))`));

    const company = await this.getCompanyByUser(admins[0].id, admins[0].user_role);

    await knex('emails_tokens')
      .where({
        other_id: admins[0].id,
        email_token_type_id: emailTokenTypeId
      })
      .del();

    if (emailTokenTypeId === 1) {
      await knex('users')
        .where({
          id: admins[0].id,
          user_role: admins[0].user_role
        })
        .update({is_email_confirmed: true})
    }


    return {
      token: jwt.sign(admins[0], config.jwt.secret, {expiresIn: config.jwt.ttl}),
      ttl: config.jwt.ttl,
      company
    };
  }

  /**
   * Handler for forgot password issue, checking email owner by sending token to it
   * @param {object} body: { {string} email}
   * @returns {email}
   */
  async forgotPassword({body}) {
    const users = await knex('users')
      .select('id', 'name', 'email')
      .where({
        email: body.email
      })
      .andWhere({user_role: body.user_role})
      .limit(1);

    if (!users.length) {
      return {
        data: {message: 'Email not found'},
        code: 400
      };
    }

    let tokens = await knex('emails_tokens')
      .select('token')
      .where({other_id: users[0].id})
      .andWhere({email_token_type_id: 3}).map((row) => {
        return row.token;
      });

    if (!tokens || !tokens.length) {
      tokens = await knex
        .returning('token')
        .insert({
          other_id: users[0].id,
          token: knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`),
          email_token_type_id: 3
        })
        .into('emails_tokens');
    }

    emailService.recoveryPassword({
      name: users[0].name,
      email: users[0].email,
      language: 1,
      link: `${config.adminUrl}/password/edit?token=${tokens[0]}`
    });

    return {
      email: users[0].email
    };
  }

  async checkRestoreToken({body}) {

    let token = await knex('emails_tokens')
      .select('*')
      .where({token: body.token});

    return !!token.length;

  }

  async statusAthletePassword(req, res) {

    let emailTokens = await knex('emails_tokens')
      .select('token', 'other_id')
      .where({
        token: req.query.token,
      });
    if (emailTokens[0]) {
      return {
        token: req.query.token,
        tokenUsed: false
      };
    }
    return {
      message: 'Your token has already been used',
      token: req.query.token,
      tokenUsed: true
    };
  }

  async changeUserPassword(req, res) {

    const [result] = await knex.select('id', 'email', 'name'
    )
      .from(function () {
        this.select('users.id as id', 'email', 'users.name as name',
          knex.raw(`(hash_password = crypt('${encodeURIComponent(req.body.current_password)}', hash_password)) as is_same_passwords`))
          .from('users')
          .where({
            id: req.user.id
          }).andWhere(function () {
          this.where({user_role: 1}).orWhere({user_role: 2})
        }).as('ignored_alias')
          .limit(1);
      })
      .where({
        is_same_passwords: true
      });

    if (result) {
      await knex('users')
        .where({id: req.user.id})
        .update('hash_password', knex.raw(`crypt('${encodeURIComponent(req.body.new_password)}', gen_salt(\'md5\'))`))
    } else {
      return {
        data: {message: 'Sorry, but your current password is not correct'},
        code: 400
      };
    }

    const company = await this.getCompanyByUser(req.user.id);

    result.profile_image_url = req.user.profile_image_url;
    return {
      token: jwt.sign(result, config.jwt.secret, {expiresIn: config.jwt.ttl}),
      ttl: config.jwt.ttl,
      company
    };
  }

  async sendPasswordLinkForAthlete({body}) {
    const [athlete] = await knex('users').select('*').where({email: body.email});

    if (athlete) {
      if ((athlete.hash_password && athlete.hash_password.length)) {
        return {status: 204, message: 'Password was already created for this email'}
      }

      return {status: 200, message: 'Password creating link was sent to your email'}
    } else {
      return {status: 406, message: 'There are no athletes with this email'}
    }
  }

  async sendEmailWithCodeForRecoveringPasswordMobile({body}) {
    const users = await knex('users')
      .select('id', 'name', 'email')
      .where({
        email: body.email
      })
      .andWhere({user_role: 3 /* ATHLETE */})
      .limit(1);

    if (!users.length) {
      // Do NOT tell if the user exists to the requester.
      // In the mobile app the email address is already checked for existence and must be valid.
      return {
        message: `The code was sent to ${body.email}`,
        status: 200
      };
    }

    let token_entry = (await knex('emails_tokens')
      .select('id', 'token', 'created_at')
      .where({other_id: users[0].id})
      .andWhere({email_token_type_id: 3})
      .orderBy('created_at', 'desc'))[0];

    let code = null;

    const moreThanOneMinutePastSinceTokenCreation = (createdAt) => {
      return (Date.now() - createdAt) > (60 * 1000 /* 1 Minute */);
    }

    if (!token_entry || moreThanOneMinutePastSinceTokenCreation(token_entry.created_at)) {

      code = (await knex
        .returning('token')
        .insert({
          other_id: users[0].id,
          token: knex.raw(`pseudo_encrypt(nextval('idsSeq')::int)`),
          email_token_type_id: 3
        })
        .into('emails_tokens'))[0];

    } else {
      return {
        status: 200,
        message: `The code was sent less than a minute ago. Please wait for the email with the code`
      };
    }

    emailService.recoveryPasswordMobile({
      name: users[0].name,
      email: users[0].email,
      language: 1,
      code
    });

    return {status: 200, message: `The code was sent to ${body.email}`};
  }

  async checkCodeForRecoveringPasswordMobile({body}) {
    const users = await knex('users')
      .select('id', 'name', 'email')
      .where({
        email: body.email
      })
      .andWhere({user_role: 3 /* ATHLETE */})
      .limit(1);

    if (!users.length) {
      // Do NOT tell if the user exists to the requester.
      // In the mobile app the email address is already checked for existence and must be valid.
      return {
        message: 'Wrong code',
        status: 400
      };
    }

    let token_entry = (await knex('emails_tokens')
      .select('id', 'token', 'created_at', 'attempts_number')
      .where({other_id: users[0].id})
      .andWhere({email_token_type_id: 3})
      .orderBy('created_at', 'desc'))[0];

    const isExpired = (createdAt) => {
      return (Date.now() - createdAt) > (10 * 60 * 1000 /* 10 Minutes */);
    };

    if (!token_entry || isExpired(token_entry.created_at) || token_entry.attempts_number >= 3) {
      return {status: 400, message: 'Wrong code'};
    }

    if (token_entry) {
      await knex('emails_tokens')
        .update({
          attempts_number: token_entry.attempts_number + 1
        })
        .where({id: token_entry.id});
    }

    if (token_entry.token !== body.code) {
      return {status: 400, message: `Wrong code. ${3 - (token_entry.attempts_number + 1)} attempts left`};
    } else {
      const newPassword = body.password;

      await knex('users')
        .where({
          id: users[0].id
        })
        .update('hash_password', knex.raw(`crypt('${escape(newPassword)}', gen_salt(\'md5\'))`));

      return {status: 200, message: 'Password updated'};
    }

  }

  async updateUserProfile(req, res) {
    await knex.transaction(async(trx) => {

      await trx
        .where({id: req.user.id})
        .update({
          name: req.body.name,
          email: req.body.email.toLowerCase(),
        })
        .into('users');

      let settingsBody = {
        profile_image_url: req.body.profile_image_url,
        weight: req.body.weight,
        height: req.body.height,
        age: req.body.age,
        user_id: req.user.id,
      };
      let settingsQuery = trx('users_settings').insert(settingsBody).toString();
      settingsQuery += ' ON CONFLICT (user_id) DO UPDATE set profile_image_url=EXCLUDED.profile_image_url,' +
        ' weight=EXCLUDED.weight, height=EXCLUDED.height, age=EXCLUDED.age;';

      return await trx.raw(settingsQuery);
    });

    const company = await this.getCompanyByUser(req.user.id, req.body.user_type);
    return {
      profile: req.body,
      authData: {
        token: jwt.sign({
          name: req.body.name,
          email: req.body.email,
          id: req.user.id,
          user_role: req.body.user_role,
          profile_image_url: req.body.profile_image_url
        }, config.jwt.secret, {expiresIn: config.jwt.ttl}),
        ttl: config.jwt.ttl,
        company
      }
    }
  }

  async userProfile(req, res) {
    const [profile] = await knex('users')
      .select('users.name', 'users.email', 'users.user_role', 'us.profile_image_url', 'us.weight', 'us.height', 'us.age')
      .where({'users.id': req.user.id})
      .leftJoin('users_settings as us', 'us.user_id', 'users.id');

    profile.company = await this.getCompanyByUser(req.user.id, profile.user_role);

    return profile;
  }


  async uploadImage(req, res) {

    const gcsname = `${req.user.id}/${Date.now()}`;
    const file = bucket.file(gcsname);
    const stream = file.createWriteStream({
      metadata: {contentType: req.file.mimetype}
    });
    const response = {};

    function getPublicUrl(filename) {
      return `https://storage.googleapis.com/${config.googleCloud.bucketName}/${filename}`;
    }

    return await new Promise((res, rej) => {

      stream.on('error', (error) => {
        return rej(error);
      });

      stream.on('finish', () => {
        file.makePublic().then(() => {
          response.public_url = getPublicUrl(gcsname);
          return res(response.public_url)
        });
      });

      stream.end(req.file.buffer);
    });
  }

  async getCompanyByUser(userId, userRole) {
    if (userRole !== 3) {
      const [company] = await knex('companies_admins')
        .select('companies_admins.company_id as company_id', 'companies.name as name', 'companies_members_types.title as role')
        .where('companies_admins.admin_id', userId)
        .innerJoin('companies', 'id', 'company_id')
        .innerJoin('companies_members_types', 'companies_members_types.id', 'companies_admins.company_member_type')
        .returning('company_id', 'name', 'role');

      return {id: company.company_id, name: company.name, role: company.role};
    }
  }

  async getTwoFactorAuthByUser(userId) {
    const [secretToken] = await knex('two_factor_auth_tokens').select('token')
      .where('other_id', userId);

    if (secretToken) {
      return {
        activated: true,
        confirm: false,
        step: 3,
        showModal: true
      }
    }
    return {
      activated: false,
      confirm: false,
      step: 1,
      showModal: false
    };
  }


  async searchCoaches({body, params}) {
    const coaches = await knex('users as u').select('u.id', 'u.email')
      .where('u.email', 'like', `%${params.term}%`)
      .where('user_role', 2);

    return coaches.map(coache => ({value: coache.email, label: coache.email}));
  }

  async activate2FA(req, res) {
    return await TFAServise.createSecretToken(req.user.id)
  }

  async verification2FA(req, res) {
    return await TFAServise.confirmSecretToken(req.body, req.user.id)
  }

  async deactivate2FA(req, res) {
    return await TFAServise.deleteSecretToken(req.body, req.user.id)
  }

  async subscribe({body}) {
    await knex('subscribe_list')
      .insert({
        email: body.email
      });
    return true;
  }
}

module.exports = new UsersController();
