'use strict';

/**
 * Module dependencies.
 */
const Response = require('../../config/lib/response');
const knex = require('../../config/lib/knex').db;
const config = require('../../config/config');
const bucket = require('../../config/lib/googleCloud');
const chalk = require('chalk');
const moment = require('moment');
const _ = require('lodash');


class TicketsController extends Response {

  async createTicket({body}) {
    return await knex('tickets')
      .insert({
        title: body.title,
        description: body.description,
        ticket_type: body.type,
        user_id: body.author,
        ticket_status: 3,
        image_url: body.image_url
      })
      .returning('id');
  }

  async createComment({body, params}) {
    return await knex('comments')
      .insert({
        text: body.text,
        user_id: body.author,
        ticket_id: params.ticketId
      })
      .returning('id');
  }

  async changeStatus({body, params}) {
    return (await knex('tickets')
      .returning('id')
      .where({
        id: params.ticketId
      })
      .update('ticket_status', body.status))[0];
  }

  async getAllTickets() {
    let tickets = await knex('tickets as t')
      .select('u.id as creator_id', 'u.name as author', 'us.profile_image_url as creator_logo_url', 't.id', 't.title', 't.description', 't.updated_at', 't.created_at as date', 't.image_url', 'ts.name as status', 'tt.name as type',
        knex.raw(`(select count(ticket_id) from comments where comments.ticket_id = t.id) as comments_count`))
      .count('tv.ticket_id as votes')
      .leftJoin('tickets_votes as tv', 'tv.ticket_id', 't.id')
      .leftJoin('users as u', 'u.id', 't.user_id')
      .leftJoin('tickets_statuses as ts', 'ts.id', 't.ticket_status')
      .leftJoin('tickets_types as tt', 'tt.id', 't.ticket_type')
      .leftJoin('users_settings as us', 'us.user_id', 't.user_id')
      .groupBy('u.id', 't.id', 'ts.id', 'tt.id', 'us.id');

    return tickets;
  }

  async getTicketById({params}) {
    return (await knex('tickets as t')
      .select('u.id as creator_id', 'u.name as author','us.profile_image_url as creator_logo_url', 't.id as id', 't.title as title', 't.description', 't.updated_at', 't.created_at', 't.image_url', 'ts.name as status', 'tt.name as type')
      .count('tv.ticket_id as votes')
      .leftJoin('tickets_votes as tv', 'tv.ticket_id', 't.id')
      .leftJoin('users as u', 'u.id', 't.user_id')
      .leftJoin('tickets_statuses as ts', 'ts.id', 't.ticket_status')
      .leftJoin('tickets_types as tt', 'tt.id', 't.ticket_type')
      .leftJoin('users_settings as us', 'us.user_id', 't.user_id')
      .groupBy('u.id', 't.id', 'ts.id', 'tt.id', 'us.id')
      .where('t.id', params.ticketId))[0];
  }

  async deleteTicketById({params}) {
    return (await knex('tickets').delete().where({id: params.ticketId}).returning(['id']))[0];
  }

  async getCommentsById({params}) {
    let result = (await knex('comments as c')
      .select('c.id as id', 'c.ticket_id as ticket_id', 'c.user_id as creator_id', 'c.text as text', 'c.created_at as date', 'u.name as creator_name', 'us.profile_image_url as creator_logo_url')
      .leftJoin('users as u', 'u.id', 'c.user_id')
      .leftJoin('users_settings as us', 'us.user_id', 'c.user_id')
      .orderBy('c.created_at', 'asc')
      .where('c.ticket_id', params.ticketId));

    return result;
  }

  async voteTicket({params}){
    return await knex('tickets_votes')
      .insert({
        user_id: params.user,
        ticket_id: params.ticketId
      })
      .returning('id');
  }

  async deleteTicketVote({params}){
    return (await knex('tickets_votes').delete().where({ticket_id: params.ticketId, user_id: params.user}).returning(['id']))[0];
  }

  async getUserVotesById({params}) {
    let result = [];
    let votes = await knex('tickets_votes').select('ticket_id').where('user_id', params.id);
    votes.forEach( (o, i , votes) => {
      result.push(o.ticket_id.toString());
    });

    return result;
  }

  async getTicketVotesCountById({params}) {
    let votes = await knex('tickets_votes').count('ticket_id').where('ticket_id', params.ticketId);
    let count = '0';

    if (votes.length && votes[0].count) {
      count = votes[0].count;
    }

    return count;
  }

  async getPendingTicketsCount() {
    let result = await knex('tickets').count('id').where('ticket_status', 3);
    let count='0';
    if (result.length && result[0].count) {
      count = result[0].count;
    }
    return count;
  }
}


module.exports = new TicketsController();
