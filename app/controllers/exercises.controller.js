'use strict';

/**
 * Module dependencies.
 */
const Response = require('../../config/lib/response');
const knex = require('../../config/lib/knex').db;
const _ = require('lodash');
const metadataService = require('../services/metadata.service');


class ExercisesController extends Response {
  async getAllExercises({user}) {
    return await knex('exercises as e').select('e.id', 'e.name', 'e.logo_image_url', 'e.creator_id', 'e.is_moderated', 'e.category_type_id',
      knex.raw(`COALESCE(json_agg(json_build_object('id', m.id,'key', m.key, 'value' , m.value))
       FILTER (WHERE m.id IS NOT NULL), '[]') as metadata`),
      knex.raw(`COALESCE(json_agg(json_build_object('equipment_id', ee.equipment_id))
       FILTER (WHERE ee.equipment_id IS NOT NULL), '[]') as equipments`),
      knex.raw(`COALESCE(json_agg(json_build_object('muscle_id', emu.muscle_id))
       FILTER (WHERE emu.muscle_id IS NOT NULL), '[]') as muscles`)
    )
      .leftJoin('exercises_metadata as em', 'em.exercise_id', 'e.id')
      .leftJoin('metadata as m', 'm.id', 'em.metadata_id')
      .leftJoin('exercises_equipments as ee', 'ee.exercise_id', 'e.id')
      .leftJoin('exercises_muscles as emu', 'emu.exercise_id', 'e.id')
      .groupBy('e.id')
      .modify(function (exercises) {
        if (user.user_role === 'coach') {
          exercises.where('e.is_moderated', true)
            .orWhere('e.creator_id', user.id)
        } else if (user.user_role === 'athlete') {
          exercises.where('e.is_moderated', true)
        }
      });
  }


  async createExercise({body, user}) {
    return await knex.transaction(async trx => {
      let [exerciseId] = await trx('exercises')
        .insert({
          name: body.name,
          description: body.description,
          video_link: body.video_link,
          logo_image_url: body.logo_image_url,
          creator_id: user.id,
          category_type_id: body.category_type_id,
          is_moderated: user.user_role === 'admin'
        })
        .returning('id');

      if (body.metadata && body.metadata.length) {
        await metadataService.assignMetadataToExercise({
          metadata: body.metadata,
          authorId: user.id,
          exerciseId: exerciseId
        }, trx);
      }

      if (body.exercises_equipments && body.exercises_equipments.length) {
        await trx('exercises_equipments')
          .insert(body.exercises_equipments.map(equipment => {
            return {equipment_id: equipment.equipment_id, exercise_id: exerciseId}
          })).returning('equipment_id')
      }

      if (body.exercises_muscles && body.exercises_muscles.length) {
        await trx('exercises_muscles').insert(body.exercises_muscles.map(muscle => {
          return {muscle_id: muscle.muscle_id, exercise_id: exerciseId, muscle_group: muscle.muscle_group}
        }))
      }

      return [exerciseId];
    });
  }

  async createBulkExercises({body, user}) {
    return await knex('exercises')
      .insert((body.exercises.map(exercise => {
          return {
            ...exercise,
            creator_id: user.id,
            is_moderated: user.user_role === 'admin'
          }
        }))
      )
      .returning('id');
  }

  async getExerciseById({params}) {

    return (await knex('exercises as e')
      .select('u.id as creator_id', 'u.name as creator_name', 'e.id as id', 'e.name as name',
        'e.description', 'e.video_link', 'e.logo_image_url', 'e.is_moderated as is_moderated', 'e.category_type_id',
        knex.raw(`COALESCE(json_agg(json_build_object('id', m.id,'key', m.key, 'value' , m.value)) FILTER (WHERE m.id IS NOT NULL), '[]') as metadata`),
        knex.raw(`COALESCE(json_agg(json_build_object('equipment_id', ee.equipment_id,'equipment_name', ee.equipment_name)) FILTER (WHERE ee.equipment_id IS NOT NULL), '[]') as exercises_equipments`),
        knex.raw(`COALESCE(json_agg(json_build_object('muscle_id', emu.muscle_id, 'muscle_name', emu.muscle_name, 'muscle_group', emu.muscle_group, 'muscle_image', emu.muscle_image)) FILTER (WHERE emu.muscle_id IS NOT NULL), '[]') as exercises_muscles`)
      )
      .leftJoin('users as u', 'u.id', 'e.creator_id')
      .leftJoin('exercises_metadata as em', 'em.exercise_id', 'e.id')
      .leftJoin('metadata as m', 'm.id', 'em.metadata_id')
      .leftJoin(knex.raw(`(SELECT * FROM "exercises_equipments" AS "er" 
                           LEFT JOIN "exercises_equipments_types" AS "et" ON "et"."id" = "er"."equipment_id")
                           AS "ee" ON "ee"."exercise_id" = "e"."id"`))
      .leftJoin(knex.raw(`(SELECT * FROM "exercises_muscles" AS "emr" 
                           LEFT JOIN "exercises_muscles_types" AS "emt" ON "emt"."id" = "emr"."muscle_id")
                           AS "emu" ON "emu"."exercise_id" = "e"."id"`))
      .groupBy('e.id', 'u.id')
      .where('e.id', params.exerciseId))[0];
  }

  async editExerciseById({body, params, user}) {

    let updatedBody = _.pick(body, ['name', 'description', 'video_link', 'logo_image_url']);
    updatedBody.category_type_id = body.category_type_id ? body.category_type_id : null;

    updatedBody['is_moderated'] = user.user_role === 'admin';
    if (body.metadata) {
      await metadataService.unAssignAllMetadataFromExercise({exerciseId: params.exerciseId}, knex);

      if (body.metadata.length) {
        await metadataService.assignMetadataToExercise({
          metadata: body.metadata,
          authorId: user.id,
          exerciseId: params.exerciseId
        }, knex);
      }

      if (body.exercises_equipments && body.exercises_equipments.length) {
        await knex('exercises_equipments')
          .where('exercise_id', params.exerciseId)
          .del();
        await knex('exercises_equipments')
          .insert(body.exercises_equipments.map(equipment => {
            return {equipment_id: equipment.equipment_id, exercise_id: params.exerciseId}
          })).returning('equipment_id')
      }
      if (body.exercises_muscles && body.exercises_muscles.length) {
        await knex('exercises_muscles')
          .where('exercise_id', params.exerciseId)
          .del();
        await knex('exercises_muscles')
          .insert(body.exercises_muscles.map(muscle => {
            return {muscle_id: muscle.muscle_id, exercise_id: params.exerciseId, muscle_group: muscle.muscle_group}
          })).returning('muscle_id')
      }
    }

    return await knex('exercises').update(updatedBody).returning('id').where({id: params.exerciseId});
  }

  async deleteExerciseById({params}) {
    return (await knex('exercises').delete().where({id: params.exerciseId}).returning(['id']))[0];
  }

  async getExercisesCategories() {
    let categories = await knex('exercises_categories_types')
      .select('*');
    let equipments = await knex('exercises_equipments_types')
      .select('id as equipment_id', 'equipment_name');
    let muscles = await knex('exercises_muscles_types')
      .select('id as muscle_id', 'muscle_name', 'muscle_image');

    return {
      categories: categories,
      equipments: equipments,
      muscles: muscles
    }
  }

}

module.exports = new ExercisesController();
