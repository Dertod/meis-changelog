'use strict';

/**
 * Module dependencies.
 */
const Response = require('../../../config/lib/response');
const knex = require('../../../config/lib/knex').db;
const config = require('../../../config/config');
const _ = require('lodash');
const AdminProgramController = require('../admin/programs.controller');

class ProgramsController extends Response {
  async getFullTraining({params, user}) {
    let allSets = await knex('sets as s').select('s.id', 's.sets', 's.duration', 's.weight', 's.exercise_setting_id', 's.week_number',
      'es.duration_type_id', 'es.rest_time', 'es.characters_in_sets', 'es.exercise_id', 'es.day_number',
      'ped.program_id', 'es.is_warmup', 'es.tempo', 'es.order', 'es.resistance_type_id', 'es.resistance', 'es.description as description',
      'e.name as exercise_name', 'e.description as exercise_description', 'e.video_link', 'e.logo_image_url',
      knex.raw(`COALESCE(json_agg(json_build_object('equipment_id', ee.equipment_id,'equipment_name', ee.equipment_name)) FILTER (WHERE ee.equipment_id IS NOT NULL), '[]') as exercises_equipments`),
      knex.raw(`COALESCE(json_agg(json_build_object('muscle_id', emu.muscle_id, 'muscle_name', emu.muscle_name, 'muscle_group', emu.muscle_group, 'muscle_image', emu.muscle_image)) FILTER (WHERE emu.muscle_id IS NOT NULL), '[]') as exercises_muscles`)
    )
      .leftJoin('exercises_settings as es', 's.exercise_setting_id', 'es.id')
      .leftJoin('exercises as e', 'e.id', 'es.exercise_id')
      .leftJoin(knex.raw(`(SELECT * FROM "exercises_equipments" AS "er" 
                           LEFT JOIN "exercises_equipments_types" AS "et" ON "et"."id" = "er"."equipment_id")
                           AS "ee" ON "ee"."exercise_id" = "e"."id"`))
      .leftJoin(knex.raw(`(SELECT * FROM "exercises_muscles" AS "emr" 
                           LEFT JOIN "exercises_muscles_types" AS "emt" ON "emt"."id" = "emr"."muscle_id")
                           AS "emu" ON "emu"."exercise_id" = "e"."id"`))
      .leftJoin(`programs_exercises_days as ped`, 'ped.exercise_setting_id', 'es.id')
      .where('ped.program_id', params.programId)
      .groupBy('s.id', 'es.duration_type_id', 'es.rest_time', 'es.characters_in_sets', 'es.exercise_id',
        'es.day_number', 'ped.program_id', 'es.is_warmup', 'es.tempo', 'es.order', 'es.resistance_type_id',
        'es.resistance', 'es.description', 'e.name', 'e.description', 'e.video_link', 'e.logo_image_url')
      .orderBy('s.week_number')
      .orderBy('es.order');

    //Initialize helpful variables
    let training = {program: [], warmUps: []}, weeks = 0, days = 0, multiSet = {program: [], warmUps: []};
    let temporaryArray = [];

    if (allSets && allSets.length) {

      for (let i = 0; i < allSets.length; i++) {
        let arrayString = 'program';
        allSets[i].day_number -= 1;

        //Choose in which array should we store info
        if (allSets[i].is_warmup) {
          arrayString = 'warmUps';
          allSets[i].day_number = 0;
        }

        //Create arrays for days and weeks if they are not exists
        training[arrayString][allSets[i].week_number] = training[arrayString][allSets[i].week_number]
          ? training[arrayString][allSets[i].week_number]
          : [];
        training[arrayString][allSets[i].week_number][allSets[i].day_number] =
          training[arrayString][allSets[i].week_number][allSets[i].day_number]
            ? training[arrayString][allSets[i].week_number][allSets[i].day_number]
            : [];

        multiSet[arrayString].push(allSets[i]);

        //Check the next element in sets to create appropriate sequence
        if (!allSets[i + 1] || allSets[i + 1].characters_in_sets <= allSets[i].characters_in_sets) {
          let j = 0;
          let sets = allSets[i].sets;

          //Sort exercises by sequence
          do {
            if (multiSet[arrayString][j].sets !== 0) {
              let previousSet = _.findLast(temporaryArray, set => multiSet[arrayString][j].id === set.id);
              temporaryArray.push({
                ...multiSet[arrayString][j],
                setNumber: previousSet ? previousSet.setNumber + 1 : 1
              });

              //Add weight and reps through ','
              let weight = multiSet[arrayString][j].weight, duration = multiSet[arrayString][j].duration,
                weightArray = [], durationArray = [];
              if (weight && weight.includes(',')) {
                weightArray = weight.split(',');
                weight = weightArray.shift();
                multiSet[arrayString][j].weight = weightArray.join(',');
              }

              if (duration && duration.includes(',')) {
                durationArray = duration.split(',');
                duration = durationArray.shift();
                multiSet[arrayString][j].duration = durationArray.join(',');
              }

              training[arrayString][allSets[i].week_number][allSets[i].day_number].push({
                ...multiSet[arrayString][j],
                setNumber: previousSet ? previousSet.setNumber + 1 : 1,
                sets, duration, weight
              });
            }
            multiSet[arrayString][j].sets = multiSet[arrayString][j].sets - 1 <= 0 ? 0 : multiSet[arrayString][j].sets - 1;

            j + 1 === multiSet[arrayString].length ? j = 0 : j++;
          } while (!(j === 0 && multiSet[arrayString][multiSet[arrayString].length - 1].sets === 0 && multiSet[arrayString][j].sets === 0));

          multiSet[arrayString] = [];
        }

      }

      if (training.program && training.program.length) {
        weeks = training.program.length;

        if (training.program[0] && training.program[0].length) {
          days = training.program[0].length
        }
      }

      let [lastDay] = await this.getLastActiveDay({user});

      let currentDay = this.getForwardTrainingDay(days, weeks, lastDay);
      let nextDay = this.getForwardTrainingDay(days, weeks, currentDay);

      return {trainingArray: training.program, warmUps: training.warmUps, currentDay, nextDay};
    } else {
      return {
        data: {message: "This program doesn't exist"},
        code: 400
      };
    }
  }

  async getProgramById({params}) {
    return await AdminProgramController.getProgramById({params});
  }

  getForwardTrainingDay(daysLength, weeksLength, trainingDay) {
    if (!trainingDay) {
      return {day_number: 0, week_number: 0}
    }

    if (trainingDay.day_number + 1 >= daysLength) {
      if (trainingDay.week_number + 1 >= weeksLength) {
        return {day_number: null, week_number: null}
      }

      return {day_number: 0, week_number: trainingDay.week_number + 1}
    } else {
      return {day_number: trainingDay.day_number + 1, week_number: trainingDay.week_number}
    }
  }

  async getAllAthletesPrograms({user}) {
    let programs = await knex('programs as p').select('p.id', 'p.name as title', 'p.description', 'p.created_at', 'p.updated_at',
      knex.raw(`(select max(week_number) from sets
                   left join exercises_settings on exercises_settings.id = sets.exercise_setting_id
                   left join programs_exercises_days on programs_exercises_days.exercise_setting_id = exercises_settings.id
                   where programs_exercises_days.program_id = p.id) as duration`))
      .leftJoin('programs_athletes as pa', 'pa.program_id', 'p.id')
      .leftJoin('companies_athletes as ca', 'ca.id', 'pa.company_athlete_id')
      .leftJoin('users as u', 'u.id', 'ca.athlete_id')
      .where({'ca.athlete_id': user.id, 'u.user_role': 3});

    let progresses = await knex('days_feedback').select('program_id')
      .count('id as progress').min('created_at as start_date')
      .whereIn('program_id', programs.map(program => program.id))
      .andWhere({athlete_id: user.id})
      .groupBy('program_id');

    programs.forEach(program => {
      let foundProgress = progresses.find(progress => progress.program_id === program.id);
      program.progress = foundProgress ? foundProgress.progress : 0;
      program.start_date = foundProgress ? foundProgress.start_date : null;
    });

    return programs;
  }

  async getMyProgramProgress({user, params}) {
    let programsDays = await knex('exercises_settings as es').max('es.day_number')
      .leftJoin('programs_exercises_days as ped', 'ped.exercise_setting_id', 'es.id')
      .where({'ped.program_id': params.programId});

    let [progress] = await knex('days_feedback').count('id as progress').where('program_id', params.programId)
      .andWhere({athlete_id: user.id});

    let [lastDay] = await knex('days_feedback').select('day_number', 'week_number').where('program_id', params.programId)
      .andWhere({athlete_id: user.id}).orderBy('created_at', 'desc');

    let weeksDaysArray = await knex('exercises_settings as es').select('es.day_number', 's.week_number')
      .leftJoin('sets as s', 's.exercise_setting_id', 'es.id')
      .leftJoin('programs_exercises_days as ped', 'ped.exercise_setting_id', 'es.id')
      .whereNotNull('es.day_number')
      .andWhere({'ped.program_id': params.programId})
      .groupBy('es.day_number', 's.week_number')
      .orderBy('s.week_number');

    return {
      program_progress: progress.progress,
      program_duration: programsDays.length,
      progress_percent: (progress.progress / programsDays.length * 100).toFixed(2),
      day_number: !!lastDay ? lastDay.day_number : null,
      week_number: !!lastDay ? lastDay.week_number : null,
      weeksDaysArray
    };
  }

  async getPreviousResults({params}) {
    let generatedObject = {};

    let previousResults = await knex('exercises_results_sets as ers')
      .select('er.exercise_id', 'ers.weight', 'ers.duration')
      .whereIn('exercise_id', function () {
        this.select('exercise_id').from('exercises_settings')
          .where({'program_id': params.programId})
          .andWhere({'day_number': params.dayNumber})
      })
      .leftJoin('exercises_results as er', 'ers.exercise_result_id', 'er.id')
      .groupBy('er.exercise_id', 'ers.weight', 'ers.duration');

    previousResults.forEach(e => generatedObject[e.exercise_id] = {
      weight: e.weight,
      duration: e.duration,
    });

    return generatedObject;
  }

  async getLastActiveDay({user}) {
    return await knex('days_feedback as df').select('*')
      .where({'df.athlete_id': user.id, 'ca.athlete_id': user.id})
      .leftJoin('programs_athletes as pa', 'df.program_id', 'pa.program_id')
      .leftJoin('companies_athletes as ca', 'pa.company_athlete_id', 'ca.id')
      .orderBy('created_at', 'desc')
      .limit(1);
  }
}

module.exports = new ProgramsController();
