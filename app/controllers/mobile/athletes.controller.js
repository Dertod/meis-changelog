
const Response = require('../../../config/lib/response');
const knex = require('../../../config/lib/knex').db;
const _ = require('lodash');
const config = require('../../../config/config');

const exerciseResultsBody = ['program_id', 'time_elapsed_sec', 'rest_time', 'athlete_status_id', 'day_number', 'week_number', 'day_feedback_id', 'order', 'exercise_id', 'characters_in_sets'];
const setsResultsBody = ['duration', 'duration_measurement_type_id', 'time_elapsed_sec', 'rest_time', 'weight', 'execution_order'];
/**
 * Methods for the athletes and their statistics
 */
class AthletesController extends Response {
  async saveSetResults({body, user}) {
    let exercisesResults = _.pick(body.exercise, exerciseResultsBody);

    let [exercisesIds] = await knex('exercises_results').insert({
      ...exercisesResults,
      athlete_id: user.id
    }).returning('id');

    let setsArray = body.sets.map((result) => {
      return {..._.pick(result, setsResultsBody), exercise_result_id: exercisesIds}
    });

    let setsIds = await knex('exercises_results_sets').insert(setsArray).returning('id');

    return {
      message: 'Exercises results were successfully saved',
      exerciseResultId: exercisesIds,
      exerciseResultsSetsId: setsIds
    }
  }

  async saveFeedback({body, user}) {
    let resultsBody = _.pick(body, ['program_id', 'rating', 'message', 'athlete_status_id', 'day_number', 'week_number']);

    let [feedbackId] = await knex('days_feedback').insert({...resultsBody, athlete_id: user.id}).returning('id');

    return {message: 'Feedback was successfully saved', id: feedbackId}
  }

  async getTrainingStatistics({user, params}) {
    let [trainingDuration] = await knex('exercises_results as er').sum('er.time_elapsed_sec as working_time').sum('er.rest_time as resting_time')
      .select('er.day_number', 'er.created_at', 'er.week_number')
      .where({'er.athlete_id': user.id, 'er.program_id': params.programId})
      .groupBy('er.created_at', 'er.day_number', 'er.week_number')
      .orderBy('er.week_number', 'desc')
      .orderBy('er.day_number', 'desc');

    let [liftedWeight] = await knex('exercises_results as er').sum('ers.weight as lifted_weight')
      .leftJoin('exercises_results_sets as ers', 'ers.exercise_result_id', 'er.id')
      .where({'er.athlete_id': user.id, 'er.program_id': params.programId});

    return {
      ...trainingDuration, ...liftedWeight,
      day_number: trainingDuration ? trainingDuration.day_number : null,
      week_number: trainingDuration ? trainingDuration.week_number : null
    };
  }

  async getCoaches({user}) {
    let coachesList = await knex('companies_athletes as cat')
      .select('cad.admin_id as id')
      .leftJoin('companies_admins as cad', 'cat.company_id', 'cad.company_id')
      .where({
        'cat.athlete_id': user.id,
        'cat.company_athlete_relationship_type_id': 3 /* INDIVIDUAL */,
        'cad.company_member_type': 1 /* OWNER */
      });

    return coachesList;
  }
}

module.exports = new AthletesController();
