'use strict';

const Response = require('../../config/lib/response');
const knex = require('../../config/lib/knex').db;

/**
 * Methods of the core
 */
class CoreController extends Response {
  getToken(authorization = '') {
    let parts = authorization.split(' ');

    if (parts.length !== 2) {
      throw Error('Format isn\'t Authorization: Bearer [token]');
    }

    let scheme = parts[0];
    let credentials = parts[1];

    if (/^Bearer$/i.test(scheme)) {
      return credentials;
    } else {
      throw Error('Format isn\'t Authorization: Bearer [token]');
    }
  }

  statusOK() {
    return {status: 'OK'};
  }
}

module.exports = new CoreController();
