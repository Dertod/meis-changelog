var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  knex = require(path.resolve('config/lib/knex')).db,
  express = require(path.resolve('config/lib/express'));

var adminToken, agent, athleteResult, companyId, metadataId;
var athleteBody = {
  name: 'Testiano Ronaldo',
  email: 'testiano_ronaldo@meisterfit.com',
  metadata: [
    {key: 'team', value: 'Khust bears'}
  ]
};

/**
 * Athletes routes tests
 */
describe('Athletes tests', function () {
  before(function (done) {
    // Get application
    agent = request.agent(express.init());
    done();
  });

  before(function (done) {
    agent.post('/auth/signin')
      .send({
        email: 'denis@compomatic.com',
        password: '123',
        user_type: 'COACH'
      })
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        const data = JSON.parse(result.text);

        adminToken = data.token;

        should(data).have.keys('token', 'ttl', 'company','twoFactorAuth');

        return done();
      });
  });

  before(function (done) {
    knex('companies').select('id').where({name: 'System'}).then((company) => {
      companyId = company[0].id;

      return done();
    })
  });

  it('Should create a new athlete', function (done) {
    agent.post(`/companies/${companyId}/athletes`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send(athleteBody)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        athleteResult = result.body;
        should(result.body).have.property('email', athleteBody.email);

        return done();
      });
  });

  it('Should get user\'s athletes', function (done) {
    agent.get(`/companies/${companyId}/athletes`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).be.an.Array;
        should(result.body[0]).have.property('email', athleteBody.email);

        return done();
      })
  });

  it('Should get an athlete by id', function (done) {
    agent.get(`/companies/${companyId}/athletes/${athleteResult.id}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        metadataId = result.body.metadata[0].id;
        should(result.body).have.property('email', athleteBody.email);

        return done();
      })
  });

  it('Should edit an athlete by id', function (done) {
    var updateAthleteBody = {
      name: 'Testinell Messi',
      email: 'testinell_messi@meisterfit.com',
      weight: 90,
      height: 180,
      age: 21
    };

    agent.put(`/companies/${companyId}/athletes/${athleteResult.id}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send(updateAthleteBody)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('email', updateAthleteBody.email);

        return done();
      })
  });

  it('Should get all athletes recent activity', function (done) {
    agent.get(`/companies/${companyId}/athletes/${athleteResult.id}/activity`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).be.an.Array;

        return done();
      })
  });

  after(function (done) {
    agent.del(`/companies/${companyId}/athletes/${athleteResult.id}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('athleteId');

        return done();
      });
  });

  after(function (done) {
    agent.del(`/companies/${companyId}/metadata/${metadataId}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).be.an.Array;

        return done();
      });
  });

  after(function (done) {
    knex('messages').delete().where('to_id', athleteResult.id).then(() => done())
  });

  after(function (done) {
    knex('users').delete().where('id', athleteResult.id).then(() => done());
  });

});
