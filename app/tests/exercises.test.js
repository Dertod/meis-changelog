var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  moment = require('moment'),
  _ = require('lodash'),
  knex = require(path.resolve('config/lib/knex')).db,
  express = require(path.resolve('config/lib/express'));

var adminToken, agent, exerciseId, metadataId, companyId;
var exerciseBody = {
  name: 'Bench dip',
  description: 'Bench dips are a basic bodyweight exercise that strengthen the triceps and shoulders.',
  logo_image_url: 'http://dipstation.org/wp-content/uploads/2016/01/bench-dips-300x225.jpg',
  metadata: [
    {key: 'bodypart', value: 'shoulders'}
  ]
};

/**
 * Companies routes tests
 */
describe('Exercises tests', function () {
  before(function (done) {
    // Get application
    agent = request.agent(express.init());
    done();
  });

  before(function (done) {
    agent.post('/auth/signin')
      .send({
        email: 'denis@compomatic.com',
        password: '123',
        user_type: 'COACH'
      })
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        const data = JSON.parse(result.text);

        adminToken = data.token;

        should(data).have.keys('token', 'ttl', 'company','twoFactorAuth');

        return done();
      });
  });

  before(function (done) {
    knex('companies').select('id').where({name: 'System'}).then((company) => {
      companyId = company[0].id;

      return done();
    })
  });

  it('Should create new exercise', function (done) {
    agent.post(`/exercises`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send(exerciseBody)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        exerciseId = result.body;
        should(result.body).be.an.Array;

        return done();
      });
  });

  it('Should get one exercise by id', function (done) {
    agent.get(`/exercises/${exerciseId[0]}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        metadataId = result.body.metadata[0].id;
        should(result.body).have.property('creator_name');

        return done();
      });
  });

  it('Should get all exercises', function (done) {
    agent.get(`/exercises`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).be.an.Array;
        should(result.body[0]).have.property('name');

        return done();
      });
  });


  it('Should edit one exercise', function (done) {
    var updatedBody = {
      name: 'STANDING CALF RAISE',
      description: 'The standing calf raise develops size and strength in the lower leg. This move also increases strength and range of motion in the ankle.',
      logo_image_url: 'http://weighttraining.guide/wp-content/uploads/2016/10/Lever-Standing-Calf-Raise-990x711.png'
    };

    agent.put(`/exercises/${exerciseId[0]}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send(updatedBody)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).be.an.Array;

        return done();
      });
  });

  after(function (done) {
    agent.del(`/companies/${companyId}/metadata/${metadataId}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).be.an.Array;

        return done();
      });
  });

  after(function (done) {
    agent.del(`/exercises/${exerciseId[0]}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('id');
        return done();
      });
  });
});
