var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  moment = require('moment'),
  _ = require('lodash'),
  knex = require(path.resolve('config/lib/knex')).db,
  express = require(path.resolve('config/lib/express')),
  uuid = require('uuid');

var adminToken, agent, programResult, programExercises, athletesId, detachAthletesId, exercisesId, programClone,
  companyId, variationId, variationId2, variationId3, variationId4, variationId5, programSavedExercises;

var programBody = {
  title: 'Program-1-test-by-denis',
  description: 'Testing program to check appropriate controller\'s work'
};

var newExercise = {
  duration_type_id: 1,
  rest_time: 200,
  characters_in_sets: 0,
  day_number: 0,
  tempo: '2-0-1',
  description: 'Last A exercise',
  resistance_type_id: 1,
  resistance: 12,
  order: 2,
  sets: [
    {
      sets: 3,
      duration: 10,
      weight: 0,
      week_number: 0
    },
    {
      sets: 3,
      duration: 10,
      weight: 0,
      week_number: 1
    }
  ]
};

var programBodyAutosaved = {
  title: 'Program-1-test-by-denis-autosaved',
  description: 'Testing autosaing program',
  exercises: [
    {
      duration_type_id: 1,
      rest_time: 60,
      characters_in_sets: 0,
      day_number: 0,
      tempo: '2-0-1',
      description: 'First A exercise',
      resistance_type_id: 1,
      resistance: 12,
      order: 2,
      sets: [
        {
          sets: 3,
          duration: 10,
          weight: 0,
          week_number: 0
        },
        {
          sets: 3,
          duration: 10,
          weight: 0,
          week_number: 1
        }
      ]
    },
    {
      duration_type_id: 1,
      rest_time: 60,
      characters_in_sets: 1,
      day_number: 0,
      tempo: '2-0-1',
      description: 'First B exercise',
      resistance_type_id: 1,
      resistance: 12,
      order: 3,
      sets: [
        {
          sets: 3,
          duration: 10,
          weight: 0,
          week_number: 0
        },
        {
          sets: 3,
          duration: 10,
          weight: 0,
          week_number: 1
        }
      ]
    },
    {
      duration_type_id: 1,
      characters_in_sets: 0,
      day_number: 1,
      description: 'Second Day First A',
      resistance_type_id: 2,
      resistance: 22,
      order: 5,
      sets: [
        {
          sets: 3,
          duration: 12,
          weight: 10,
          week_number: 0
        },
        {
          sets: 3,
          duration: 12,
          weight: 10,
          week_number: 1
        }
      ]
    },
    {
      duration_type_id: 1,
      characters_in_sets: 0,
      day_number: 1,
      description: 'Second Day First A Again',
      resistance_type_id: 2,
      resistance: 22,
      order: 6,
      sets: [
        {
          sets: 2,
          duration: 12,
          weight: 10,
          week_number: 0
        },
        {
          sets: 3,
          duration: 12,
          weight: 10,
          week_number: 1
        }
      ]
    },
    {
      duration_type_id: 1,
      characters_in_sets: 1,
      day_number: 1,
      description: 'Second Day First B',
      resistance_type_id: 2,
      resistance: 22,
      order: 7,
      sets: [
        {
          sets: 2,
          duration: 12,
          weight: 10,
          week_number: 0
        },
        {
          sets: 3,
          duration: 12,
          weight: 10,
          week_number: 1
        }
      ]
    }
  ]
};

var updateProgramBody = {
  title: 'Updated-test-program',
  description: 'Edited program that shows possibility of program updating',
};
var emailToAssign = {emails: ['Nobunaga@test.ru', 'Ceasar@test.net', 'putin@test.ru', 'Chingishan@test.net', 'Napoleon@test.net', 'Gitler@test.net']};

/**
 * Programs routes tests
 */
describe('Programs tests', function () {
  before(function (done) {
    // Get application
    agent = request.agent(express.init());
    done();
  });
  before(function (done) {
    knex('users').delete().whereIn('email', emailToAssign.emails).andWhere('user_role', 3).then(() => done());
  });
  before(function (done) {
    knex('exercises').select('id').limit(3).then(ids => {
      exercisesId = ids;

      return done();
    })
  });

  before(function (done) {
    agent.post('/auth/signin')
      .send({
        email: 'denis@compomatic.com',
        password: '123',
        user_type: 'COACH'
      })
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        const data = JSON.parse(result.text);

        adminToken = data.token;

        should(data).have.keys('token', 'ttl', 'company', 'twoFactorAuth');

        return done();
      });
  });

  before(function (done) {
    knex('companies').select('id').where({name: 'System'}).then((company) => {
      companyId = company[0].id;

      return done();
    })
  });

  it('Should create new program in draft', function (done) {
    agent.post(`/companies/${companyId}/programs`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send(programBody)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        programResult = result.body;
        should(result.body).have.properties('message');

        return done();
      });
  });

  it('Should get all programs for coach', function (done) {
    agent.get(`/companies/${companyId}/programs/`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).be.an.Array;

        return done();
      })
  });

  it('Should get a program by id', function (done) {
    agent.get(`/companies/${companyId}/programs/${programResult.id}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('title', programBody.title);
        should(result.body).have.property('exercises');
        should(result.body.exercises).be.an.Array;

        programExercises = result.body.exercises;

        return done();
      })
  });

  it('Should autosave a program by id', function (done) {

    programBodyAutosaved.exercises[0].exercise_id = exercisesId[0].id;
    programBodyAutosaved.exercises[1].exercise_id = exercisesId[1].id;

    agent.post(`/companies/${companyId}/programs/${programResult.id}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send(programBodyAutosaved)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('id', programResult.id);

        return done();
      })
  });

  it('Should edit a program by id', function (done) {

    programBodyAutosaved.exercises[0].exercise_id = exercisesId[0].id;
    programBodyAutosaved.exercises[1].exercise_id = exercisesId[1].id;
    programBodyAutosaved.exercises[2].exercise_id = exercisesId[2].id;
    programBodyAutosaved.exercises[3].exercise_id = exercisesId[0].id;
    programBodyAutosaved.exercises[4].exercise_id = exercisesId[1].id;

    agent.put(`/companies/${companyId}/programs/${programResult.id}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send(programBodyAutosaved)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('id', programResult.id);

        return done();
      })
  });

  it('Should clone a program by id', function (done) {

    agent.post(`/companies/${companyId}/programs/${programResult.id}/clone`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send({})
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        programClone = result.body;
        should(result.body).have.property('id');

        return done();
      })
  });

  it('Should create variation of a program by id', function (done) {

    agent.post(`/companies/${companyId}/programs/${programResult.id}/variation`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send({})
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body.program).have.property('id');
        variationId = result.body.program.id;

        return done();
      })
  });

  it('Should create variation 2 of a program by id', function (done) {

    agent.post(`/companies/${companyId}/programs/${programResult.id}/variation`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send({})
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body.program).have.property('id');
        variationId2 = result.body.program.id;

        return done();
      })
  });

  it('Should create variation 3 of a program by id', function (done) {

    agent.post(`/companies/${companyId}/programs/${variationId}/variation`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send({})
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body.program).have.property('id');
        variationId3 = result.body.program.id;

        return done();
      })
  });

  it('Should create variation 4 of a program by id', function (done) {

    agent.post(`/companies/${companyId}/programs/${variationId3}/variation`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send({})
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body.program).have.property('id');
        variationId4 = result.body.program.id;

        return done();
      })
  });

  it('Should create variation 5 of a program by id', function (done) {

    agent.post(`/companies/${companyId}/programs/${variationId3}/variation`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send({})
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body.program).have.property('id');
        variationId5 = result.body.program.id;

        return done();
      })
  });

  it('Should get a program by id', function (done) {
    agent.get(`/companies/${companyId}/programs/${programResult.id}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        programSavedExercises = result.body.exercises;

        return done();
      })
  });

  it('Should edit a program by id', function (done) {

    updateProgramBody.deleted = programSavedExercises[3].id;
    updateProgramBody.exercises = [];
    updateProgramBody.exercises[0] = programSavedExercises[0];
    updateProgramBody.exercises[1] = programSavedExercises[1];
    updateProgramBody.exercises[2] = programSavedExercises[2];
    updateProgramBody.exercises[3] = programSavedExercises[4];
    updateProgramBody.exercises[0].rest_time = 1000;
    updateProgramBody.exercises[1].sets[0].weight = 1000;
    newExercise.exercise_id = exercisesId[2].id;
    updateProgramBody.exercises.push(newExercise);

    agent.put(`/companies/${companyId}/programs/${variationId}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send(updateProgramBody)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('id', variationId);

        return done();
      })
  });

  it('Should get all programs for coach', function (done) {
    agent.get(`/companies/${companyId}/programs/`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }


        should(result.body).be.an.Array;

        return done();
      })
  });

  it('Should assign team or athlete to program', function (done) {
    knex('users').insert([
      {
        name: 'Test-athlete-1',
        email: 'Nobunaga@test.ru',
        hash_password: knex.raw(`crypt('${encodeURIComponent('123')}', gen_salt(\'md5\'))`),
        user_role: 3
      },
      {
        name: 'Test-athlete-2',
        email: 'Ceasar@test.net',
        hash_password: knex.raw(`crypt('${encodeURIComponent('456')}', gen_salt(\'md5\'))`),
        user_role: 3,
      },
      {
        name: 'Test-athlete-3',
        email: 'putin@test.ru',
        hash_password: knex.raw(`crypt('${encodeURIComponent('789')}', gen_salt(\'md5\'))`),
        user_role: 3,
      },
      {
        name: 'Test-athlete-4',
        email: 'Chingishan@test.net',
        hash_password: knex.raw(`crypt('${encodeURIComponent('123')}', gen_salt(\'md5\'))`),
        user_role: 3
      },
      {
        name: 'Test-athlete-5',
        email: 'Napoleon@test.net',
        hash_password: knex.raw(`crypt('${encodeURIComponent('456')}', gen_salt(\'md5\'))`),
        user_role: 3,
      },
      {
        name: 'Test-athlete-6',
        email: 'Gitler@test.net',
        hash_password: knex.raw(`crypt('${encodeURIComponent('789')}', gen_salt(\'md5\'))`),
        user_role: 3,
      },
    ], 'id').then((athleteId) => {
      agent.post(`/companies/${companyId}/programs/${programResult.id}/assign`)
        .set('Authorization', `Bearer ${adminToken}`)
        .send(emailToAssign)
        .expect(200)
        .end(function (err, result) {
          if (err) {
            return done(err);
          }
          athletesId = athleteId;
          should(result.body).have.properties('athletesIds', 'program');

          return done();
        })
    });
  });

  it('Should get my program progress', function (done) {
    knex('users').select('id').where({email: 'Nobunaga@test.ru', user_role: 3}).then((user) => {
      knex('days_feedback').insert({
        athlete_id: user[0].id,
        program_id: programResult.id,
        week_number: 1,
        day_number: 1
      }).then(() => {
        agent.get(`/programs/${programResult.id}/progress`)
          .set('Authorization', `Bearer ${adminToken}`)
          .expect(200)
          .end(function (err, result) {
            if (err) {
              return done(err);
            }

            should(result.body).have.property('progress_percent', (result.body.program_progress / result.body.program_duration * 100).toFixed(2));

            return done();
          });
      })
    })
  });

  it('Should save set results', function (done) {
    var resultBody = {
      exercise: {
        program_id: programResult.id,
        time_elapsed_sec: 40,
        rest_time: 300,
        athlete_status_id: 3,
        day_number: 1,
        week_number: 1,
        order: 0,
        exercise_id: exercisesId[0].id
      },
      sets: [
        {
          duration: 10,
          duration_measurement_type_id: 1,
          weight: 20
        },
        {
          duration: 12,
          duration_measurement_type_id: 1,
          weight: 30
        },
        {
          duration: 14,
          duration_measurement_type_id: 1,
          weight: 15
        }
      ]
    };
    agent.post(`/athletes/me/statistics`)
      .set('Authorization', `Bearer ${adminToken}`)
      .send(resultBody)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('exerciseResultId');
        should(result.body).have.property('exerciseResultsSetsId');

        return done();
      })
  });

  it('Should get training statistics', function (done) {
    agent.get(`/athletes/me/programs/${programResult.id}/statistics`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }
        should(result.body).have.property('lifted_weight');
        return done();
      });
  });

  it('Should get previous results info', function (done) {
    agent.get(`/programs/${programResult.id}/day/1`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).be.an.Object;
        should(result.body[Object.keys(result.body)[0]]).have.property('weight');

        return done();
      });
  });

  it('Should get athletes for dashboard', function (done) {
    agent.get(`/companies/${companyId}/dashboard`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).be.an.Array;
        should(result.body[0]).have.property('name', 'Test-athlete-1');

        return done();
      })
  });

  it('Should detach program from athlete', function (done) {
    detachAthletesId = athletesId[3];
    agent.del(`/companies/${companyId}/athletes/${detachAthletesId}/programs/${programResult.id}/attach`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }
        should(result.body).have.property('athleteId', detachAthletesId);

        return done();
      });
  });

  it('Should get all athletes programs', function (done) {
    agent.get(`/companies/${companyId}/programs/${programResult.id}/athletes`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }
        should(result.body).be.an.Array;
        should(result.body[0]).have.property('id', detachAthletesId);

        return done();
      });
  });


  after(function (done) {
    agent.del(`/companies/${companyId}/programs/${programResult.id}/assign`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).be.an.Array;
        should(result.body[0]).have.property('athlete_id');

        return done();
      });
  });

  after(function (done) {
    agent.del(`/companies/${companyId}/programs/${programClone.id}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('message');
        return done();
      });
  });

  after(function (done) {
    agent.del(`/companies/${companyId}/programs/${variationId5}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('message');
        return done();
      });
  });

  after(function (done) {
    agent.del(`/companies/${companyId}/programs/${variationId4}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('message');
        return done();
      });
  });

  after(function (done) {
    agent.del(`/companies/${companyId}/programs/${variationId3}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('message');
        return done();
      });
  });

  after(function (done) {
    agent.del(`/companies/${companyId}/programs/${variationId2}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('message');
        return done();
      });
  });

  after(function (done) {
    agent.del(`/companies/${companyId}/programs/${variationId}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('message');
        return done();
      });
  });

  after(function (done) {
    agent.del(`/companies/${companyId}/programs/${programResult.id}`)
      .set('Authorization', `Bearer ${adminToken}`)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }

        should(result.body).have.property('message');
        return done();
      });
  });

  after(function (done) {
    knex('messages').delete().whereIn('to_id', athletesId).then(() => done())
  });

  after(function (done) {
    knex('users').delete().whereIn('id', athletesId).then(() => done());
  });

  after(function (done) {
    knex('exercises_settings')
      .delete()
      .whereIn('id',
        knex('programs_exercises_days')
          .select('exercise_setting_id')
          .whereIn('program_id', [programResult.id, variationId, variationId2, variationId3, variationId4, variationId5, programClone.id]))
      .then(() => done())
  });

  after(function (done) {
    knex('programs').delete().where('id', programClone.id).then(() => done())
  });

  after(function (done) {
    knex('programs').delete().where('main_program_id', programResult.id).then(() => done())
  });

  after(function (done) {
    knex('programs').delete().where('id', programResult.id).then(() => done())
  });
});
