var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  knex = require(path.resolve('config/lib/knex')).db,
  express = require(path.resolve('config/lib/express')),
  uuid = require('uuid');

var agent, userToken, userResult;
var userBody = {
  name: 'TEST-MEISTERFIT-USER',
  email: 'yaroslav@mfit.com',
  password: '123',
  user_type: 'COACH'
};
/**
 * Users routes tests
 */
describe('Users tests', function () {
  before(function (done) {
    // Get application
    agent = request.agent(express.init());
    done();
  });

  it('Should signup', function (done) {
    agent.post(`/auth/signup`)
      .send(userBody)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }
        const data = result.body;

        userToken = data.token;

        should(data).have.keys('token', 'ttl', 'company');

        return done();
      });
  });

  it('Should get user profile', function (done) {
    knex('users').select('id').where({email: userBody.email, user_role: 2}).then((userId) => {
      agent.get(`/users/me`)
        .set('Authorization', `Bearer ${userToken}`)
        .expect(200)
        .end(function (err, result) {
          if (err) {
            return done(err);
          }
          userResult = result.body;
          userResult.id = userId[0].id;
          should(userResult).have.property('email', userBody.email);
          return done();
        })
    });
  });

  it('Should edit an user profile', function (done) {
    var updateUserBody = {
      name: 'UPDATE-MEISTERFIT-USER',
      email: 'yaroslav-test@meisterfit.com',
      weight: 100,
      height: 200,
      age: 30
    };

    agent.post(`/users/me`)
      .set('Authorization', `Bearer ${userToken}`)
      .send(updateUserBody)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }
        userToken = result.body.authData.token;

        should(result.body).have.keys('profile', 'authData');
        return done();
      })
  });

  it('Should edit an user password', function (done) {
    var passwordUser = {
      current_password: '123',
      new_password: '12345'
    };

    agent.post(`/users/me/password`)
      .set('Authorization', `Bearer ${userToken}`)
      .send(passwordUser)
      .expect(200)
      .end(function (err, result) {
        if (err) {
          return done(err);
        }
        userToken = result.body.token;

        should(result.body).have.keys('token', 'ttl', 'company');
        return done();
      })
  });
    after(function (done) {
   knex('companies').delete().where('id', userResult.company.id).then(() => done())
   });
    after(function (done) {
   knex('emails_tokens').delete().where('other_id',  userResult.id).then(() => done())
   });
    after(function (done) {
   knex('users').delete().where('id', userResult.id).then(() => done());
   });
});
