'use strict';

const controller = require('../controllers/core.controller');

module.exports = (app) => {
  app.route('/health')
    .get(controller.method('statusOK'));
};