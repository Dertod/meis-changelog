'use strict';

const controller = require('../controllers/exercises.controller');
const validation = require('../validations/exercises.validation');

module.exports = (app) => {
  app.route('/exercises')
    .get(controller.method('getAllExercises'))
    .post(validation.method('createExercise'), controller.method('createExercise'))
    .put(controller.method('createBulkExercises'));

  app.route('/exercises/:exerciseId')
    .get(controller.method('getExerciseById'))
    .put(validation.method('editExercise'), controller.method('editExerciseById'))
    .delete(controller.method('deleteExerciseById'));

  app.route('/categories')
    .get(controller.method('getExercisesCategories'));


};
