'use strict';

const controller = require('../controllers/chat.controller');
const validation = require('../validations/chat.validation');


module.exports = (app) => {
  app.route('/messages/direct')
    .get(validation.method('getLastDirectMessages'), controller.method('getLastDirectMessages'));

  app.route('/companies/:companyId/athletes/:athleteId/messages')
    .get(controller.method('getUserCoachMessages'));
};
