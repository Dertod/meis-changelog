'use strict';

const controller = require('../../controllers/admin/metadata.controller');
const companyValidation = require('../../validations/companies.validation');

module.exports = (app) => {
  app.param('companyId', companyValidation.method('validateCompanyId'));

  app.route('/companies/:companyId/metadata')
    .post(controller.method('createMetadata'))
    .get(controller.method('getAllMetadata'));

  app.route('/companies/:companyId/metadata/athletes')
    .get(controller.method('getAllAthletesMetadata'));

  app.route('/companies/:companyId/metadata/exercises')
    .get(controller.method('getAllExercisesMetadata'));

  app.route('/companies/:companyId/metadata/:metadataId')
    .delete(controller.method('deleteMetadata'));
};
