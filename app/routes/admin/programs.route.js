'use strict';

const controller = require('../../controllers/admin/programs.controller');
const validation = require('../../validations/programs.validation');
const companyValidation = require('../../validations/companies.validation');
//const policy = require('../policies/users.policy');


module.exports = (app) => {
  app.param('companyId', companyValidation.method('validateCompanyId'));
  app.param('programId', validation.method('validateProgramId'));

  app.route('/companies/:companyId/programs')
    .post(validation.method('createProgram'), controller.method('createProgram'))
    .get(controller.method('getAllPrograms'));

  app.route('/companies/:companyId/programs/:programId')
    .get(controller.method('getProgramById'))
    .put(validation.method('createProgram'), controller.method('editProgramById'))
    .post(controller.method('autosaveProgramById'))
    .delete(controller.method('deleteProgramById'));

  app.route('/companies/:companyId/programs/:programId/assign')
    .post(controller.method('assignProgramToAthletes'))
    .delete(controller.method('unAssignAllFromProgram'));

  app.route('/companies/:companyId/programs/:programId/detach')
    .delete(controller.method('detachAthletesFromProgram'));

  app.route('/companies/:companyId/programs/:programId/publish')
    .post(controller.method('publishProgram'));

  app.route('/companies/:companyId/programs/:programId/unpublish')
    .post(controller.method('unpublishProgram'));

  app.route('/companies/:companyId/programs/:programId/athletes')
    .get(controller.method('getProgramAthletes'));

  app.route('/companies/:companyId/programs/:programId/clone')
    .post(controller.method('cloneProgram'));

  app.route('/companies/:companyId/programs/:programId/variation')
    .post(controller.method('createVariation'));

  app.route('/companies/:companyId/programs/:programId/clean')
    .get(controller.method('removeDraftProgram'));
};
