'use strict';

const controller = require('../../controllers/admin/companies.controller');
const validation = require('../../validations/companies.validation');

module.exports = (app) => {
  app.param('companyId', validation.method('validateCompanyId'));

  app.route('/companies')
    .post(validation.method('create'), controller.method('createCompany'));

  app.route('/companies/:companyId')
    .get(controller.method('getCompanyCoaches'))
    .put(validation.method('update'), controller.method('editCompany'))
    .delete(controller.method('removeCompany'));

  app.route('/companies/:user/profile')
    .get(controller.method('getCompanyProfile'));

  app.route('/companies/:companyId/admins/:adminId')
    .post(controller.method('addAdminToCompany'))
    .delete(controller.method('deleteAdminFromCompany'));
};
