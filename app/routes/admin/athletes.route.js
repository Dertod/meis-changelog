'use strict';

const controller = require('../../controllers/admin/athletes.controller');
const validation = require('../../validations/athlete.validation');
const programValidation = require('../../validations/programs.validation');
const companyValidation = require('../../validations/companies.validation');

module.exports = (app) => {
  app.param('companyId', companyValidation.method('validateCompanyId'));
  app.param('athleteId', validation.method('validateAthleteId'));
  app.param('programId', programValidation.method('validateProgramId'));

  app.route('/companies/:companyId/athletes')
    .get(controller.method('getMyAthletes'))
    .post(validation.method('createAthlete'), controller.method('createAthlete'));

  app.route('/companies/:companyId/dashboard')
    .get(controller.method('getAthletesForDashboard'));

  app.route('/companies/:companyId/athletes/:athleteId')
    .get(controller.method('getAthleteById'))
    .put(validation.method('editAthlete'), controller.method('editAthlete'))
    .delete(controller.method('removeAthlete'));

  app.route('/companies/:companyId/athletes/:athleteId/programs/:programId/attach')
    .post(controller.method('attachProgramToAthlete'))
    .delete(controller.method('detachProgramFromAthlete'));

  app.route('/companies/:companyId/athletes/search/:term')
    .get(controller.method('searchAthletes'));

  app.route('/companies/:companyId/athletes/:athleteId/activity')
    .get(controller.method('getAthletesRecentActivity'));

  app.route('/companies/:companyId/athletes/:athleteId/progress')
    .get(controller.method('getAllAthleteProgress'));

  app.route('/companies/:companyId/athletes/:athleteId/programs')
    .get(controller.method('getAthletePrograms'));

  app.route('/companies/:companyId/athletes/:athleteId/metadata')
    .post(controller.method('addMetadataToAthlete'));

  app.route('/companies/:companyId/athletes/:athleteId/reinvite')
    .get(controller.method('reinviteAthlete'))
};
