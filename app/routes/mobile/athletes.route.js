'use strict';

const controller = require('../../controllers/mobile/athletes.controller');
const programValidation = require('../../validations/programs.validation');

module.exports = (app) => {
  app.param('programId', programValidation.method('validateProgramId'));

  app.route('/athletes/me/statistics')
    .post(controller.method('saveSetResults'));

  app.route('/athletes/me/programs/:programId/statistics')
    .get(controller.method('getTrainingStatistics'));

  app.route('/athletes/me/coaches')
    .get(controller.method('getCoaches'));

  app.route('/athletes/feedback')
    .post(controller.method('saveFeedback'));
};
