'use strict';

const controller = require('../../controllers/mobile/programs.controller');
const validation = require('../../validations/programs.validation');
//const policy = require('../policies/users.policy');


module.exports = (app) => {
  app.param('programId', validation.method('validateProgramId'));

  app.route('/programs/:programId/training')
    .get(controller.method('getFullTraining'));

  app.route('/programs/:programId')
    .get(controller.method('getProgramById'));

  app.route('/programs')
    .get(controller.method('getAllAthletesPrograms'));

  app.route('/programs/:programId/progress')
    .get(controller.method('getMyProgramProgress'));

  app.route('/programs/:programId/day/:dayNumber')
    .get(controller.method('getPreviousResults'));

  app.route('/programs/me/active')
    .get(controller.method('getLastActiveDay'));
};
