'use strict';

const controller = require('../controllers/tickets.controller');

module.exports = (app) => {
  app.route('/tickets')
    .post(controller.method('createTicket'))
    .get(controller.method('getAllTickets'));

  app.route('/tickets/:ticketId')
    .get(controller.method('getTicketById'))
    .delete(controller.method('deleteTicketById'));

  app.route('/tickets/:ticketId/comment')
    .post(controller.method('createComment'))
    .get(controller.method('getCommentsById'));

  app.route('/tickets/:ticketId/status')
    .post(controller.method('changeStatus'));

  app.route('/tickets/user/:id/likes')
    .get(controller.method('getUserVotesById'));

  app.route('/tickets/moderate/count')
    .get(controller.method('getPendingTicketsCount'));

  app.route('/tickets/ticket/:ticketId/likes')
    .get(controller.method('getTicketVotesCountById'));

  app.route('/tickets/:user/vote/:ticketId')
    .get(controller.method('voteTicket'))
    .delete(controller.method('deleteTicketVote'));
}
