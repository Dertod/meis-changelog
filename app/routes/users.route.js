'use strict';

const multer = require('multer');

const controller = require('../controllers/users.controller');
const validation = require('../validations/users.validation');
const companyValidation = require('../validations/companies.validation');
//const policy = require('../policies/users.policy');


module.exports = (app) => {

  app.param('userId', validation.method('userId'));
  app.param('companyId', companyValidation.method('validateCompanyId'));

  app.route('/auth/signin')
    .post(validation.method('signin'), controller.method('signin'));

  app.route('/auth/signin/social')
    .get(controller.method('notEmailSignFacebook'))
    .post(validation.method('signinViaSocial'), controller.method('signinViaSocial'));

  app.route('/auth/signup')
    .post(validation.method('signup'), controller.method('signup'));

  app.route('/auth/email/confirm')
    .post(validation.method('confirmEmail'), controller.method('confirmEmail'));
  app.route('/companies/:companyId/coaches/invite')
    .post(controller.method('inviteCoachesToCompany'));

  app.route('/auth/password')
    .get(controller.method('statusAthletePassword'))
    .post(validation.method('handlePassword'), controller.method('createPassword'))
    .put(validation.method('handlePassword'), controller.method('updatePassword'));

  app.route('/auth/password/forgot')
    .post(validation.method('forgotPassword'), controller.method('forgotPassword'))
    .put(validation.method('checkRestoreToken'),controller.method('checkRestoreToken'));

  app.route('/auth/password/athlete')
    .post(validation.method('passwordLink'), controller.method('sendPasswordLinkForAthlete'));

  app.route('/auth/password/forgot/mobile')
    .post(validation.method('sendEmailWithCodeForRecoveringPasswordMobile'), controller.method('sendEmailWithCodeForRecoveringPasswordMobile'))
    .put(validation.method('checkCodeForRecoveringPasswordMobile'), controller.method('checkCodeForRecoveringPasswordMobile'));

  app.route('/coaches/search/:term')
    .get(controller.method('searchCoaches'));

  app.route('/users/me')
    .get(controller.method('userProfile'))
    .post(validation.method('update'), controller.method('updateUserProfile'));

  app.route('/users/me/password')
    .post(validation.method('changePassword'), controller.method('changeUserPassword'));

  app.route('/users/me/images')
    .post(multer({
      limits: {
        fieldSize: 10000,
        fileSize: 1024 * 1024
      }
    }).single('image'), controller.method('uploadImage'));

  app.route('/users/me/2fa')
    .get(controller.method('activate2FA'))
    .post(controller.method('verification2FA'))
    .delete(controller.method('deactivate2FA'));

  app.route('/auth/subscribe')
    .post(controller.method('subscribe'));

};
