const _ = require('lodash');

function trim(obj) {
  _.forEach(obj, (value, key) => {
    if (typeof obj[key] === 'string') {
      obj[key] = key === 'email' ? value.toLowerCase().trim() : value.trim();
    }
    if (typeof obj[key] === _.isObject) {
      trimData(value);
    }
  });
  return obj;
}

module.exports = trim;
