'use strict';

var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Reports Permissions
 */
exports.invokeRolesPolicies = () => {
  acl.allow([
    {
    roles: ['admin'],
    allows: [{
      resources: '/companies/:companyId/',
      permissions: ['put']
    }]
  },{
    roles: ['admin'],
    allows: [{
      resources: '/users/:userId/companies/',
      permissions: ['post']
    }]
  }
  ]);
};

/**
 * Check If Reports Policy Allows
 */
exports.isAllowed = (req, res, next) => {
  acl.areAnyRolesAllowed(req.user.user_role, req.route.path, req.method.toLowerCase(), (err, isAllowed) => {
    if (err) {
      return res.status(500).send({
        message: 'Unexpected authorization error'
      });
    }

    if (isAllowed) {
      return next();
    }

    return res.status(403).json({
      message: 'Access denied'
    });
  });
};
