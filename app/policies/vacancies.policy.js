'use strict';

var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Reports Permissions
 */
exports.invokeRolesPolicies = () => {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/companies/:companyId/vacancies',
      permissions: ['post']
    }]
  },{
    roles: ['admin'],
    allows: [{
      resources: '/vacancies/:vacancyId/',
      permissions: ['put']
    }]
  },{
    roles: ['admin'],
    allows: [{
      resources: '/vacancies/:vacancyId/',
      permissions: ['delete']
    }]
  }]);
};


/**
 * Check If Reports Policy Allows
 */
exports.isAllowed = (req, res, next) => {
  acl.areAnyRolesAllowed(req.user.user_role, req.route.path, req.method.toLowerCase(), (err, isAllowed) => {
    if (err) {
      return res.status(500).send({
        message: 'Unexpected authorization error'
      });
    }

    if (isAllowed) {
      return next();
    }

    return res.status(403).json({
      message: 'Access denied'
    });
  });
};

