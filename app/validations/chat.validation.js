'use strict';

const Joi = require('../../config/lib/joi');
const Response = require('../../config/lib/response');

class ChatValidation extends Response {

  async getLastDirectMessages({query}) {
    const querySchema = Joi.object().keys({
      user1: Joi.string().uuid(),
      user2: Joi.string().uuid()
    }).required();

    const {error} = Joi.validate(query, querySchema, {allowUnknown: false});

    return await this.getValidationError(error);
  }
}

module.exports = new ChatValidation();

