'use strict';
const _ = require('lodash');
const Joi = require('../../config/lib/joi');
const Response = require('../../config/lib/response');

class ExercisesValidation extends Response {
  async createExercise({body}) {
    const schema = Joi.object().keys({
      name: Joi.name()
    }).requiredKeys('name');

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    body.metadata = _.uniqWith(body.metadata, _.isEqual);

    return await this.getValidationError(error);
  }

  async editExercise({body, user}) {
    const schema = Joi.object().keys({
      name: Joi.name()
    }).requiredKeys('name');

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    body.metadata = _.uniqWith(body.metadata, _.isEqual);


    if (user.user_role !== 'admin' && body.is_moderated === true) {
      const customError = `Operation is denied for userId: ${user.id}`;
      return await this.getValidationError(customError, true);
    }

    return await this.getValidationError(error);
  }
}

module.exports = new ExercisesValidation();
