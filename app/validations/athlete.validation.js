'use strict';

const Joi = require('../../config/lib/joi');
const Response = require('../../config/lib/response');
const knex = require('../../config/lib/knex').db;
const validator = require('validator');
const moment = require('moment');
const _ = require('lodash');
const config = require('../../config/config');

class AthletesValidation extends Response {

  async createAthlete({body}) {
    const schema = Joi.object().keys({
      email: Joi.email(),
    }).requiredKeys('email');

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    body.metadata = _.uniqWith(body.metadata, _.isEqual);

    return await this.getValidationError(error);
  }

  async editAthlete({body}) {
    const schema = Joi.object().keys({
      first_name: Joi.name(),
      email: Joi.email(),
      profile_image_url: Joi.imageUrl(),
      age: Joi.age(),
      weight: Joi.weight(),
      height: Joi.height(),
    });

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    body.metadata = _.uniqWith(body.metadata, _.isEqual);

    return await this.getValidationError(error);
  }

  async validateAthleteId({params, user}) {
    const schema = Joi.object().keys({
      athleteId: Joi.athleteId()
    });

    const paramsError = Joi.validate(params, schema, {allowUnknown: true}).error;

    if (paramsError !== null) {
      return await this.getValidationError(paramsError);
    }

    let [athlete] = await knex('users as u')
      .select('*')
      .leftJoin('companies_athletes as ca', 'ca.athlete_id', 'u.id')
      .leftJoin('companies_admins as cad', 'cad.company_id', 'ca.company_id')
      .where({'u.id': params.athleteId})
      .andWhere({'cad.admin_id': user.id});

    let customError = null;
    if (!athlete) {
      customError = `Bad request for athleteId: ${params.athleteId}`;
    }

    return await this.getValidationError(customError, true);
  }

  async saveFeedback({body}) {
    const schema = Joi.object().keys({
      programId: Joi.programId()
    }).requiredKeys('programId');

    const paramsError = Joi.validate(body, schema, {allowUnknown: true}).error;

    if (paramsError !== null) {
      return await this.getValidationError(paramsError);
    }
  }
}

module.exports = new AthletesValidation();
