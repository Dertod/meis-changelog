'use strict';

const Joi = require('../../config/lib/joi');
const Response = require('../../config/lib/response');
const knex = require('../../config/lib/knex').db;
const validator = require('validator');
const moment = require('moment');
const _ = require('lodash');
const config = require('../../config/config');

class CompaniesValidation extends Response {

  async create({body,params}) {
    const schema = Joi.object().keys({
      name: Joi.name()
    }).requiredKeys('name');

    const bodyError = Joi.validate(body, schema, {allowUnknown: true}).error;


    if (bodyError !== null) {
      return await this.getValidationError(bodyError);
    }


    let customError = null;

    let users = await knex('users')
      .select('*')
      .where({
        id: params.userId,
        user_role: 1
      }).limit(1);

    if (!users.length) {
      customError = `Bad request for userId: ${params.adminId}`;
    }

    return await this.getValidationError(customError, true);
  }

  async update({body, params}) {
    const schema = Joi.object().keys({
      name: Joi.name()
    }).requiredKeys('name');

    const bodyError = Joi.validate(body, schema, {allowUnknown: true}).error;

    return await this.getValidationError(bodyError);

  }


  async validateCompanyId(req) {
    const {params, user} = req;

    const schema = Joi.object().keys({
      companyId: Joi.companyId()
    }).requiredKeys('companyId');

    const {error} = Joi.validate(params, schema, {allowUnknown: true});

    if (error !== null) {
      return await this.getValidationError(error);
    }


    let customError = null;

    let companies = await knex('companies as c')
      .select('c.id', 'c.name', 'c.logo_url', 'c.created_at', 'c.updated_at')
      .leftJoin('companies_admins as ca', 'c.id', 'ca.company_id')
      .where({
        'c.id': params.companyId,
        'ca.admin_id': user.id
      }).limit(1);

    if (!companies.length) {
      customError = `Bad request for companyId: ${params.companyId}`;
    }

    req.company = companies[0];

    return await this.getValidationError(customError, true);
  }
}

module.exports = new CompaniesValidation();

