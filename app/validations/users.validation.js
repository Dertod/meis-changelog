'use strict';

const Joi = require('../../config/lib/joi');
const Response = require('../../config/lib/response');
const knex = require('../../config/lib/knex').db;
const validator = require('validator');
const moment = require('moment');
const _ = require('lodash');
const config = require('../../config/config');


class UsersValidation extends Response {

  async signin({body}) {
    const schema = Joi.object().keys({
      email: Joi.email(),
      password: Joi.password(),
      user_type: Joi.string()
    }).requiredKeys('email', 'password', 'user_type');

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    if (error !== null) {
      return await this.getValidationError(error);
    }

    let userType = body.user_type.toLowerCase();
    if (_.isString(userType)) body.user_type = userType === 'coach' ? 2 : userType === 'admin' ? 1 : 3;

    return await this.getValidationError(error);
  }

  async signinViaSocial({body}) {
    const schema = Joi.object().keys({
      provider: Joi.string(),
      social_id: Joi.number(),
      profile: Joi.object(),
      user_type: Joi.string()
    }).requiredKeys('provider','social_id','profile', 'user_type');

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    if (error !== null) {
      return await this.getValidationError(error);
    }

    let userType = body.user_type.toLowerCase();
    if (_.isString(userType)) body.user_type = userType === 'coach' ? 2 : userType === 'admin' ? 1 : 3;

    return await this.getValidationError(error);
  }

  async signup({body}) {
    const schema = Joi.object().keys({
      email: Joi.email(),
      name: Joi.name(),
      password: Joi.password(),
      user_type: Joi.string(),
    }).requiredKeys('email', 'password', 'user_type', 'name');

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    if (error !== null) {
      return await this.getValidationError(error);
    }

    let userType = body.user_type.toLowerCase();
    if (_.isString(userType)) body.user_type = userType === 'coach' ? 2 : 3;

    return await this.getValidationError(error);
  }

  async confirmEmail({body}) {
    const schema = Joi.object().keys({
      token: Joi.token()
    }).requiredKeys('token');

    let {error} = Joi.validate(body, schema, {allowUnknown: true});

    if (error !== null) {
      error = 'No token provided';
    }

    return await this.getValidationError(error, true);
  }

  async createAdmin({body}) {
    const schema = Joi.object().keys({
      email: Joi.email(),
      name: Joi.name()
    }).requiredKeys('email', 'name');

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    return await this.getValidationError(error);
  }

  async handlePassword({body}) {
    const schema = Joi.object().keys({
      token: Joi.token(),
      password: Joi.password()
    }).requiredKeys('token', 'password');

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    return await this.getValidationError(error);
  }

  async checkRestoreToken({body}) {
    const schema = Joi.object().keys({
      token: Joi.token(),
    }).requiredKeys('token');

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    return await this.getValidationError(error);
  }

  async forgotPassword({body}) {
    const schema = Joi.object().keys({
      email: Joi.email()
    }).requiredKeys('email');

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    return await this.getValidationError(error);
  }

  async passwordLink({body}) {
    const schema = Joi.object().keys({
      email: Joi.string().email(),
    }).requiredKeys('email');

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    return await this.getValidationError(error);
  }

  async sendEmailWithCodeForRecoveringPasswordMobile({body}) {
    const schema = Joi.object().keys({
      email: Joi.email()
    }).requiredKeys('email');

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    return await this.getValidationError(error);
  }

  async checkCodeForRecoveringPasswordMobile({body}) {
    const schema = Joi.object().keys({
      email: Joi.email(),
      code: Joi.token(),
      password: Joi.password()
    }).requiredKeys('email', 'code', 'password');

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    return await this.getValidationError(error);
  }

  async update({body}) {
    const schema = Joi.object().keys({
      name: Joi.name(),
      email: Joi.email(),
      profile_image_url: Joi.imageUrl(),
    });

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    return await this.getValidationError(error);
  }

  async changePassword({body}) {
    const schema = Joi.object().keys({
      current_password: Joi.password(),
      new_password: Joi.password(),
      repeat_new_password: Joi.password()
    });

    const {error} = Joi.validate(body, schema, {allowUnknown: true});

    return await this.getValidationError(error);
  }

  async userId(req) {
    const {params} = req;

    const schema = Joi.object().keys({
      userId: Joi.userId()
    }).requiredKeys('userId');

    const {error} = Joi.validate(params, schema, {allowUnknown: true});

    if (error !== null) {
      return await this.getValidationError(error);
    }


    let customError = null;

    let users = await knex('users')
      .select('users.id as id', 'first_name', 'email', knex.raw('(lower(users_roles.title)) as user_role'))
      .where({'users.id': params.userId})
      .leftJoin('users_roles', 'users_roles.id', 'users.user_role')
      .limit(1);

    if (!users.length) {
      customError = `Bad request for userId: ${params.userId}`;
    }

    req.user = users[0];

    return await this.getValidationError(customError, true);
  }

}

module.exports = new UsersValidation();

