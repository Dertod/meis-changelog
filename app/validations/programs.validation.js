'use strict';

const Joi = require('../../config/lib/joi');
const Response = require('../../config/lib/response');
const knex = require('../../config/lib/knex').db;
const validator = require('validator');
const moment = require('moment');
const _ = require('lodash');
const config = require('../../config/config');

const uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;

class ProgramsValidation extends Response {
  async createProgram({body, user}) {
    const schema = Joi.object().keys({
      title: Joi.name()
    }).requiredKeys('title');

    const userSchema = Joi.object().keys({
      id: Joi.userId()
    }).requiredKeys('id');

    const bodyError = Joi.validate(body, schema, {allowUnknown: true}).error;
    const userError = Joi.validate(user, userSchema, {allowUnknown: true}).error;

    let errors = [].concat(
      //await this.validateExercises(body.exercises),
    );

    if (bodyError) {
      errors.push(...bodyError.details);
    }

    if (userError) {
      errors.push(...userError.details);
    }


    return await this.getValidationError(errors);
  }

  async validateProgramId(req) {
    const {params, user} = req;
    const schema = Joi.object().keys({
      programId: Joi.programId()
    });

    const paramsError = Joi.validate(params, schema, {allowUnknown: true}).error;

    if (paramsError !== null) {
      return await this.getValidationError(paramsError);
    }

    let existingProgram;
    let existingDraft;

    if (user.user_role !== 'athlete') {
      [existingProgram] = await knex('programs as p')
        .select('*')
        .leftJoin('companies_admins as ca', 'ca.company_id', 'p.company_owner_id')
        .where({'p.id': params.programId})
        .andWhere({'ca.admin_id': user.id});
      [existingDraft] = await knex('draft_programs as p')
        .select('*')
        .leftJoin('companies_admins as ca', 'ca.company_id', 'p.company_owner_id')
        .where({'p.id': params.programId})
        .andWhere({'ca.admin_id': user.id});


    } else {
      [existingProgram] = await knex('programs as p')
        .select('*')
        .leftJoin('programs_athletes as pa', 'pa.program_id', 'p.id')
        .leftJoin('companies_athletes as ca', 'ca.id', 'pa.company_athlete_id')
        .where({'p.id': params.programId})
        .andWhere({'ca.athlete_id': user.id});
    }

    req.params.draftExist = existingDraft ? true : false;
    req.params.mainExist = existingProgram ? true : false;

    if (user.user_role !== 'athlete' && !existingProgram && existingDraft) {
      existingProgram = existingDraft;
    }


    let customError = null;

    if (!existingProgram) {
      customError = `Bad request for programId: ${params.programId}`;
    } else {
      req.program = existingProgram;
    }

    return await this.getValidationError(customError, true);
  }

  validateWeeks(weeks) {
    const errors = [];
    if (!_.isArray(weeks)) {
      errors.push({
        param: 'weeks',
        msg: 'Must be array',
        value: weeks
      });
    }

    _.forEach(weeks, (w, i) => {
      if (!_.isString(w) && !uuidRegex.test(w)) {
        errors.push({
          param: `weeks[${i}]`,
          msg: 'Must be guid type',
          value: w
        });
      }
    });

    return errors;
  }

  validateDays(days) {
    const errors = [];
    if (!_.isArray(days)) {
      errors.push({
        param: 'days',
        msg: 'Must be array',
        value: days
      });
    }

    _.forEach(days, (d, i) => {
      if (!_.isString(d.week_id) && !uuidRegex.test(d.week_id)) {
        errors.push({
          param: `days[${i}].week_id`,
          msg: 'Must be guid type',
          value: d.week_id
        });
      }

      if (!_.isString(d.id) && !uuidRegex.test(d.id)) {
        errors.push({
          param: `days[${i}].id`,
          msg: 'Must be guid type',
          value: d.id
        });
      }
    });

    return errors;
  }

  async validateExercises(exercises) {
    const errors = [];
    if (!_.isArray(exercises)) {
      errors.push({
        param: 'exercises',
        msg: 'Must be array',
        value: exercises
      });
    }

    await Promise.all(exercises.map(async(e, i) => {
      if (!_.isString(e.day_id) && !uuidRegex.test(e.day_id)) {
        errors.push({
          param: `exercises[${i}].day_id`,
          msg: 'Must be guid type',
          value: e.day_id
        });
      }

      if (!_.isString(e.exercise_id) && !uuidRegex.test(e.exercise_id)) {
        errors.push({
          param: `exercises[${i}].exercise_id`,
          msg: 'Must be guid type',
          value: e.exercise_id
        });
      }

      if (_.isNull(e.set_number)) {
        errors.push({
          param: `exercises[${i}].set_number`,
          msg: 'Must not be a null',
          value: e.set_number
        });
      }

      if (_.isNull(e.session)) {
        errors.push({
          param: `exercises[${i}].session`,
          msg: 'Must not be a null value',
          value: e.session
        });
      }

      if (_.isNull(e.resistance) || (parseInt(e.resistance) < 0)) {
        errors.push({
          param: `exercises[${i}].resistance`,
          msg: 'Must be equal or more 0',
          value: e.resistance
        });
      }
      if (e.resistance_type_id && !(await knex('exercise_resistance_types').select('*').where({id: e.resistance_type_id})).length) {
        errors.push({
          param: `exercises[${i}].resistance_type_id`,
          msg: "Id doesn't exist",
          value: e.resistance_type_id
        });
      }
    }));

    return errors;
  }
}

module.exports = new ProgramsValidation();
