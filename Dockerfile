FROM eu.gcr.io/meisterfit/meisterfit-node:carbon

ARG DEFAULT_NODE_ENV=development
ARG BUILD_NUMBER

ENV HOME=/home/node
ENV NODE_ENV $DEFAULT_NODE_ENV
ENV BUILD_NUMBER $BUILD_NUMBER

RUN mkdir -p $HOME/app

WORKDIR $HOME/app

COPY . $HOME/app

RUN yarn install --production

EXPOSE $PORT

CMD node server

